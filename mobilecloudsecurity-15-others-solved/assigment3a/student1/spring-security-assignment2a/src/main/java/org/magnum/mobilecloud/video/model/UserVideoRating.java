package org.magnum.mobilecloud.video.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserVideoRating {

    // Id is composed of <user>::<videoId>
    @Id
    private String id;

    private long videoId;

    private double rating;

    private String user;

    public UserVideoRating() {
    }

    public UserVideoRating(long videoId, double rating, String user) {
	super();
	this.id = user + "::" + videoId;
	this.videoId = videoId;
	this.rating = rating;
	this.user = user;
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public long getVideoId() {
	return videoId;
    }

    public void setVideoId(long videoId) {
	this.videoId = videoId;
    }

    public double getRating() {
	return rating;
    }

    public void setRating(double rating) {
	this.rating = rating;
    }

    public String getUser() {
	return user;
    }

    public void setUser(String user) {
	this.user = user;
    }

}