package org.magnum.mobilecloud.video.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.magnum.mobilecloud.video.auth.User;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoLocation;
import org.magnum.mobilecloud.video.model.VideoRepository;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.model.VideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRatingRepository;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

@Controller
public class VideoController {

    final static Logger log = LoggerFactory.getLogger(VideoController.class);
    
	@Autowired
	private VideoRepository videos;
	
	@Autowired
	private UserVideoRatingRepository ratings;

	@Autowired
	private VideoFileManager videoFileManager;
   
	String baseUrl; 
	
	
    protected VideoFileManager getVideoFileManager() throws IOException {
        return videoFileManager; // VideoFileManager.get();
    }
             
    // ------------------------------------------------------------------------        
    //  GET /video/{id}/data   - fetch video data
    
    @RequestMapping(value = "/video/{id}/data", method = RequestMethod.GET)
    public void getVideoData(@PathVariable("id") long id, 
    						 HttpServletResponse response, 
    						 Principal principal) throws IOException {
    	
    	String username = principal.getName();
    	
    	String info = "(" + username + ") getVideo("+ id + ") ";
     
        log.info(info + "begin");
        
        // check that video exists
        
        Video v = videos.findOne(id);
        if(v == null) {
            log.info(info +"video not found");
            response.setStatus(HTTP_NOT_FOUND);
            return;
        }

        // find video data
        
        VideoFileManager videoFileManager = getVideoFileManager();

        if(!videoFileManager.hasVideoData(v)) {
            log.info(info + "no video data");
            response.setStatus(HTTP_NOT_FOUND);
            return;
        }
        
        // output it
        
        // log.info("video + " + id + " found : " + v.getDataUrl());
        
        OutputStream out = response.getOutputStream();
        videoFileManager.copyVideoData(v, out);
        out.close();
        log.info(info + "output done");
    }

    // ------------------------------------------------------------------------    
    //  POST /video/{id}/data - save video data

    @RequestMapping(value = "/video/{id}/data", method = RequestMethod.POST)
    public @ResponseBody VideoStatus addVideoData(@PathVariable("id") long id,
                                                  HttpServletResponse response,
                                                  Principal principal,
                                                  @RequestParam("data") MultipartFile data)
        throws IOException
    {
    	String username = principal.getName();
    	String info = "(" + username + ") addVideoData(" + id + ") ";
    	
        log.info(info + "begin");
        
        if(data == null) {
            log.info(info + "no data");
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }
        
        // check that video exists 

        Video v = videos.findOne(id);
        if(v == null) {
            log.info(info + "video not found");            
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }
        log.info("video is: " + v);
        
        // check that video is owner by current user
        
        if(!username.equals(v.getOwner())) {
            log.info(info + "video is owned by other (" + v.getOwner() + ")");            
            response.setStatus(HTTP_FORBIDDEN);
            return null;        	
        }
        
        // save video to file

        
        InputStream ins = data.getInputStream();
        
        if(ins == null) {
            log.info(info + "no inputstream");                        
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }

        VideoFileManager videoFileManager = getVideoFileManager();
        videoFileManager.saveVideoData(v, ins);
        ins.close();
        
        log.info(info + "video add done: " + id);

        //+++ v.setUploaded(true);
        
        return new VideoStatus(VideoStatus.VideoState.READY);
    }
    
    // ------------------------------------------------------------------------
    // GET /video   - return all videos
    

    @RequestMapping(value = "/video", method = RequestMethod.GET)
    public @ResponseBody Collection<Video> getVideos(Principal principal) throws IOException {
    	String username = principal.getName();
    	String info = "(" + username + ") getVideos() ";
    	log.info(info + "begin");
    	List<Video> list = new LinkedList<Video>();
    	for(Video video: videos.findAll()) {
    		video.setOwner(null);
        	list.add(video);
        }
        return list;
    }


    // ------------------------------------------------------------------------
    //  POST /video
    //
    // save video info
    //

    @RequestMapping(value = "/video", method = RequestMethod.POST)
    public @ResponseBody Video addVideo(@RequestBody Video video, 
    		Principal principal,
    		HttpServletResponse response) throws IOException {
    	
    	String username = principal.getName();
    	long id = video.getId();
    	String title = video.getTitle();
    	String url = video.getUrl();
    	long duration = video.getDuration();
    	
    	String info = "(" + username + ") addVideo(): id=" +id  + ", title=" + title + ", duration=" + duration + ", url="+ url + " ";
    	
    	Video old = null;
    	
    	// extra "feature" handle updating by id
    	if(id > 0) {
    		old = videos.findOne(id);
    		if(old != null) {
    			if(!username.equals(old.getOwner())) {
    				log.info(info + "video is owned by other (" + old.getOwner() + ")");            
    				response.setStatus(HTTP_FORBIDDEN);
    				return null;        	
    			} else {
    	    		log.info(info + " OLD ONE by id");    				
    			}
    		} else {
    			video.setId(id);
    		}

    	} else if((old = videos.findByOwnerAndTitleAndDurationAndUrl(username, title, duration, url)) != null) {
    		log.info(info + "OLD BY title,duration,url id=" + old.getId());
    		video.setId(old.getId()); 
    	} else {
    		log.info(info + "video is new");
    	}

    	video.setOwner(username);
    	
    	Video ret = videos.save(video);
        log.info(info + "add new video: result= " + ret);
        return ret;
    }

    // ------------------------------------------------------------------------    
    // 
    // GET /video/{id} - return information about video by id
    

    @RequestMapping(value = "/video/{id}", method = RequestMethod.GET)
    public @ResponseBody Video getVideo(@PathVariable("id") long id, 
    		HttpServletResponse response, 
    		Principal user)
        throws IOException
    {
    	String username = user.getName();
    	String info = "(" + username + ") getVideo(" + id + ") ";
    	
    	log.info(info + "get");
    	
        Video video = videos.findOne(id);

        if(video == null) {
            log.info(info + "not found");                        
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }
        video.setOwner(null);

        return video;
    }

    // ------------------------------------------------------------
    // 
    // (For testing) retun all ratings
    //

    @RequestMapping(value = "/video/ratings", method = RequestMethod.GET)
    public @ResponseBody List<UserVideoRating> getVideoRatings(
    		Principal user,
    		HttpServletResponse response) 
           throws IOException
    {
    	String username = user.getName();
    	String info = "(" + username + ") getRatings()";

    	List<UserVideoRating> list = new LinkedList<UserVideoRating>();
    	int count = 0;
    	for(UserVideoRating rating: ratings.findAll()) {
    		list.add(rating);
    		count++;
    	}
    	log.info(info + "count of ratings=" + count);
    	return list;
    }


    public AverageVideoRating findAverageRatingByVideoId(long videoId)  throws IOException {

    	return  ratings.findAvgRatingByVideoId(videoId);
    	
    }
    
    // ------------------------------------------------------------
    //
    // GET /video/{id}/rating  - get current avarge rating and total rateres 
    
  
    @RequestMapping(value = "/video/{id}/rating", method = RequestMethod.GET)
    public @ResponseBody AverageVideoRating getVideoRating(
    		@PathVariable("id") long id,
    		Principal user,
    		HttpServletResponse response) 
           throws IOException
    {
    	String username = user.getName();
    	String info = "(" + username + ") getRating(" + id + ") ";
        Video v = videos.findOne(id);
        if(v == null) {
            log.warn(info + "not found");
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }
        AverageVideoRating rating = findAverageRatingByVideoId(id);
        log.info(info + " rating=" + rating);
        return rating;
    }
 
    // ------------------------------------------------------------
    // POST /video/{id}/rating/{rating}
    //
    // Handle rating
    //
 
    @RequestMapping(value = "/video/{id}/rating/{rating}", method = RequestMethod.POST)
    public @ResponseBody AverageVideoRating setVideoRating(
    		@PathVariable("id") long id,
    		@PathVariable("rating") int rating,    		
       		Principal principal,
    		HttpServletResponse response)
        throws IOException
    {
    	String username = principal.getName();
    	String info = "(" + username + ") addRating id=" + id + ", rating=" + rating + " : ";
    	
    	// check rating value 
    	
        if(rating <=  0 || rating > 5) {
            log.info(info + " bad rating");
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }
        
        // Find video
        
        Video v = videos.findOne(id);
        if(v == null) {
            log.info(info + "video not found");
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }
        
        // Find previous rating if exists
        
        UserVideoRating r = ratings.findByVideoIdAndUser(id, username);
        if(r == null) {
        	log.info(info + "crate new rating");
        	r = new UserVideoRating(id, rating, username);
        	UserVideoRating nr = ratings.save(r);
            log.info(info + " rating-id=" + nr.getId());        	
        } else if(r.getRating() != rating) {
        	log.info(info + "old rating found and changed" + + r.getRating());
        	// if rating changed then update it.
        	r.setRating(rating);
        	ratings.save(r);
        } else {
        	log.info(info + "old rating found ant not changed: " + r.getRating());
        }

        return findAverageRatingByVideoId(id);
    }

    // Extra thing inherited from previous assignment

    @RequestMapping(value = "/video/{id}/ratingbody", method = RequestMethod.POST)
    public @ResponseBody AverageVideoRating setVideoRatingBody(
    		@PathVariable("id") long id,
    		@RequestBody VideoRating ratingObj,
       		Principal principal,
    		HttpServletResponse response)
        throws IOException
    {
        int rating = ratingObj.getRating();
        return setVideoRating(id, rating, principal, response);
    }
        
    // Extra thing inherited from previous assignment

    @RequestMapping(value = "/video/{id}/location", method = RequestMethod.POST)
    public @ResponseBody VideoLocation setVideoLocation(@PathVariable("id") long id,
                                                        @RequestBody VideoLocation locationObj,
                                                        Principal user,
                                                        HttpServletResponse response)
        throws IOException
    {
    	String username = user.getName();
        String location = locationObj.getLocation();
        
        String info = "(" + username + ") postLocation(" + id + ", " + location + ")"; 
        
        Video v = videos.findOne(id);
        if(v == null) {
            log.info(info + "video not found");
            response.setStatus(HTTP_NOT_FOUND);
            return null;
        }
        
        if(!username.equals(v.getOwner())) {
        	log.info(info +" not owner=" + v.getOwner());
            response.setStatus(HTTP_FORBIDDEN);
            return null;
        }
        
        log.info(info + "save location");
        v.setLocation(location);
        videos.save(v);
        
        return locationObj;
    }




    private String getUrlBaseForLocalServer() {
    	if(baseUrl == null) {
    		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    		String base =   "https://"+request.getServerName() 
    				+ ((request.getServerPort() != 433) ? ":"+request.getServerPort() : "");
    		baseUrl = base;
    	}
        return baseUrl;
    }
    
}
