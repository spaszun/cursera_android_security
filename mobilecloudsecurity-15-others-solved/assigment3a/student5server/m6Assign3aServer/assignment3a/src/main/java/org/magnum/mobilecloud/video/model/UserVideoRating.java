package org.magnum.mobilecloud.video.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



// Annotated for JPA object
// separate repository from videoRepository_, but linked
// to the video object by the "Video ID"
//
@Entity
public class UserVideoRating {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long rid;
	
	private long vid;

	private double rating;

	private String user;

	public UserVideoRating() {
	}

	public UserVideoRating(long vid, double rating, String user) {
		super();
		this.vid = vid;
		this.rating = rating;
		this.user = user;
	}

	public long getRid() {
		return rid;
	}

	public void setRid(long rid) {
		this.rid = rid;
	}

	public long getVid() {
		return vid;
	}

	public void setVid(long vid) {
		this.vid = vid;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
