/* 
**
** Copyright 2014, Jules White
**
** 
*/
package vandy.mooc.common;

public interface TaskCallback<T> {

    public void success(T result);

    public void error(Exception e);

}
