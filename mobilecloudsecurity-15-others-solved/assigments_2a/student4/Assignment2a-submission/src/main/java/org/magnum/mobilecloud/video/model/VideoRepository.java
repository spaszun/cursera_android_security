package org.magnum.mobilecloud.video.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Using Spring data jpa
 */
@Repository
public interface VideoRepository extends CrudRepository<Video, Long> {

    // Find all videos with a matching title (e.g., Video.title)
    public Collection<Video> findByTitle(String title);

    // Find all videos that are shorter than a specified duration
    public Collection<Video> findByDurationLessThan(long maxduration);

    // Find a video with a matching url
    public Video findByUrl(String url);


    /*
	 * See: http://docs.spring.io/spring-data/jpa/docs/1.3.0.RELEASE/reference/html/jpa.repositories.html
	 * for more examples of writing query methods
	 */
}
