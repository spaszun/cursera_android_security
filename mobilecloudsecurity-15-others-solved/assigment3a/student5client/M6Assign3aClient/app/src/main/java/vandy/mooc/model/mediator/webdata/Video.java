package vandy.mooc.model.mediator.webdata;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

/**
 * This "Plain Ol' Java Object" (POJO) class represents meta-data of
 * interest downloaded in Json from the Video Service via the
 * VideoServiceProxy.
 */
public class Video {
    /**
     * Various fields corresponding to data downloaded in Json from
     * the Video WebService.
     */
    private long id;
    private String title;
    private String url;
    private long duration;
    private String location;
    private String subject;
    private String contentType;
    @JsonIgnore
    private double rating;

    /**
     * No-op constructor
     */
    public Video() {
    }

    /**
     * Constructor that initializes title, duration, and contentType.
     */
    public Video(String title,
                 long duration,
                 String contentType) {
        this.title = title;
        this.duration = duration;
        this.contentType = contentType;
    }

    /**
     * Constructor that initializes all the fields of interest.
     */
    public Video(long id,
                 String title,
                 long duration,
                 String contentType,
                 String url) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.contentType = contentType;
        this.url = url;
    }

    /**
     * Constructor that initializes all the fields of interest.
     */
    public Video(long id,
                 String title,
                 String url,
                 long duration,
                 String location,
                 String subject,
                 String contentType,
                 double rating ) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.location = location;
        this.subject = subject;
        this.contentType = contentType;
        this.url = url;
        this.rating = rating;
    }

    /**
     * Constructor that initializes all the fields of interest.
     */
    public Video(long id,
                 String title,
                 String url,
                 long duration,
                 String location,
                 String subject,
                 String contentType) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.location = location;
        this.subject = subject;
        this.contentType = contentType;
        this.url = url;
    }
    /*
     * Getters and setters to access Video.
     */

    /**
     * Get the Id of the Video.
     *
     * @return id of video
     */
    public long getId() {
        return id;
    }

    /**
     * Get the Video by Id
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the Title of Video.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the Title of Video.
     */
    public void setTitle(String title) {
        this.title = title;
    }



    /**
     * Get the url of Video
     *
     * @return url of Video
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set the url of the Video.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Get the Duration of Video.
     *
     * @return Duration of Video.
     */
    public long getDuration() {
        return duration;
    }

    /**
     * Set the Duration of Video.
     */
    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    /**
     * Get ContentType of Video.
     *
     * @return contentType of Video.
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Set the ContentType of Video.
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @JsonIgnore
    public double getRating() {
        return rating;
    }
    @JsonIgnore
    public void setRating(double rating) {
        this.rating = rating;
    }

    /**
     * @return the textual representation of Video object.
     */
    @Override
    public String toString() {
        return "{" +
                "Id: "+ id + ", "+
                "Title: "+ title + ", "+
                "Url: "+ url + ", "+
                "Duration: "+ duration + ", "+
                "Location: "+ location + ", "+
                "Subject: "+ subject + ", "+
                "ContentType: "+ contentType + ", "+
           //     "Rating: "+ rating +
                "}";
    }

    /**
     * @return an Integer hash code for this object. 
     */
    @Override
    public int hashCode() {
        return Objects.hash(getTitle(),
                getDuration());
    }

    /**
     * @return Compares this Video instance with specified 
     *         Video and indicates if they are equal.
     */
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Video)
                && Objects.equals(getTitle(),
                ((Video) obj).getTitle())
                && getDuration() == ((Video) obj).getDuration();
    }
}
