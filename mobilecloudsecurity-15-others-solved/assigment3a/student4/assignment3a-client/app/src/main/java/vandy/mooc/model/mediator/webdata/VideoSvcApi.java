package vandy.mooc.model.mediator.webdata;

import java.util.List;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Streaming;
import retrofit.mime.TypedFile;

/**
 * This interface defines an API for a VideoSvc. The
 * interface is used to provide a contract for client/server
 * interactions. The interface is annotated with Retrofit
 * annotations so that clients can automatically convert the
 *
 * @author jules
 */
public interface VideoSvcApi {

    String ID_PARAMETER = "id";
    String DATA_PARAMETER = "data";
    String RATING_PARAMETER = "rating";

    String TOKEN_PATH = "/oauth/token";

    // The path where we expect the VideoSvc to live
    String VIDEO_SVC_PATH = "/video";

    String VIDEO_DATA_PATH = VIDEO_SVC_PATH + "/{" + ID_PARAMETER + "}/data";

    String VIDEO_RATING_PATH = VIDEO_SVC_PATH + "/{" + ID_PARAMETER + "}/rating";

    String VIDEO_USER_RATING_PATH = VIDEO_RATING_PATH + "/{" + RATING_PARAMETER + "}";


    /**
     * returns list of videos available on server.
     * @return list of video objects
     */
    @GET(VIDEO_SVC_PATH)
    List<Video> getVideoList();


    /**
     * Returns video metadata object including average rating and rating count
     * available also as separate service
     * @param id video identifier
     * @return video metadata object
     */
    @GET(VIDEO_SVC_PATH + "/{id}")
    Video getVideoById(@Path("id") long id);


    /**
     * Created video metadata object in server and returns same object with server
     * generated identifier.
     *
     * @param v video metadata object
     * @return video metadata object with added and updated fields
     */
    @POST(VIDEO_SVC_PATH)
    Video addVideo(@Body Video v);


    /**
     * Service to register user rating for video
     *
     * @param id identifier for video to rate
     * @param rating user rating given
     * @return average rating for video after registered user rating
     */
    @POST(VIDEO_USER_RATING_PATH)
    AverageVideoRating rateVideo(@Path("id") long id, @Path("rating") double rating);

    /**
     * Average rating for video
     *
     * @param id video id
     * @return object containing average rating, number of ratings and video id
     */
    @GET(VIDEO_RATING_PATH)
    AverageVideoRating getVideoRating(@Path("id") long id);


    /**
     * Store video binary container in server
     *
     * @param id video identifier
     * @param videoData encoded multipart video binary container
     * @return object containing video status enum
     */
    @Multipart
    @POST(VIDEO_DATA_PATH)
    VideoStatus setVideoData(@Path(ID_PARAMETER) long id, @Part(DATA_PARAMETER) TypedFile videoData);


    /**
     * This method uses Retrofit's @Streaming annotation to indicate that the
     * method is going to access a large stream of data (e.g., the mpeg video
     * data on the server). The client can access this stream of data by obtaining
     * an InputStream from the Response as shown below:
     * <p/>
     * VideoSvcApi client = ... // use retrofit to create the client
     * Response response = client.getData(someVideoId);
     * InputStream videoDataStream = response.getBody().in();
     *
     * @param id video identifier
     * @return response to request, 200 and stream if requested resource exists and 404 if not.
     */
    @Streaming
    @GET(VIDEO_DATA_PATH)
    Response getVideoData(@Path(ID_PARAMETER) long id);

}
