package vandy.mooc.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Collection;
import java.util.concurrent.Callable;

import vandy.mooc.R;
import vandy.mooc.common.CallableTask;
import vandy.mooc.common.TaskCallback;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.mediator.webdata.VideoServiceProxy;
import vandy.mooc.model.mediator.webdata.VideoSvc;

public class LoginScreenActivity extends Activity implements View.OnClickListener {

    private EditText textUsername;

    private EditText textPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_screen_activity);

        textUsername = (EditText) findViewById(R.id.textUsername);
        textUsername.requestFocus();

        textPassword = (EditText) findViewById(R.id.textPassword);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(LoginScreenActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:

                String user = textUsername.getText().toString().trim();
                String pass = textPassword.getText().toString().trim();

                final VideoServiceProxy svc = VideoSvc.init(user, pass);

                CallableTask.invoke(new Callable<Collection<Video>>() {

                    @Override
                    public Collection<Video> call() throws Exception {
                        return svc.getVideoList();
                    }
                }, new TaskCallback<Collection<Video>>() {

                    @Override
                    public void success(Collection<Video> result) {
                        // OAuth 2.0 grant was successful and web
                        // can talk to the server, open up the video listing
                        startActivity(new Intent(
                                LoginScreenActivity.this,
                                ListVideosActivity.class));
                    }

                    @Override
                    public void error(Exception e) {
                        Log.e(LoginScreenActivity.class.getName(), "Error logging in via OAuth.", e);

                        Toast.makeText(
                                LoginScreenActivity.this,
                                "Login failed, check your Internet connection and credentials.",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                break;
        }
    }

}
