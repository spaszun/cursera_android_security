/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package vandy.mooc.diagnostics;


import android.content.Context;
import android.content.Intent;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;
import vandy.mooc.model.mediator.webdata.SecuredRestBuilder;
import vandy.mooc.model.mediator.webdata.VideoSvcApi;
import vandy.mooc.view.LoginActivity;

public class VideoSvc {

	public static final String CLIENT_ID = "mobile";

	private static VideoSvcApi videoSvc_;

	public static synchronized VideoSvcApi getOrShowLogin(Context ctx) {
		if (videoSvc_ != null) {
			return videoSvc_;
		} else {
			Intent i = new Intent(ctx, LoginActivity.class);
			ctx.startActivity(i);
			return null;
		}
	}

	public static synchronized VideoSvcApi init(String server, String user,
			String pass) {

		videoSvc_ = new SecuredRestBuilder()
				.setLoginEndpoint(server + VideoSvcApi.TOKEN_PATH)
				.setUsername(user)
				.setPassword(pass)
				.setClientId(CLIENT_ID)
				.setClient(
						new ApacheClient(new EasyHttpClient()))
				.setEndpoint(server).setLogLevel(LogLevel.FULL).build()
				.create(VideoSvcApi.class);

		return videoSvc_;
	}
}
