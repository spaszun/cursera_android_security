package vandy.mooc.model.mediator.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import vandy.mooc.model.mediator.webdata.Video;


public class VideoCache {
    private WeakReference<Context> mContext;

    public VideoCache(Context context) {
        mContext = new WeakReference<>(context);
    }

    public Uri insert(Video video) {
        return mContext.get()
                .getContentResolver()
                .insert(VideoContract.VideoInfoEntry.CONTENT_URI,
                        getContentValues(video));
    }

    public int insertAllVideos(ArrayList<Video> videos) {
        ContentValues[] cvArray =
                new ContentValues[videos.size()];

        int i = 0;

        for (Video video : videos)
            cvArray[i++] = getContentValues(video);

        return mContext.get().getContentResolver()
                .bulkInsert(VideoContract.VideoInfoEntry.CONTENT_URI, cvArray);

    }

    public Video getVideoById(long videoId) {

        final String rowId =
                "" + VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID
                        + " = '"
                        + videoId
                        + "'";

        try (Cursor cursor = mContext.get().getContentResolver()
                .query(VideoContract.VideoInfoEntry.CONTENT_URI,
                        null,
                        rowId,
                        null,
                        null)) {
            if (cursor != null) {
                cursor.moveToFirst();
                return getVideo(cursor);
            }
            else
                return null;
        }
    }

    public ArrayList<Video> getAllVideos() {
        ArrayList<Video> videos = new ArrayList<>();

        try (Cursor cursor =
                     mContext.get().getContentResolver()
                             .query(VideoContract.VideoInfoEntry.CONTENT_URI,
                                     null,
                                     null,
                                     null,
                                     null)) {
            if (cursor != null) {
                while (cursor.moveToNext())
                    videos.add(getVideo(cursor));
            }
        }

        return videos;
    }


    private ContentValues getContentValues(Video video) {
        ContentValues values = new ContentValues();
        values.put(VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID,
                video.getId());
        values.put(VideoContract.VideoInfoEntry.COLUMN_TITLE,
                video.getTitle());
        values.put(VideoContract.VideoInfoEntry.COLUMN_URL,
                video.getUrl());
        values.put(VideoContract.VideoInfoEntry.COLUMN_DURATION,
                video.getDuration());
        values.put(VideoContract.VideoInfoEntry.COLUMN_LOCATION,
                video.getLocation());
        values.put(VideoContract.VideoInfoEntry.COLUMN_SUBJECT,
                video.getSubject());
        values.put(VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE,
                video.getContentType());
        values.put(VideoContract.VideoInfoEntry.COLUMN_RATING,
                video.getRating());
        return values;
    }

    private Video getVideo(Cursor cursor) throws IllegalArgumentException {
        long video_id =
                cursor.getLong(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID));
        String title =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_TITLE));
        String url =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_URL));
        long duration =
                cursor.getLong(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_DURATION));
        String location =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_LOCATION));
        String subject =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_SUBJECT));
        String content_type =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE));
        double rating =
                cursor.getLong(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_RATING));


        return new Video(video_id,
                title,
                url,
                duration,
                location,
                subject,
                content_type,
                rating);
    }
}
