package com.sps.videoserviceapp.videos.contentprovider;


import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.sps.videoserviceapp.util.SelectionBuilder;


public class VideoContentProvider extends ContentProvider {

    // database
    private VideoDatabaseHelper mDatabaseHelper;

    /**
     * Content authority for this provider.
     */
    private static final String AUTHORITY = VideoContract.CONTENT_AUTHORITY;

    /**
     * URI ID for route: /ROUTE_VIDEOS
     */
    public static final int ROUTE_VIDEOS = 1;

    /**
     * URI ID for route: /entries/{ID}
     */
    public static final int ROUTE_VIDEOS_ID = 2;


    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(AUTHORITY, "videos", ROUTE_VIDEOS);
        sUriMatcher.addURI(AUTHORITY, "videos/*", ROUTE_VIDEOS_ID);
    }

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new VideoDatabaseHelper(getContext());
        return true;
    }

    /**
     * Determine the mime type for entries returned by a given URI.
     */
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_VIDEOS:
                return VideoContract.Entry.CONTENT_TYPE;
            case ROUTE_VIDEOS_ID:
                return VideoContract.Entry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * Perform a database query by URI.
     *
     * <p>Currently supports returning all videos (/videos) and individual video by ID
     * (/videos/{ID}).
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        SelectionBuilder builder = new SelectionBuilder();
        int uriMatch = sUriMatcher.match(uri);
        switch (uriMatch) {
            case ROUTE_VIDEOS_ID:
                // Return a single entry, by ID.
                String id = uri.getLastPathSegment();
                builder.where(VideoContract.Entry.COLUMN_NAME_ID + "=?", id);
            case ROUTE_VIDEOS:
                // Return all known entries.
                builder.table(VideoContract.Entry.TABLE_NAME)
                        .where(selection, selectionArgs);
                Cursor c = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                Context ctx = getContext();
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * Insert a new entry into the database.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
            final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
            assert db != null;
            final int match = sUriMatcher.match(uri);
            Uri result;
            switch (match) {
                case ROUTE_VIDEOS:
                    long id = db.insertOrThrow(VideoContract.Entry.TABLE_NAME, null, values);
                    result = Uri.parse(VideoContract.Entry.CONTENT_URI + "/" + id);
                    break;
                case ROUTE_VIDEOS_ID:
                    throw new UnsupportedOperationException("Insert not supported on URI: " + uri);
                default:
                    throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
            // Send broadcast to registered ContentObservers, to refresh UI.
            Context ctx = getContext();
            assert ctx != null;
            ctx.getContentResolver().notifyChange(uri, null, false);
            return result;
    }
    /**
     * Delete an entry by database by URI.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_VIDEOS:
                count = builder.table(VideoContract.Entry.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            case ROUTE_VIDEOS_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(VideoContract.Entry.TABLE_NAME)
                        .where(VideoContract.Entry.COLUMN_NAME_ID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * Update an entry in the database by URI.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_VIDEOS:
                count = builder.table(VideoContract.Entry.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_VIDEOS_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(VideoContract.Entry.TABLE_NAME)
                        .where(VideoContract.Entry.COLUMN_NAME_ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }



    static public class VideoDatabaseHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "videotable.db";
        private static final int DATABASE_VERSION = 1;

        // Database creation SQL statement
        private static final String DATABASE_CREATE = "create table "
                + VideoContract.Entry.TABLE_NAME
                + "("
                + VideoContract.Entry.COLUMN_NAME_ID + " integer primary key, "
                + VideoContract.Entry.COLUMN_NAME_TITLE + " text not null, "
                + VideoContract.Entry.COLUMN_NAME_URL + " text not null,"
                + VideoContract.Entry.COLUMN_NAME_DURATION + " integer, "
                + VideoContract.Entry.COLUMN_NAME_LOCATION + " text,"
                + VideoContract.Entry.COLUMN_NAME_SUBJECT + " text,"
                + VideoContract.Entry.COLUMN_NAME_CONTENT_TYPE + " text,"
                + VideoContract.Entry.COLUMN_NAME_RATING + " real"
                + ");";

        //SQL statement to drop "entry" table.
        private static final String DATABASE_DROP =
                "DROP TABLE IF EXISTS " + VideoContract.Entry.TABLE_NAME;

        public VideoDatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Method is called during creation of the database
        @Override
        public void onCreate(SQLiteDatabase database) {
            database.execSQL(DATABASE_CREATE);
        }

        // Method is called during an upgrade of the database,
        // e.g. if you increase the database version
        @Override
        public void onUpgrade(SQLiteDatabase database, int oldVersion,
                              int newVersion) {
            database.execSQL(DATABASE_DROP);
            onCreate(database);
        }
    }

} 
