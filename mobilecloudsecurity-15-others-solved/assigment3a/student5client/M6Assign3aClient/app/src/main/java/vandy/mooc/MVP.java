package vandy.mooc;

import android.net.Uri;

import vandy.mooc.common.ContextView;
import vandy.mooc.common.PresenterOps;
import vandy.mooc.model.mediator.webdata.Video;

/**
 * Created by roy on 7/17/2015.
 */
public interface MVP {

    /*
     *  This interface defines the minimum API needed by the
     *  VideoPresenter class in the Presenter layer.
     *  It extends the ContentView interface so the Presentation
     *  Layer can access the Context's defined in the View
     *  Layer.
     */
    interface VideosRequiredViewOps extends ContextView {
        /*
         *  Show results of Loading videos in Activity.
         */
        void showGetVideosResult(Boolean success);
    }


    /*
     *  This interface defines the minimum API needed by the
     *  VideoPresenter class in the Presenter layer.
     *  It extends the PresenterOps interface, which is
     *  instantiated by MVP.VideoRequiredViewOps interface
     *
    */
    interface VideosProvidedPresenterOps
            extends PresenterOps<MVP.VideosRequiredViewOps> {
        /*
         *  Show results of Loading videos in Activity.
         */
        void uploadVideo(Uri videoUri);
    }


    interface DisplayVideoRequiredViewOps
            extends ContextView {
        void updateRating(long videoId, double ratingResult);
    }


    interface DisplayVideoProvidedPresenterOps
            extends PresenterOps<DisplayVideoRequiredViewOps> {

        Video getVideoById(long videoId);

        String getPathByName(String videoName);

        void downloadVideo(long videoId,
                           String videoName);

        void addRating(long videoId,
                       int rating);

    }


}


