package vandy.mooc.model.provider;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created on 16.07.2015.
 */

public final class VideoContract {

    public static final String CONTENT_AUTHORITY = "vandy.mooc.videos";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    public static final int VIDEO = 101;
    public static final int VIDEOS = 100;

    public static final class VideoEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(VideoEntry.TABLE_NAME).build();

        /**
         * When the Cursor returned for a given URI by the ContentProvider contains 0..x items.
         */
        public static final String VIDEO_ITEMS_TYPE = "vnd.android.cursor.dir/"
                + CONTENT_AUTHORITY
                + "/"
                + VideoEntry.TABLE_NAME;

        /**
         * When the Cursor returned for a given URI by the ContentProvider contains 1 item.
         */
        public static final String VIDEO_ITEM_TYPE = "vnd.android.cursor.item/"
                + CONTENT_AUTHORITY
                + "/"
                + VideoEntry.TABLE_NAME;

        /**
         * From requirements doc:
         * id,
         * title,
         * duration,
         * contentType,
         * data_url, and
         * star_rating.
         */

        public static final String TABLE_NAME = "video";

        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DURATION = "duration";
        public static final String COLUMN_CONTENT_TYPE = "content_type";
        public static final String COLUMN_DATA_URL = "data_url";
        public static final String COLUMN_STAR_RATING = "star_rating";

        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }

    protected static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        // For each type of URI that is added, a corresponding code is
        // created.
        matcher.addURI(VideoContract.CONTENT_AUTHORITY,
                VideoContract.VideoEntry.TABLE_NAME, VIDEO);
        return matcher;
    }

}
