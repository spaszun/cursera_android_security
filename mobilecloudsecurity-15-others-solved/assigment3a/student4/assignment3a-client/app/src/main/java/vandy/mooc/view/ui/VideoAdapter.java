package vandy.mooc.view.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vandy.mooc.R;
import vandy.mooc.model.mediator.webdata.Video;

/**
 * Show the view for each Video's meta-data in a ListView.
 */
public class VideoAdapter extends BaseAdapter {
    /**
     * Allows access to application-specific resources and classes.
     */
    private final Context mContext;

    /**
     * ArrayList to hold list of Videos that is shown in ListView.
     */
    private List<Video> videoList = new ArrayList<>();

    /**
     * Constructor that stores the Application Context.
     *
     * @param context context the adapter should work in
     */
    public VideoAdapter(Context context) {
        super();
        mContext = context;

    }

    /**
     * Method used by the ListView to "get" the "view" for each row of
     * data in the ListView.
     *
     * @param position    The position of the item within the adapter's data
     *                    set of the item whose view we want.
     * @param convertView The old view to reuse, if possible.
     *                    Note: You should check that this view is non-null and of an
     *                    appropriate type before using. If it is not possible
     *                    to convert this view to display the correct data,
     *                    this method can create a new view. Heterogeneous
     *                    lists can specify their number of view types, so
     *                    that this View is always of the right type (see
     *                    getViewTypeCount() and getItemViewType(int)).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        Video video = videoList.get(position);

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.video_list_item, null);
        }

        TextView titleText = (TextView) convertView.findViewById(R.id.textview_video_title);
        titleText.setText(video.getTitle());

        RatingBar starRating = (RatingBar) convertView.findViewById(R.id.ratingbar_video_user_rating);
        starRating.setRating(video.getStarRating());

        /*  We don't want to listen inside the list item anymore

        titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("VideoAdapter", "Title text pressed. Video id = " + video.getId());

                // todo Open video details activity

                Utils.showToast(mContext, "Video id = " + video.getId());

                startActivity(new Intent(
                        VideoListActivity.this,
                        VideoDetailActivity.class));
            }
        });

        //  If we want to accept rating from this list for some reason
        RatingBar.OnRatingBarChangeListener ratingListener =
                new RatingBar.OnRatingBarChangeListener() {
                    public void onRatingChanged(RatingBar bar, float rating, boolean touch) {
                    }
                };
                */

        return convertView;
    }

    /**
     * Adds a Video to the Adapter and notify the change.
     */
    public void add(Video video) {
        videoList.add(video);
        notifyDataSetChanged();
    }

    /**
     * Removes a Video from the Adapter and notify the change.
     */
    public void remove(Video video) {
        videoList.remove(video);
        notifyDataSetChanged();
    }

    /**
     * Get the List of Videos from Adapter.
     */
    public List<Video> getVideos() {
        return videoList;
    }

    /**
     * Set the Adapter to list of Videos.
     */
    public void setVideos(List<Video> videos) {
        this.videoList = videos;
        notifyDataSetChanged();
    }

    /**
     * Get the no of videos in adapter.
     */
    public int getCount() {
        return videoList.size();
    }

    /**
     * Get video from a given position.
     */
    public Video getItem(int position) {
        return videoList.get(position);
    }

    /**
     * Get Id of video from a given position.
     */
    public long getItemId(int position) {
        return position;
    }
}
