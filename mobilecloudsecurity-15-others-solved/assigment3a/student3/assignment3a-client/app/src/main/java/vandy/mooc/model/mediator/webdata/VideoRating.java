package vandy.mooc.model.mediator.webdata;

/**
 * Created by kaido on 3.09.2015.
 */
public class VideoRating {

    private final long videoId;

    private final double rating;

    public long getVideoId() {
        return videoId;
    }

    public double getRating() {
        return rating;
    }

    public VideoRating(long videoId, double rating) {
        this.rating = rating;
        this.videoId = videoId;
    }
}
