package vandy.mooc.presenter;

import java.lang.ref.WeakReference;

import vandy.mooc.MVP;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;
import vandy.mooc.model.mediator.VideoDataMediator;
import vandy.mooc.model.mediator.webdata.AverageVideoRating;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.services.DownloadVideoService;
import vandy.mooc.utils.VideoMediaStoreUtils;


public class DisplayVideoPresenter
        implements  GenericAsyncTaskOps<Integer, Void, AverageVideoRating>,
        MVP.DisplayVideoProvidedPresenterOps {

    private static final String TAG =
            DisplayVideoPresenter.class.getSimpleName();

    private WeakReference<MVP.DisplayVideoRequiredViewOps> mDisplayVideoView;

    private GenericAsyncTask<Integer,
            Void,
            AverageVideoRating,
            DisplayVideoPresenter> mAsyncTask;

    VideoDataMediator mVideoMediator;

    private long mVideoId;

    public DisplayVideoPresenter() {
    }

    @Override
    public void onCreate(MVP.DisplayVideoRequiredViewOps view) {

        mDisplayVideoView = new WeakReference<>(view);

        mVideoMediator = new VideoDataMediator
                (mDisplayVideoView.get().getApplicationContext());

    }


    public void onConfigurationChange(MVP.DisplayVideoRequiredViewOps view) {
        mDisplayVideoView = new WeakReference<>(view);
    }

    @Override
    public void onDestroy(boolean isChangingConfigurations) {

    }


    public String getPathByName(String videoName) {
        return VideoMediaStoreUtils.getPathByName(
                mDisplayVideoView.get().getApplicationContext(),
                videoName);
    }

    public Video getVideoById(long videoId) {
        return mVideoMediator.getVideo(videoId);
    }

    public void downloadVideo(long videoId, String videoName) {
        mDisplayVideoView
                .get()
                .getApplicationContext()
                .startService
                        (DownloadVideoService.makeIntent
                                (mDisplayVideoView
                                  .get()
                                  .getApplicationContext(),
                                       videoId,
                                       videoName));
    }

    public void addRating(long videoId,
                          int rating) {
        mVideoId = videoId;
        mAsyncTask = new GenericAsyncTask<>(this);
        mAsyncTask.execute(rating);
    }

    @Override
    public AverageVideoRating doInBackground(Integer... params) {
        final int rating = params[0];
        return mVideoMediator.rateVideo(mVideoId, rating);
    }

    @Override
    public void onPostExecute(AverageVideoRating result) {
        long vid = result.getVideoId();
        double ratingResult = result.getRating();
        mDisplayVideoView.get().updateRating(vid, ratingResult);
    }


}