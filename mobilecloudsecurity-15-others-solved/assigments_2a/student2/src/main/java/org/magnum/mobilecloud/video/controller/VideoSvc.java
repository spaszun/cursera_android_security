package org.magnum.mobilecloud.video.controller;

import com.google.common.collect.Lists;

import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.repository.RatingRepository;
import org.magnum.mobilecloud.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import retrofit.client.Response;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller for Jpa video repository.
 */
@Controller
public class VideoSvc {
    /**
     * Video manager store will be automatically autowired.
     */
    @Autowired
    private VideoRepository videoRepository;

    /**
     * Video data manager store will be automatically autowired.
     */
    @Autowired
    private VideoFileManager videoDataManager;

    /**
     * Video data manager store will be automatically autowired.
     */
    @Autowired
    private RatingRepository ratingRepository;

    /**
     * Returns a list of the videos that have been added to this servlet.
     *
     * @return A list of video objects (JSON)
     */
    @RequestMapping(
            value = VideoSvcApi.VIDEO_SVC_PATH,
            method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<Video> getVideoList() throws IOException {
        return Lists.newArrayList(videoRepository.findAll());
    }

    /**
     * Returns a list of the videos that have been added to the repository .
     *
     * @return A list of video objects (JSON)
     */
    @RequestMapping(
            value = VideoSvcApi.VIDEO_GET_VIDEO_PATH,
            method = RequestMethod.GET)
    public
    @ResponseBody
    Video getVideoById(@PathVariable(VideoSvcApi.ID_PARAMETER) long id) {
        return videoRepository.findOne(id);
    }

    /**
     * Adds a video object to the repository.
     *
     * NOTE: the README.md specifically says to only allow the owner to
     * modify/overwrite and existing video for this addVideo endpoint, so
     * I added a check. But I think it was a documentation error and they
     * meant to only do it for the setVideoData endpoint...
     *
     * from the README.md:
     *
     * POST /video
     *     - __The "owner" member variable of the Video must be set to the
     *        name of the currently authenticated Principal__
     *     - __If a Video already exists, it should not be overwritten unless
     *       the name of the authenticated Principal matches the name of the
     *       owner member variable of the Video__
     *
     * @return returns the passed video object with updated id and URL path.
     */
    @RequestMapping(
            value = VideoSvcApi.VIDEO_SVC_PATH,
            method = RequestMethod.POST)
    @ResponseBody
    public Video addVideo(@RequestBody Video video,
                          Principal p,
                          HttpServletResponse response) throws IOException {

        if (video.getId() != 0) {
            // Check if a video object with the specified id exists.
            Video videoFind = videoRepository.findOne(video.getId());

            // Check to see if the video already exists.
            if (videoFind != null) {
                // Found a video with that id. Only let the user
                // change the meta-data for video (the same goes
                // for adding/changing the actual video data).
                if (!videoFind.getOwner().equals(p.getName())) {
                    // Principal is not the owner; return an error.
                    response.sendError(HttpServletResponse.SC_FORBIDDEN,
                            "Only the owner can modify video meta-data");
                    return null;
                }
            }
        }

        // Set the video owner field before saving.
        video.setOwner(p.getName());

        // Save the video.
        return videoRepository.save(video);
    }

    /**
     * Sets the MPEG video data for previously added Video object.
     * If data for this video has been previously set, it will be
     * overwritten.
     *
     * from the README.md
     *
     * POST /video/{id}/data
     *     - __Before saving the binary data received, the service should check to
     *       ensure that the name of the currently authenticated Principal matches
     *       the owner of the Video. If not, a HTTP 403 response should be sent
     *       to the client.__
     *
     * @return video status object containing a READY status.
     */
    @RequestMapping(
            value = VideoSvcApi.VIDEO_DATA_PATH,
            method = RequestMethod.POST)
    @ResponseBody
    public VideoStatus setVideoData(
            @PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            @RequestPart(VideoSvcApi.DATA_PARAMETER) MultipartFile videoData,
            Principal p,
            HttpServletResponse response) throws IOException {

        // Check if a video object with the specified id exists.
        Video video = videoRepository.findOne(id);
        if (video == null) {
            // The video doesn't exist, send the error (does not return).
            response.sendError(HttpServletResponse.SC_NOT_FOUND,
                    "No video exists with id " + id);
            return null;
        }

        if (!video.getOwner().equals(p.getName())) {
            // Principal is not the owner; return an error.
            response.setContentType("text/plain");
            response.sendError(HttpServletResponse.SC_FORBIDDEN,
                    "Only the owner can upload video data");
            return null;
        }

        // The video object exists so save the associated data.
        videoDataManager.saveVideoData(video, videoData.getInputStream());

        // Update the database with to register the state change.
        videoRepository.save(video);

        // Return a video status set to the READY state.
        return new VideoStatus(VideoStatus.VideoState.READY);
    }

    /**
     * Returns the data associated with the passed video id
     * as an input stream that is attached to the response body.
     *
     * @param id the id of the video
     * @return ResponseEntity that contains the response header,
     * a FileSystemResource (the data as a File object), and the
     * response code.
     */
    @RequestMapping(
            value = VideoSvcApi.VIDEO_DATA_PATH,
            method = RequestMethod.GET)
    @ResponseBody
    public void getVideoData(
            @PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            HttpServletResponse response) throws IOException {

        // Check if a video object with the specified id exists.
        Video video = videoRepository.findOne(id);
        if (video == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND,
                    "No video exists with id " + id);
            return ;
        }

        // Add the content type.
        response.addHeader("Content-Type", video.getContentType());

        // Set the default "Save As" name.
        response.addHeader("Content-Disposition", "attachment; filename=\""
                + videoDataManager.getVideoFileName(video) + "\"");

        // Construct and return a ResponseEntity wrapper that
        // contains the header, the served video data wrapped in
        // a FileSystemResource, and the response status code.
        // The video object exists so save the associated data.
        videoDataManager.copyVideoData(video, response.getOutputStream());

        // Default is OK, but it never hurts to be explicit.
        response.setStatus(HttpServletResponse.SC_OK);

        return;
    }

    /**
     * Adds a new rating to the accumulated ratings
     * for the specified video.
     *
     * @param id
     * @param response
     * @return returns the new average rating.
     */
    @RequestMapping(
            value = VideoSvcApi.VIDEO_GET_VIDEO_RATING_PATH,
            method = RequestMethod.GET)
    @ResponseBody
    public AverageVideoRating getVideoRating(
            @PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            HttpServletResponse response) throws IOException {

        // Check if a video object with the specified id exists.
        if (!videoRepository.exists(id)) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND,
                    "No video exists with id " + id);
            return null;
        }

        try {
            // This will throw and exception if there are no ratings
            // for this video, in which case the catch() handler will
            // send back a sensible empty AverageVideoRating.
            return ratingRepository.findAverageVideoRatingByVideoId(id);
        } catch (Exception e) {
            return new AverageVideoRating(0, id, 0);
        }
    }

    /**
     * Gets the average video rating for the specified video id.
     *
     * @param id
     * @param rating
     * @param response
     * @return returns the new average rating.
     */
    @RequestMapping(
            value = VideoSvcApi.VIDEO_RATE_VIDEO_PATH,
            method = RequestMethod.GET)
    @ResponseBody
    public AverageVideoRating rateVideo(
            @PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            @PathVariable(VideoSvcApi.RATING_PARAMETER) float rating,
            Principal p,
            HttpServletResponse response) throws IOException {

        Video video = videoRepository.findOne(id);
        if (video == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND,
                    "No video exists with id " + id);
            return null;
        }

        if (rating < 0 || rating > 5) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }

        // Check if the user has already rated this video.
        UserVideoRating userVideoRating =
                ratingRepository.findOneByUserAndVideoId(
                        p.getName(), video.getId());

        if (userVideoRating != null) {
            // User has already rated this video.
            if (rating != userVideoRating.getRating()) {
                // Update the user ratings table.
                userVideoRating.setRating(rating);
                ratingRepository.save(userVideoRating);
            }
        } else {
            // Create new user rating and save it.
            ratingRepository.save(new UserVideoRating(video.getId(), rating, p.getName()));
        }

        // Return the current average rating statistics.
        return ratingRepository.findAverageVideoRatingByVideoId(video.getId());
    }


    /**
     * Constructs a full URL for the specified video id
     *
     * @param videoId the id to use in the URL
     * @return a unique URL identifying the video for the passed video id
     */
    private String getDataUrl(long videoId) {
        String url = getUrlBaseForLocalServer()
                + "/video/" + videoId + "/data";
        return url;
    }

    /**
     * Constructs the base URL for the local server using information
     * from the request context holder.
     *
     * @return String value of the local URL base
     */
    private String getUrlBaseForLocalServer() {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder
                        .getRequestAttributes()).getRequest();

        String base = "http://"
                + request.getServerName()
                + ((request.getServerPort() != 80) ? ":"
                + request.getServerPort() : "");

        return base;
    }
}
