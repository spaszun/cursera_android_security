package secure.video.upload.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import secure.video.upload.R;

public class VideoPlayerActivity extends Activity
{
    public static final String VIDEO_FILE = "VideoFile";

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_video_player);

        String videoFile = getIntent().getStringExtra (VIDEO_FILE);
        if (videoFile == null) {
            Toast.makeText (this, "Impossible to play the video", Toast.LENGTH_SHORT).show();
            finish();
        }

        VideoView videoView = (VideoView) findViewById (R.id.videoView);
        videoView.setVisibility (View.VISIBLE);
        videoView.setVideoPath (videoFile);
        videoView.setMediaController (new MediaController (this));
        videoView.requestFocus();
        videoView.bringToFront();
        videoView.start();
    }
}
