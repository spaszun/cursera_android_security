package com.sps.videoserviceapp.videos;

import com.sps.videoserviceapp.rest.video.model.Video;

import java.io.Serializable;

/**
 * Created by Q1SW on 27-08-2015.
 */
public class VideoWithRating extends Video {

    public static final String VIDEO_OBJ = "VIDEO_OBJ";
    private double rating;

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
