package vandy.mooc.presenter;

import android.net.Uri;
import android.util.Log;

import java.lang.ref.WeakReference;

import vandy.mooc.MVP;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;
import vandy.mooc.model.mediator.VideoDataMediator;
import vandy.mooc.model.mediator.webdata.VideoServiceProxy;
import vandy.mooc.model.mediator.webdata.VideoSvc;
import vandy.mooc.model.services.UploadVideoService;

/**
 * This class plays implements all the operations related to listing on the
 * Video Service and uploading videos to the Video Service.  It plays
 * the role of the "Presenter" in the Model-View-Presenter (MVP)
 * pattern.  It extends GenericAsyncTaskOps so its doInBackground()
 * method runs in a background task.  It implements
 * MVP.VideosProvidedPresenterOps so it can be created/managed by the
 * GenericActivity framework and to decouple the Presenter and View
 * layers in the MVP pattern.
 */
public class VideosPresenter
        implements GenericAsyncTaskOps<Void, Void, Boolean>,
        MVP.VideosProvidedPresenterOps {

    /**
     * Debugging tag used by the Android logger.
     */
    protected final static String TAG =
            VideosPresenter.class.getSimpleName();

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<MVP.VideoRequiredViewOps> mVideosView;

    /**
     * The GenericAsyncTask used to expand a Video in background
     * thread via the Video web service.
     */
    private GenericAsyncTask<Void,
            Void,
            Boolean,
            VideosPresenter> mAsyncTask;

    /**
     * VideoDataMediator mediates the communication between Video
     * Service and local storage on the Android device.
     */
    VideoDataMediator mVideoDataMediator;

    /**
     * Default constructor that's needed by the GenericActivity
     * framework.
     */
    public VideosPresenter() {
    }

    /**
     * Hook method called when a new instance of VideosPresenter is
     * created.  One time initialization code goes here, e.g., storing
     * a WeakReference to the View layer and initializing the Model
     * layer.
     *
     * @param view A reference to the View layer.
     */
    @Override
    public void onCreate(MVP.VideoRequiredViewOps view) {
        // Set the WeakReference.
        mVideosView = new WeakReference<>(view);

        // Create VideoDataMediator that will mediate the
        // communication between Server and Android storage.
        mVideoDataMediator =
                new VideoDataMediator
                        (mVideosView.get().getActivityContext());

        // Execute AsyncTask to load the list of available videos from
        // the Video Service.
        mAsyncTask = new GenericAsyncTask<>(this);
        mAsyncTask.execute();
    }

    /**
     * Called after a runtime configuration change occurs to finish
     * the initialization steps.
     */
    @Override
    public void onConfigurationChange(MVP.VideoRequiredViewOps view) {
        Log.d(TAG,
                "onConfigurationChange() called with view = "
                        + view);

        // (Re)set the mVideosView WeakReference.
        mVideosView =
                new WeakReference<>(view);
    }

    /**
     * Hook method called to shutdown the Model layer.
     *
     * @param isChangingConfigurations True if a runtime configuration triggered the onDestroy() call.
     */
    @Override
    public void onDestroy(boolean isChangingConfigurations) {
        // No-op.
    }

    /**
     * Retrieve the Video List and save it via the VideoDataMediator
     * which runs a two-way synchronous call in a background thread to
     * avoid blocking the UI thread.
     */
    @Override
    public Boolean doInBackground(Void... ignore) {
        return mVideoDataMediator.getAndSaveVideoMetadata();
    }


    /**
     * Display the results in the UI Thread.
     */
    @Override
    public void onPostExecute(Boolean success) {
        mVideosView.get().showGetVideosResult(success);
    }

    /**
     * Start a Service that uploads the Video with a given Uri
     *
     * @param videoUri
     */
    public void uploadVideo(Uri videoUri) {
        // Sends an Intent command to the uploadVideoService
        // commanding it to upload the video at the given videoUri.
        mVideosView.get()
                .getApplicationContext()
                .startService
                        (UploadVideoService.makeIntent
                                (mVideosView.get()
                                                .getApplicationContext(),
                                        videoUri));
    }
}
