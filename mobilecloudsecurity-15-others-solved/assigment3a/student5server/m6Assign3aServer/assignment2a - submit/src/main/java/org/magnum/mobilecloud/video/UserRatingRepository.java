package org.magnum.mobilecloud.video;


import java.util.Collection;

import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRatingRepository extends CrudRepository<UserVideoRating, Long>{

	// Find all videos with a matching title (e.g., Video.name)
	public Collection<UserVideoRating> findByVid(long vid);
	

	// Find all videos with a matching title (e.g., Video.name)
	public UserVideoRating findOneByUserAndVid(String user, long vid);
}