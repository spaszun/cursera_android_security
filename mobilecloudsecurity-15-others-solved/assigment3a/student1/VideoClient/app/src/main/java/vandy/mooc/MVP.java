package vandy.mooc;

import android.net.Uri;

import vandy.mooc.common.ContextView;
import vandy.mooc.common.PresenterOps;
import vandy.mooc.model.mediator.webdata.Video;

/**
 * Defines the interfaces for the Acronym application that are
 * required and provided by the layers in the Model-View-Presenter
 * (MVP) pattern.  This design ensures loose coupling between the
 * layers in the app's MVP-based architecture.
 */
public interface MVP {
    /**
     * This interface defines the minimum API needed by the
     * VideoPresenter class in the Presenter layer to interact with
     * ListVideosActivity in the View layer.  It extends the
     * ContextView interface so the Presentation layer can access
     * Context's defined in the View layer.
     */
    interface VideoRequiredViewOps
            extends ContextView {
        /**
         * Show the results of Loading videos in activity.
         * to the user.
         *
         * @param success
         */
        void showGetVideosResult(Boolean success);
    }

    /**
     * This interface defines the minimum public API provided by the
     * VideosPresenter class in the Presenter layer to the
     * ListVideosActivity in the View layer.  It extends the
     * PresenterOps interface, which is instantiated by the
     * MVP.VideosRequiredViewOps interface used to define the parameter
     * that's passed to the onConfigurationChange() method.
     */
    interface VideosProvidedPresenterOps
            extends PresenterOps<MVP.VideoRequiredViewOps> {
        /**
         * Start a Service that uploads a Video with a given Uri.
         *
         * @param videoUri
         */
        void uploadVideo(Uri videoUri);
    }

    /**
     * This interface defines the minimum public API provided by the
     * DisplayVideoPresenter class in the Presenter layer to interact
     * with DisplayVideoActivity in the View layer.  It extends the
     * ContextView interface so the Presentation layer can access
     * Context's defined in the View layer.
     */
    interface DisplayVideoRequiredViewOps
            extends ContextView {
        /**
         * Display the result of rating the Video in the UI thread.
         */
        void updateRating(Video receivedVideo);
    }

    /**
     * This interface defines the minimum public API provided by the
     * DisplayVideoPresenter class in the Presenter layer to the
     * DisplayVideoActivity in the view layer.  It extends the
     * PresenterOps interface, which is instantiated bt the
     * MVP.DisplayVideoRequiredViewOps interface used to define the
     * parameter that's passed to the onConfigurationChange() method.
     * passed to the onConfigurationChange() method.
     */
    interface DisplayVideoProvidedPresenterOps
            extends PresenterOps<MVP.DisplayVideoRequiredViewOps> {

        /**
         * Get video from video id.
         *
         * @param videoId
         * @return
         */
        Video getVideoById(long videoId);

        /**
         * Get file path of video in local storage by video name.
         *
         * @param videoName
         * @return
         */
        String getPathByName(String videoName);


        /**
         * Downloads the video having given videoId and video name.
         *
         * @param videoId
         * @param videoName
         */
        void downloadVideo(long videoId,
                           String videoName);

        /**
         * Runs an AsyncTask that updates the rating of video
         * in the Video Service.
         *
         * @param videoId
         * @param rating
         */
        void addRating(long videoId,
                       int rating);

        void getRating(long videoId);

    }
}