package secure.video.upload.operations;


import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import retrofit.client.Response;
import secure.video.upload.R;
import secure.video.upload.activities.SingleVideoActivity;
import secure.video.upload.model.mediator.VideoMediator;
import secure.video.upload.model.mediator.webdata.VideoSvcApi;
import secure.video.upload.provider.VideoContract;
import secure.video.upload.utils.ConfigurableOps;
import secure.video.upload.utils.Constants;
import secure.video.upload.utils.GenericAsyncTask;
import secure.video.upload.utils.GenericAsyncTaskOps;
import secure.video.upload.utils.PersProgressDialog;

public class DownloadVideoOps implements ConfigurableOps, GenericAsyncTaskOps<Long,Void,String>
{
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * Retrofit api
     */
    private VideoSvcApi mVideoSvcApi;

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<SingleVideoActivity> mActivity;

    /**
     * The GenericAsyncTask used to download the video data
     */
    private GenericAsyncTask<Long, Void, String, DownloadVideoOps> mAsyncTask;

    /**
     * Result to display (if any).
     */
    private String mResult;

    /**
     * Default constructor that's needed by the GenericActivity framework.
     */
    public DownloadVideoOps()
    {
    }

    @Override
    public void onConfiguration (Activity activity, boolean firstTimeIn)
    {
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d (TAG, "onConfiguration() called the " + time + " with activity = " + activity);

        mActivity = new WeakReference<>((SingleVideoActivity) activity);

        if (firstTimeIn) {
            try {
                mVideoSvcApi = VideoMediator.get();
            }
            catch (SecurityException e) {
                mActivity.get().notifyMediatorNotConnected();
                return;
            }
        }
        else {
            if (mResult != null) {
                mActivity.get().videoDataArrived (mResult, null);
            }
        }
    }

    public void downloadVideoData (long videoId)
    {
        if (mAsyncTask != null) {
            mAsyncTask.cancel (true);
        }

        mAsyncTask = new GenericAsyncTask<>(this);
        mAsyncTask.execute (videoId);
    }

    @Override
    public void publishProgress (int progress)
    {
        mActivity.get().notifyProgressUpdate (progress, R.string.downloadVideoDialogTitle,
                R.string.downloadVideoDialogExpl);
    }

    @Override
    public String doInBackground (Long param)
    {
        String SELECTION_ID = VideoContract.VideoEntry.COLUMN_ID + " = " + param;
        try (Cursor cursor = mActivity.get().getContentResolver().query (
                VideoContract.VideoEntry.CONTENT_URI, null, SELECTION_ID, null, null)) {
            if (cursor.moveToFirst()) { // The video data has already been downloaded
                String localUri = cursor.getString (cursor.getColumnIndex (
                        VideoContract.VideoEntry.COLUMN_DATA_LOCAL_URI));
                if ((localUri != null) && (!localUri.isEmpty())) {
                    return localUri;
                }
            }

            // The video data has not been downloaded yet
            Response response = mVideoSvcApi.getVideoData (param);
            if (response.getStatus() != 200) {
                return null;
            }

            try {
                InputStream videoData = response.getBody().in();
                byte[] retrievedData = IOUtils.toByteArray (videoData);
                File storageDir = Environment.getExternalStorageDirectory();
                storageDir.mkdirs();
                File appDir = new File (storageDir.getAbsolutePath() + "/" + Constants.APP_FOLDER + "/downloads/");
                appDir.mkdir();
                File outFile = new File (appDir, String.valueOf (param) + "_data.mp4");
                FileOutputStream out = new FileOutputStream (outFile);
                out.write (retrievedData);
                out.close();

                String localDataUri = outFile.getAbsolutePath();

                ContentValues cv = new ContentValues();
                cv.put (VideoContract.VideoEntry.COLUMN_DATA_LOCAL_URI, localDataUri);
                int rows = mActivity.get().getContentResolver().update (
                        VideoContract.VideoEntry.CONTENT_URI, cv, SELECTION_ID, null);
                Log.d (TAG, "Updated " + rows + " rows in the db");

                return localDataUri;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public void onPostExecute (String dataUri, Long param)
    {
        mResult = dataUri;
        publishProgress (PersProgressDialog.OPERATION_COMPLETED);
        mActivity.get().videoDataArrived (dataUri, "Impossible to download the requested video");
        mAsyncTask = null;
    }
}
