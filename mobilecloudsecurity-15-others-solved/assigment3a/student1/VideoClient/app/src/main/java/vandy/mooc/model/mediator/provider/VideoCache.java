package vandy.mooc.model.mediator.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import vandy.mooc.model.mediator.webdata.Video;

/**
 * Facade that access the VideoContentProvider and performs CRUD
 * operations on it.
 */
public class VideoCache {
    /**
     * Store a weak Reference of Context so it can be collected by
     * Garbage collector later.
     */
    private WeakReference<Context> mContext;

    /**
     * Constructor to initialize the Video cache.
     *
     * @param context
     */
    public VideoCache(Context context) {
        mContext = new WeakReference<>(context);
    }

    /**
     * Inserts on Video into VideoDatabase with the help of
     * Content Resolvers.
     *
     * @param video
     * @return
     */
    public Uri insert(Video video) {
        return mContext.get()
                .getContentResolver()
                .insert(VideoContract.VideoInfoEntry.CONTENT_URI,
                        getContentValues(video));
    }

    /**
     * Inserts more than one videos into the VideoDatabase
     * with the help of Content Resolvers.
     *
     * @param videos
     * @return no of rows inserted.
     */
    public int insertAllVideos(ArrayList<Video> videos) {
        // Create array of ContentValues to hold parameter of list of
        // videos.
        ContentValues[] cvArray =
                new ContentValues[videos.size()];

        int i = 0;

        // Get the ContentValues for each Video object in the list.
        for (Video video : videos)
            cvArray[i++] = getContentValues(video);

        // Bulk insert all videos into VideoDatabase.
        return mContext.get().getContentResolver()
                .bulkInsert(VideoContract.VideoInfoEntry.CONTENT_URI,
                        cvArray);
    }

    /**
     * Gets a Video from VideoDatabase by videoId.
     *
     * @param videoId
     * @return
     */
    public Video getVideoById(long videoId) {
        // Selection statement that matches the videoId of the row
        // with the given videoId.
        final String rowId =
                ""
                        + VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID
                        + " = '"
                        + videoId
                        + "'";

        // Get teh cursor to the VideoDatabase with the given
        // selection statement.
        try (Cursor cursor =
                     mContext.get().getContentResolver()
                             .query(VideoContract.VideoInfoEntry.CONTENT_URI,
                                     null,
                                     rowId,
                                     null,
                                     null)) {
            // Check if there is any Video.
            if (cursor.moveToFirst())
                // Get video from cursor.
                return getVideo(cursor);
            else
                // No video present.
                return null;
        }
    }

    /**
     * Gets more than one video from the VideoDatabase with the help
     * of Content Resolvers.
     *
     * @return List of videos.
     */
    public ArrayList<Video> getAllVideos() {
        ArrayList<Video> videos = new ArrayList<>();

        // Get the cursor to the VideoDatabase.
        try (Cursor cursor =
                     mContext.get().getContentResolver()
                             .query(VideoContract.VideoInfoEntry.CONTENT_URI,
                                     null,
                                     null,
                                     null,
                                     null)) {

            // Iterate through all the cursors and get videos
            // from it.
            while (cursor.moveToNext())
                // Get video from cursor and add it to list.
                videos.add(getVideo(cursor));
        }

        return videos;
    }

    /**
     * Gets the ContentValues from yhe Video object.
     *
     * @param video
     * @return ContentValues
     */
    private ContentValues getContentValues(Video video) {
        ContentValues values = new ContentValues();
        values.put(VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID,
                video.getId());
        values.put(VideoContract.VideoInfoEntry.COLUMN_TITLE,
                video.getTitle());
        values.put(VideoContract.VideoInfoEntry.COLUMN_DURATION,
                video.getDuration());
        values.put(VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE,
                video.getContentType());
        values.put(VideoContract.VideoInfoEntry.COLUMN_DATA_URL,
                video.getUrl());
        values.put(VideoContract.VideoInfoEntry.COLUMN_RATING_COUNT,
                video.getRatingCount());
        values.put(VideoContract.VideoInfoEntry.COLUMN_USER_COUNT,
                video.getUserCount());
        return values;
    }

    /**
     * Gets metadata associated with Video from the Cursor that points
     * to the VideoDatabase.
     *
     * @param cursor
     * @return Video
     * @throws IllegalArgumentException
     */
    private Video getVideo(Cursor cursor) throws IllegalArgumentException {
        long videoId =
                cursor.getLong(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID));
        String title =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_TITLE));
        long duration =
                cursor.getLong(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_DURATION));
        String contentType =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE));
        String dataUrl =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_DATA_URL));
        float ratingCount =
                cursor.getFloat(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_RATING_COUNT));
        int userCount =
                cursor.getInt(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_USER_COUNT));

        // Create and return the Video object with the given metadata.
        return new Video(videoId,
                title,
                duration,
                contentType,
                dataUrl,
                ratingCount,
                userCount);
    }
}
