package vandy.mooc.model.mediator.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 * Content Provider interface used to manage Hobbit characters.  This
 * class plays the role of the "Abstraction" in the Bridge pattern.
 * It and the hierarchy it abstracts play the role of the "Model" in
 * the Model-View-Presenter pattern.
 */
public class VideoProvider extends ContentProvider {
    /**
     * Debugging tag used by the Android logger.
     */
    protected final static String TAG =
            VideoProvider.class.getSimpleName();

    private VideoDbHelper mOpenHelper;

    private static final int VIDEOS = 100;
    private static final int VIDEO = 101;


    /**
     * The URI Matcher used by this content provider.
     */
    public static final UriMatcher sUriMatcher =
            buildUriMatcher();

    /**
     * Helper method to match each URI to the ACRONYM integers
     * constant defined above.
     *
     * @return UriMatcher
     */
    protected static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher =
                new UriMatcher(UriMatcher.NO_MATCH);

        // For each type of URI that is added, a corresponding code is
        // created.
        matcher.addURI(VideoContract.CONTENT_AUTHORITY,
                VideoContract.PATH_VIDEO,
                VIDEOS);
        matcher.addURI(VideoContract.CONTENT_AUTHORITY,
                VideoContract.PATH_VIDEO
                        + "/#",
                VIDEO);
        return matcher;
    }


    /**
     * Return true if successfully started.
     */
    @Override
    public boolean onCreate() {
        // Select the concrete implementor.
        mOpenHelper = new VideoDbHelper(getContext());
        return true;
    }

    /**
     * Method called to handle type requests from client applications.
     * It returns the MIME type of the data associated with each
     * URI.  */
    @Override
    public String getType(Uri uri) {
        // Match the id returned by UriMatcher to return appropriate
        // MIME_TYPE.
        switch (sUriMatcher.match(uri)) {
            case VIDEOS:
                return VideoContract.VideoInfoEntry.CONTENT_ITEMS_TYPE;
            case VIDEO:
                return VideoContract.VideoInfoEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }
    }

    /**
     * Method called to handle insert requests from client
     * applications.
     */
    @Override
    public Uri insert(Uri uri, ContentValues cvs) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        Uri returnUri;

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If there's a match insert a new
        // row.
        switch (sUriMatcher.match(uri)) {
            case VIDEOS:
                long id =
                        db.insert(VideoContract.VideoInfoEntry.TABLE_NAME,
                                null,
                                cvs);
                if (id > 0)
                    returnUri =
                            VideoContract.VideoInfoEntry.buildUri(id);
                else
                    throw new android.database.SQLException
                            ("Failed to insert row into " + uri);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }

        // Notifies registered observers that a row was inserted.
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    /**
     * Method that handles bulk insert requests.
     */
    @Override
    public int bulkInsert( Uri uri, ContentValues[] cvsArray) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case VIDEOS:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues cvs : cvsArray) {
                        final long id =
                                db.insert(VideoContract.VideoInfoEntry.TABLE_NAME,
                                        null,
                                        cvs);
                        if (id != -1)
                            returnCount++;

                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

                getContext().getContentResolver().notifyChange(uri,
                        null);
                return returnCount;
            default:
                return super.bulkInsert(uri, cvsArray);
        }
    }

    /**
     * Method called to handle query requests from client
     * applications.
     */
    @Override
    public Cursor query( Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {

        Cursor retCursor;

        // Match the id returned by UriMatcher to query appropriate
        // rows.
        switch (sUriMatcher.match(uri)) {
            case VIDEOS:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        VideoContract.VideoInfoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case VIDEO:
                final String rowId =
                        ""
                                + VideoContract.VideoInfoEntry._ID
                                + " = '"
                                + ContentUris.parseId(uri)
                                + "'";

                retCursor = mOpenHelper.getReadableDatabase().query(
                        VideoContract.VideoInfoEntry.TABLE_NAME,
                        projection,
                        rowId,
                        null,
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }

        // Register to watch a content URI for changes.
        retCursor.setNotificationUri(getContext().getContentResolver(),
                uri);
        return retCursor;
    }

    /**
     * Method called to handle update requests from client
     * applications.
     */
    @Override
    public int update( Uri uri,
                      ContentValues cvs,
                      String selection,
                      String[] selectionArgs) {


        // Expand the selection if necessary.
        selection = addSelectionArgs(selection,
                selectionArgs,
                " OR ");
        // Just update a single row in the database.
        return mOpenHelper.getWritableDatabase().update
                (VideoContract.VideoInfoEntry.TABLE_NAME,
                        cvs,
                        addKeyIdCheckToWhereStatement(selection,
                                ContentUris.parseId(uri)),
                        selectionArgs);

    }

    /**
     * Method called to handle delete requests from client
     * applications.
     */
    @Override
    public int delete( Uri uri,
                      String selection,
                      String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int rowsDeleted;

        switch (sUriMatcher.match(uri)) {
            case VIDEOS:
                rowsDeleted = db.delete(
                        VideoContract.VideoInfoEntry.TABLE_NAME,
                        selection,
                        selectionArgs);

                break;
            case VIDEO:

            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }
        return rowsDeleted;
    }


    /**
     * Helper method that appends a given key id to the end of the
     * WHERE statement parameter.
     */
    private static String addKeyIdCheckToWhereStatement(String whereStatement,
                                                        long id) {
        String newWhereStatement;
        if (TextUtils.isEmpty(whereStatement))
            newWhereStatement = "";
        else
            newWhereStatement = whereStatement + " AND ";

        // Append the key id to the end of the WHERE statement.
        return newWhereStatement
                + VideoContract.VideoInfoEntry._ID
                + " = '"
                + id
                + "'";
    }

    /**
     * Return a selection string that concatenates all the
     *  selectionArgs for a given  selection using the given
     * operation.
     */
    private String addSelectionArgs(String selection,
                                    String [] selectionArgs,
                                    String operation) {
        // Handle the "null" case.
        if (selection == null
                || selectionArgs == null)
            return null;
        else {
            String selectionResult = "";

            // Properly add the selection args to the selectionResult.
            for (int i = 0;
                 i < selectionArgs.length - 1;
                 ++i)
                selectionResult += (selection
                        + " = ? "
                        + operation
                        + " ");

            // Handle the final selection case.
            selectionResult += (selection
                    + " = ?");

            // Output the selectionResults to Logcat.
            Log.d(TAG,
                    "selection = "
                            + selectionResult
                            + " selectionArgs = ");
            for (String args : selectionArgs)
                Log.d(TAG,
                        args
                                + " ");

            return selectionResult;
        }
    }




}
