
package com.sps.videoserviceapp;

import android.accounts.AccountAuthenticatorActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sps.videoserviceapp.rest.video.VideoService;

public class LoginActivity extends AccountAuthenticatorActivity {
    EditText mUsername;
    EditText mPassword;
    Button mLoginButton;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);

        mLoginButton = (Button) findViewById(R.id.login);
        mLoginButton.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                String user = mUsername.getText().toString().trim().toLowerCase();
                String password = mPassword.getText().toString().trim().toLowerCase();

                if (user.length() > 0 && password.length() > 0) {
                    LoginTask t = new LoginTask(LoginActivity.this);
                    t.execute(user, password);
                }
            }

        });
    }

    private class LoginTask extends AsyncTask<String, Void, Boolean> {
        Context mContext;
        ProgressDialog mDialog;

        LoginTask(Context c) {
            mContext = c;
            mLoginButton.setEnabled(false);

            mDialog = ProgressDialog.show(c, "", getString(R.string.authenticating), true, false);
            mDialog.setCancelable(true);
        }

        @Override
        public Boolean doInBackground(String... params) {
            String user = params[0];
            String pass = params[1];

            // Do something internetty
            try {
                VideoService.init(user, pass);
                VideoService.getService().getVideoList();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                return false;
            }

            return true;
        }

        @Override
        public void onPostExecute(Boolean result) {
            mLoginButton.setEnabled(true);
            mDialog.dismiss();
            if (result) {
                startActivity(new Intent(getApplicationContext(), EntryListActivity.class));
                finish();
            } else {
                Toast.makeText(LoginActivity.this, "Failed to login check credentials or connection", Toast.LENGTH_LONG).show();
            }
        }
    }
}


