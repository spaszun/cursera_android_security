package org.magnum.mobilecloud.video.repository;

import org.magnum.mobilecloud.video.model.Video;
import org.springframework.data.repository.CrudRepository;

public interface VideoRepo extends CrudRepository<Video, Long>
{
}
