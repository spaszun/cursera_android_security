package secure.video.upload.operations;


import android.app.Activity;
import android.content.ContentValues;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.Collection;

import secure.video.upload.R;
import secure.video.upload.activities.LoginActivity;
import secure.video.upload.model.mediator.webdata.AverageVideoRating;
import secure.video.upload.model.mediator.webdata.SecuredRestException;
import secure.video.upload.model.mediator.webdata.Video;
import secure.video.upload.provider.VideoContract;
import secure.video.upload.utils.ConfigurableOps;
import secure.video.upload.utils.GenericAsyncTask;
import secure.video.upload.utils.GenericAsyncTaskOps;
import secure.video.upload.utils.PersProgressDialog;
import secure.video.upload.model.mediator.VideoMediator;

public class LoginOps implements ConfigurableOps, GenericAsyncTaskOps<String[], Integer, Collection<Video>>
{
    private final String TAG = getClass().getSimpleName();

    private WeakReference<LoginActivity> mActivity;

    private String mUser;
    private String mPassword;
    private boolean mAttemptedLogin = false;
    private boolean mLoginResult;
    private Collection<Video> mResults;

    private GenericAsyncTask<String[], Integer, Collection<Video>, LoginOps> mAsyncTask;

    /**
     * Default constructor that's needed by the GenericActivity framework
     */
    public LoginOps()
    {
    }

    @Override
    public void onConfiguration (Activity activity, boolean firstTimeIn)
    {
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d (TAG, "onConfiguration() called the " + time + " with activity = " + activity);

        mActivity = new WeakReference<>((LoginActivity) activity);

        if (firstTimeIn) {
            // Nothing to do for now
        }
        else {
            updateResultsDisplay();
        }
    }

    private void updateResultsDisplay()
    {
//        if (mAttemptedLogin) {
//            publishProgress (PersProgressDialog.OPERATION_COMPLETED);
//            if (mLoginResult) {
//                mActivity.get().notifySuccessfulLogin();
//            }
//            else {
//                mActivity.get().notifyFailedLogin (R.string.loginFailed);
//            }
//        }
        if (mResults != null) {
            publishProgress (PersProgressDialog.OPERATION_COMPLETED);
            mActivity.get().notifySuccessfulLogin (mResults);
        }
        else {
            if (mAttemptedLogin) {
                if (!mLoginResult) {
                    publishProgress (PersProgressDialog.OPERATION_COMPLETED);
                    mActivity.get().notifyFailedLogin (R.string.loginFailed);
                }
            }
        }
    }

    public void login (String user, String password)
    {
        if (mAsyncTask != null) {
            mAsyncTask.cancel (true);
        }

        mUser = user;
        mPassword = password;

        String[] params = new String[3];
        params[0] = user;
        params[1] = password;
        mAsyncTask = new GenericAsyncTask<>(this);
        mAsyncTask.execute (params);
    }

    @Override
    public void publishProgress (int progress)
    {
        mActivity.get().notifyProgressUpdate (progress, R.string.loginDialogTitle,
                R.string.loginDialogExpl);
    }

    @Override
    public Collection<Video> doInBackground (String... param)
    {
        Log.i (TAG, "Started login doInBackground");
        Collection<Video> videos;

        try {
            VideoMediator.connect (param[0], param[1]);

            videos = VideoMediator.get().getVideoList();

            // Deleting all the rows in the db
            int rows = mActivity.get().getContentResolver().delete (VideoContract.VideoEntry.CONTENT_URI,
                    null, null);
            Log.d (TAG, "Deleted " + rows + " rows from the db");

            ContentValues[] cvs = new ContentValues[videos.size()];
            int i=0;
            for (Video video : videos) {
                AverageVideoRating videoRating = VideoMediator.get().getVideoRating(video.getId());
                Log.d (TAG, "Rating for " + video.getId() + ": " + videoRating.getRating() + " for " + videoRating.getTotalRatings());
                video.setAverageVideoRating (videoRating);

                ContentValues cv = new ContentValues();
                cv.put (VideoContract.VideoEntry.COLUMN_ID, video.getId());
                cv.put (VideoContract.VideoEntry.COLUMN_TITLE, video.getTitle());
                cv.put (VideoContract.VideoEntry.COLUMN_DURATION, video.getDuration());
                String contentType = video.getContentType();
                cv.put (VideoContract.VideoEntry.COLUMN_CONTENT_TYPE, (contentType == null ? "" : contentType));
                String dataUrl = video.getUrl();
                cv.put (VideoContract.VideoEntry.COLUMN_DATA_URL, (dataUrl == null ? "" : dataUrl));
                if (video.getAverageVideoRating() != null) {
                    cv.put (VideoContract.VideoEntry.COLUMN_STAR_RATING, video.getAverageVideoRating().getRating());
                    cv.put (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS, video.getAverageVideoRating().getTotalRatings());
                }
                else {
                    cv.put (VideoContract.VideoEntry.COLUMN_STAR_RATING, 0);
                    cv.put (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS, 0);
                }
                cv.put (VideoContract.VideoEntry.COLUMN_DATA_LOCAL_URI, "");
                cvs[i] = cv;
                i++;
            }
            rows = mActivity.get().getContentResolver().bulkInsert (VideoContract.VideoEntry.CONTENT_URI, cvs);
            Log.d (TAG, "Added " + rows + " rows to the db");
        }
        catch (Exception e) {
            Log.e (TAG, "Impossible to login", e);
            publishProgress (PersProgressDialog.OPERATION_COMPLETED);
            return null;
        }

        return videos;
    }

    @Override
    public void onPostExecute (Collection<Video> videos, String[] param)
    {
        mAttemptedLogin = true;
        if (videos != null) {
            mLoginResult = true;
        }
        mResults = videos;
        publishProgress (PersProgressDialog.OPERATION_COMPLETED);
        Log.i (TAG, "Finished login execution");
        updateResultsDisplay();
    }
}
