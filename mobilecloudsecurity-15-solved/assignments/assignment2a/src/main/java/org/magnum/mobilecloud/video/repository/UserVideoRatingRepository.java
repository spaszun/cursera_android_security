package org.magnum.mobilecloud.video.repository;

import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by Q1SW on 17-08-2015.
 */
public interface UserVideoRatingRepository extends CrudRepository<UserVideoRating, Long> {

    Integer countByVideoId(Long videoId);



    @Query("select avg(e.rating) from org.magnum.mobilecloud.video.model.UserVideoRating e where e.videoId = :videoId")
    Double getAvgRating(@Param("videoId") Long videoId);

    UserVideoRating findOneByVideoIdAndUser(Long videoId, String user);


}
