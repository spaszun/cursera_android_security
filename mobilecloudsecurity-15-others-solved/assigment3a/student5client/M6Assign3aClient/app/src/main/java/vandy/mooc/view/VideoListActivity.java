package vandy.mooc.view;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import vandy.mooc.MVP;
import vandy.mooc.R;
import vandy.mooc.common.GenericActivity;
import vandy.mooc.common.Utils;
import vandy.mooc.model.mediator.provider.VideoContract.VideoInfoEntry;
import vandy.mooc.presenter.VideosPresenter;
import vandy.mooc.view.ui.FloatingActionButton;
import vandy.mooc.view.ui.VideoAdapter;



/**
 * This Activity can be used upload a selected video to a Video
 * Service and also displays a list of videos available at the Video
 * Service.  The user can record a video or get a video from gallery
 * and upload it.  It implements OnVideoSelectedListener that will
 * handle callbacks from the UploadVideoDialog Fragment.  It extends
 * GenericActivity that provides a framework for automatically
 * handling runtime configuration changes of an VideoOps object, which
 * plays the role of the "Presenter" in the MVP pattern.  The
 * VideoOps.View interface is used to minimize dependencies between
 * the View and Presenter layers.
 */
public class VideoListActivity
        extends GenericActivity<MVP.VideosRequiredViewOps,
        MVP.VideosProvidedPresenterOps,
        VideosPresenter>
        implements   MVP.VideosRequiredViewOps,
        LoaderManager.LoaderCallbacks<Cursor>,
        OnItemClickListener {

    /**
     * The Request Code needed in Implicit Intent to get Video from
     * Gallery.
     */
    private final int REQUEST_GET_VIDEO = 1;

    /**
     * The Floating Action Button that will show a Dialog Fragment to
     * upload Video when user clicks on it.
     */
    private FloatingActionButton mUploadVideoButton;

    /**
     * The ListView that contains a list of Videos available from
     * the Video Service.
     */
    private ListView mVideosList;

    /**
     * Cursor Adapter
     */
    private VideoAdapter mAdapter;

    /**
     * Factory method that makes the explicit intent another Activity
     * uses to call this Service.
     *
     * @param context
     * @return
     */
    public static Intent makeIntent(Context context) {
        return  new Intent(context, VideoListActivity.class);
    }


    /**
     * Hook method called when a new instance of Activity is created.
     * One time initialization code goes here, e.g., storing Views.
     *
     *            object that contains saved state information.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Initialize the default layout.
        setContentView(R.layout.video_list_activity);

        initializeViews();

        // Initialize Cursor loader to update video list
        getLoaderManager().initLoader(0, null, this);

        // invoke the special onCreate() method in
        // GenericActivity, passing the videoPresenter class
        super.onCreate(VideosPresenter.class, this);
    }


    /**
     * Initialize Views
     */
    private void initializeViews() {

        // Get reference to the ListView for displaying the results
        mVideosList = (ListView) findViewById(R.id.videoList);

        // Get reference to the Floating Action Button.
        mUploadVideoButton =
                (FloatingActionButton) findViewById(R.id.fabButton);

        mUploadVideoButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                displayChooserDialog();
            }
        });

        // Initialize the videos Adapter
        mAdapter = new VideoAdapter(this, null, 0);
        mVideosList.setAdapter(mAdapter);
        mVideosList.setOnItemClickListener(this);
    }


    private void displayChooserDialog() {

        final Intent videoGalleryIntent =
                new Intent(Intent.ACTION_GET_CONTENT)
                        .setType("video/*")
                        .putExtra(Intent.EXTRA_LOCAL_ONLY, true);

        final Intent recordVideoIntent =
                new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        final Intent chooserIntent =
                Intent.createChooser(videoGalleryIntent,
                        "Upload Video via");

        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                new Intent[]{
                        recordVideoIntent
                });

        startActivityForResult(chooserIntent, REQUEST_GET_VIDEO);

    }


    /**
     *  Hook method that is called when user resumes activity
     *  from paused state, onPause().
     */
    @Override
    protected void onResume() {
        // Call up to the superclass.
        super.onResume();
        getLoaderManager().restartLoader(0, null, this);
    }


    /**
     * Hook method called when an activity you launched exits, giving
     * you the requestCode you started it with, the resultCode it
     * returned, and any additional data from it.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        // Check if the Result is Ok and upload the Video to the Video
        // Service.
        if ((resultCode == Activity.RESULT_OK) &&
                requestCode == REQUEST_GET_VIDEO) {
            // Inform User
            Utils.showToast(this, "Uploading video");
            getPresenter().uploadVideo(data.getData());

        } else {
            // inform user.
            Utils.showToast(this, "Could not get video to upload");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent,
                            View view,
                            int position,
                            long id) {

        Cursor cursor = mAdapter.getCursor();

        if ((cursor != null) && cursor.moveToPosition(position)) {
            Long videoId =
                    cursor.getLong
                            (cursor.getColumnIndex
                                    (VideoInfoEntry.COLUMN_VIDEO_ID));
            startActivity
                    (DisplayVideoActivity.makeVideoIntent
                            (getApplicationContext(), videoId));
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader
                (this,
                        VideoInfoEntry.CONTENT_URI,
                        null,
                        null,
                        null,
                        // @@
                        null);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader,
                               Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }


    @Override
    public void showGetVideosResult(Boolean success) {
        if (success == null) {
            Utils.showToast(this,
                    "Please connect to the Video Service");
            super.finish();
        }
        else {
            Utils.showToast(this,
                    "Videos available from the Video Service");
        }
    }


    /**
     * Hook method called to initialize the contents of the Activity's
     * standard options menu.
     *
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it
        // is present.
        getMenuInflater().inflate(R.menu.video_list,
                menu);
        return true;
    }

    /**
     * Hook method called whenever an item in your options menu is
     * selected
     *
     * @param item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
