package vandy.mooc.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import vandy.mooc.MVP;
import vandy.mooc.R;
import vandy.mooc.common.GenericActivity;
import vandy.mooc.common.Utils;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.services.DownloadVideoService;
import vandy.mooc.presenter.DisplayVideoPresenter;
import vandy.mooc.view.ui.FloatingActionButton;

/**
 * Created by roy on 7/17/2015.
 */
public class DisplayVideoActivity
        extends GenericActivity <MVP.DisplayVideoRequiredViewOps,
        MVP.DisplayVideoProvidedPresenterOps,
        DisplayVideoPresenter>
        implements RatingBar.OnRatingBarChangeListener,
        MVP.DisplayVideoRequiredViewOps {


    private static final String KEY_VIDEO_ID = "video_id";
    private static final long DEFAULT_VIDEO_ID = 0;

    /**
     * The Broadcast Receiver that registers itself to receive the
     * result from DownloadVideoService when a video upload completes.
     */
    private DownloadResultReceiver mDownloadResultReceiver;

    private FloatingActionButton mPlayVideoButton;

    private FloatingActionButton mDownloadVideoButton;

    private Animation mFadeInAnimation;

    private ImageView mVideoImg;

    private TextView mTitleText;

    private RatingBar mRatingBar;

    private Video mVideo;

    private long mVideoId;

    private String mFilePath;

    /**
     * Factory method that makes the explicit intent another Activity
     * uses to call this Service.
     *
     * @param context
     * @return
     */
    public static Intent makeVideoIntent(Context context,
                                         Long videoId) {
        return new Intent(context, DisplayVideoActivity.class)
                .putExtra(KEY_VIDEO_ID, videoId);
    }


    /**
     * Hook method called when a new instance of Activity is created.
     * One time initialization code goes here, e.g., storing Views.
     * <p/>
     * object that contains saved state information.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Log.d("DisplayAct", "Starting OnCreate");

        // Initialize the default layout.
        setContentView(R.layout.display_video_activity);

        Log.d("DisplayAct", "Set Content View Complete");

        super.onCreate(DisplayVideoPresenter.class, this);

        Log.d("DisplayAct", "Second onCreateCalled");

        mVideoId = getIntent().getLongExtra(KEY_VIDEO_ID,
                DEFAULT_VIDEO_ID);

        Log.d("DisplayAct", "Second onCreateCalled");

        getActionBar().hide();

        Log.d("DisplayAct", "getActionBar().hide)()");

        initializeViews();

        Log.d("DisplayAct", "InitializeViews() complete");

        mDownloadResultReceiver = new DownloadResultReceiver();

        Log.d("DisplayAct", "mDownloadRcr set");

        getAndDisplayVideo();

        Log.d("DisplayAct", "getAndDisplayVideo() complete");

    }

    /**
     * Initialize Views
     */
    private void initializeViews() {

        // Get reference to the ListView for displaying the results
        // entered.
        mVideoImg = (ImageView) findViewById(R.id.imageView1);

        mTitleText = (TextView) findViewById(R.id.textView1);

        mRatingBar = (RatingBar) findViewById(R.id.ratingBar1);

        mPlayVideoButton = (FloatingActionButton)
                findViewById(R.id.playVideoButton);

        mDownloadVideoButton = (FloatingActionButton)
                findViewById(R.id.downloadVideoButton);

        mFadeInAnimation =
                AnimationUtils.loadAnimation(this,
                        android.R.anim.fade_in);

        mDownloadVideoButton.setOnClickListener(null);

        mPlayVideoButton.setOnClickListener(new PlayVideoListener());
    }

    private class PlayVideoListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            if (mFilePath != null)
                startVideoPlayingActivity
                        (Uri.parse(mFilePath));
            else
                startVideoPlayingActivity
                        (Uri.parse(mVideo.getUrl()));
        }
    }


    private void getAndDisplayVideo() {
        mVideo = getPresenter().getVideoById(mVideoId);
        mTitleText.setText(mVideo.getTitle());
        mRatingBar.setRating((float) mVideo.getRating());
        mFilePath = getPresenter().getPathByName(mVideo.getTitle());
        setImageBitmap();
    }


    /**
     * Hook method that is called when user resumes activity
     * from paused state, onPause().
     */
    @Override
    protected void onResume() {
        // Call up to the superclass.
        super.onResume();

        IntentFilter intentFilter = new IntentFilter
                (DownloadVideoService.ACTION_DOWNLOAD_SERVICE_RESPONSE);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mDownloadResultReceiver,
                        intentFilter);
    }


    /**
     * Hook method that gives a final chance to release resources and
     * stop spawned threads.  onDestroy() may not always be
     * called-when system kills hosting process.
     */
    @Override
    protected void onPause() {
        // Call onPause() in superclass.
        super.onPause();
        // Unregister BroadcastReceiver.
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mDownloadResultReceiver);
    }


    /**
     * The Broadcast Receiver that registers itself to receive result
     * from UploadVideoService.
     */
    private class DownloadResultReceiver
            extends BroadcastReceiver {
        /**
         * Hook method that's dispatched when the UploadService has
         * uploaded the Video.
         */
        @Override
        public void onReceive(Context context,
                              Intent intent) {
            // Starts an AsyncTask to get fresh Video list from the
            // Video Service.
            final int status =
                    intent.getIntExtra(DownloadVideoService.KEY_STATUS, 0);
            final long receivedVideoId = intent.getLongExtra
                    (DownloadVideoService.KEY_DOWNLOAD_VIDEO_ID,
                            DownloadVideoService.DEFAULT_VIDEO_ID);
            if (status == DownloadVideoService.RESULT_OK
                    && receivedVideoId == mVideoId) {
                mDownloadVideoButton.hideFloatingActionButton();
                mFilePath = intent.getStringExtra
                        (DownloadVideoService.KEY_FILE_PATH);
            }
            setImageBitmap();
        }
    }


    @SuppressWarnings("deprecation")
    private void setImageBitmap() {

        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        final int width = metrics.widthPixels;
        if (mFilePath != null) {
            new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Void... params) {
                    final Bitmap thumbnail =
                            ThumbnailUtils.createVideoThumbnail
                                    (mFilePath,
                                            MediaStore.Images.Thumbnails.MINI_KIND);
                    return Bitmap.createScaledBitmap(thumbnail,
                            width,
                            350,
                            false);
                }

                protected void onPostExecute(Bitmap result) {
                    if (result != null) {
                        mVideoImg.setImageBitmap(result);
                        mVideoImg.startAnimation(mFadeInAnimation);
                    }
                }
            }.execute();
        } else {
            mDownloadVideoButton.setVisibility(View.VISIBLE);
            mDownloadVideoButton.setFloatingActionButtonDrawable
                    (getResources().getDrawable(R.drawable.ic_video));
            mDownloadVideoButton.setOnClickListener(new DownloadVideoListener());
        }
    }

    private class DownloadVideoListener
            implements OnClickListener {
        @SuppressWarnings("deprecation")
        @Override
        public void onClick(View v) {
            mDownloadVideoButton.setOnClickListener(null);
            mDownloadVideoButton
                    .setFloatingActionButtonDrawable
                            (getResources().getDrawable
                                    (R.drawable.ic_action_download));
            getPresenter().downloadVideo(mVideoId, mVideo.getTitle());
        }
    }

    public void startVideoPlayingActivity(Uri videoLocation) {

        Intent videoIntent =
                new Intent(Intent.ACTION_VIEW)
                        .setDataAndType(videoLocation, "video/*");

        if (videoIntent.resolveActivity(getPackageManager()) != null)
            startActivity(videoIntent);
    }

    @Override
    public void onRatingChanged(final RatingBar ratingBar,
                                float ratingFloat,
                                boolean fromUser) {
        if (fromUser)
            ratingBar.setRating(ratingFloat);
    }

    @Override
    public void updateRating(long videoId, double ratingResult) {
        if (videoId != 0) {
            mRatingBar.setRating((float)ratingResult);
            Utils.showToast(DisplayVideoActivity.this,
                    "Rating Updated...");
        } else {
            Utils.showToast(DisplayVideoActivity.this,
                    "Rating could not be Updated...");
        }

    }
}
