package org.magnum.mobilecloud.video.model;

public class VideoRating {
    
    private int rating;

    public VideoRating() {
    }

    public VideoRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String toString() {
        return String.valueOf(rating);
    }
}
