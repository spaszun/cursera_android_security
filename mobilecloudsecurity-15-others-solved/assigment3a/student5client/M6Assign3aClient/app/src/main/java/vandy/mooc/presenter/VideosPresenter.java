package vandy.mooc.presenter;

import android.net.Uri;

import java.lang.ref.WeakReference;

import vandy.mooc.MVP;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;
import vandy.mooc.model.mediator.VideoDataMediator;
import vandy.mooc.model.services.UploadVideoService;

public class VideosPresenter
    implements  GenericAsyncTaskOps<Void, Void, Boolean>,
                MVP.VideosProvidedPresenterOps {

    private static final String TAG =
            VideosPresenter.class.getSimpleName();

    private WeakReference<MVP.VideosRequiredViewOps> mVideosView;

    private GenericAsyncTask<Void,
                            Void,
                            Boolean,
                            VideosPresenter> mAsyncTask;

    VideoDataMediator mVideoMediator;

    public VideosPresenter() {
    }

    @Override
    public void onCreate(MVP.VideosRequiredViewOps view) {
        mVideosView = new WeakReference<>(view);
        mVideoMediator = new VideoDataMediator
                (mVideosView.get().getApplicationContext());
        mAsyncTask = new GenericAsyncTask<>(this);
        mAsyncTask.execute();
    }

    public void onConfigurationChange(MVP.VideosRequiredViewOps view) {
        mVideosView = new WeakReference<>(view);
    }


    public void onDestroy(boolean isChangingConfigurations) {
        // No-Op
    }

    @Override
    public Boolean doInBackground(Void... ignore) {
        return mVideoMediator.getAndSaveVideoMetadata();
    }

    @Override
    public void onPostExecute(Boolean success) {
        mVideosView.get().showGetVideosResult(success);
    }


    public void uploadVideo(Uri videoUri) {
        mVideosView.get()
                .getApplicationContext()
                .startService(UploadVideoService.makeIntent
                   (mVideosView.get().getApplicationContext(), videoUri));

    }

}
