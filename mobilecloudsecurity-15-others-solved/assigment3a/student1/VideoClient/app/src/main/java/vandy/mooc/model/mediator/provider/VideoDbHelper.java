package vandy.mooc.model.mediator.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Manages a local database for Video metadata.
 */
public class VideoDbHelper extends SQLiteOpenHelper {
    /**
     * if database schema is changed, the database version must be
     * incremented.
     */
    private static int DATABASE_VERSION = 1;

    /**
     * Database name.
     */
    private static String DATABASE_NAME =
            "video_info.db";

    /**
     * Constructor for VideoDbHelper.
     *
     * @param context
     */
    public VideoDbHelper(Context context) {
        super(context,
                DATABASE_NAME,
                null,
                DATABASE_VERSION);
    }

    /**
     * Hook method called when Database is created.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Define an SQL string that creates a table to hold Video
        // metadata.
        final String SQL_CREATE_VIDEO_INFO_TABLE =
                "CREATE TABLE " + VideoContract.VideoInfoEntry.TABLE_NAME + " ("
                        + VideoContract.VideoInfoEntry._ID + " INTEGER PRIMARY KEY, "
                        + VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID + " INTEGER NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_TITLE + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_DURATION + " INTEGER NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_DATA_URL + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_RATING_COUNT + " REAL,"
                        + VideoContract.VideoInfoEntry.COLUMN_USER_COUNT + " INTEGER,"

                        // To assure that application have just one video entry
                        // it's created with UNIQUE constraint with REPLACE
                        // strategy.
                        + " UNIQUE (" + VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID + ", "
                        + VideoContract.VideoInfoEntry.COLUMN_TITLE
                        + ") ON CONFLICT REPLACE);";

        // Create the table.
        sqLiteDatabase.execSQL(SQL_CREATE_VIDEO_INFO_TABLE);
    }

    /**
     * Hook method called when the database is upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,
                          int oldVersion,
                          int newVersion) {
        // Delete the existing tables.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "
                + VideoContract.VideoInfoEntry.TABLE_NAME);
        // Create the new tables.
        onCreate(sqLiteDatabase);
    }
}