package org.magnum.mobilecloud.video.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by
 * @author kaido on 26.08.2015.
 *
 */
@Repository
public interface VideoRepository extends CrudRepository <Video, Long> {

//    public Video findByVideoIdAndUser(long id, String username);

}
