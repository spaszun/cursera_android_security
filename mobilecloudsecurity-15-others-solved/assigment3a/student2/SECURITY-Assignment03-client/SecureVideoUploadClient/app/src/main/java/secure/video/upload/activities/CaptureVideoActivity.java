package secure.video.upload.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import secure.video.upload.R;
import secure.video.upload.utils.Constants;
import secure.video.upload.utils.LifecycleLoggingActivity;

public class CaptureVideoActivity extends LifecycleLoggingActivity
{
    private MediaRecorder mRecorder;

    private SurfaceHolder mHolder;

    private boolean mRecording = false;

    private String mVideoFile;

    public static final String TAG_VIDEO_DURATION = VideosInfoActivity.TAG_VIDEO_DURATION;
    public static final String TAG_VIDEO_DATA_FILE = "VIDEO_DATA_FILE";
    public static final int CAPTURE_VIDEO_REQUEST = 11;

    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        Intent captureVideoIntent = new Intent (MediaStore.ACTION_VIDEO_CAPTURE);
        captureVideoIntent.putExtra (MediaStore.Video.Media.MIME_TYPE, "video/mp4");

        File videoFile = null;
        try {
            videoFile = createEmptyVideoFile (Constants.APP_FOLDER);
        }
        catch (IOException e) {
            Log.e (TAG, "Impossible to save video", e);
            Toast.makeText (this, "Impossible to write on the device", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent (this, VideosInfoActivity.class);
            setResult (RESULT_CANCELED, intent);
            finish();
            return;
        }

        mVideoFile = videoFile.getAbsolutePath();
        captureVideoIntent.putExtra (MediaStore.EXTRA_OUTPUT, Uri.fromFile (videoFile));
        if (captureVideoIntent.resolveActivity (getPackageManager()) != null) {
            startActivityForResult (captureVideoIntent, CAPTURE_VIDEO_REQUEST);
        }
    }

    private File createEmptyVideoFile (String appFolder) throws IOException
    {
        String fileName = new SimpleDateFormat ("yyyyMMdd_HHmmss").format (new Date()) + ".mp4";
        File storageDir = Environment.getExternalStorageDirectory();
        storageDir.mkdirs();
        File appDir = new File (storageDir.getAbsolutePath() + "/" + appFolder + "/upload/");
        appDir.mkdir();
        File dstFile = new File (appDir.getAbsolutePath(), fileName);
        dstFile.createNewFile();
        Log.d (TAG, "File text length " + dstFile.length() + " path " + dstFile.getAbsolutePath());

        return dstFile;
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent intent)
    {
        if (requestCode == CAPTURE_VIDEO_REQUEST) {
            Intent answerIntent = new Intent(); // Going back to VideosInfoActivity
            if (resultCode == RESULT_OK) {
                // Computing video duration
                MediaPlayer mp = MediaPlayer.create (this, Uri.parse (mVideoFile));
                int duration = mp.getDuration();
                mp.release();
                //answerIntent.putExtra (TAG_VIDEO_DURATION, 0);
                answerIntent.putExtra (TAG_VIDEO_DURATION, duration);
                answerIntent.putExtra (TAG_VIDEO_DATA_FILE, mVideoFile);
                setResult(RESULT_OK, answerIntent);
            }
            else {
                Toast.makeText (this, "Impossible to create the video", Toast.LENGTH_SHORT).show();
                setResult (RESULT_CANCELED, answerIntent);
            }
            finish();
        }
    }
}
