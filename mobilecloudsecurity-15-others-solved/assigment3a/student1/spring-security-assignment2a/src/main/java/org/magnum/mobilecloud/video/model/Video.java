package org.magnum.mobilecloud.video.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * A simple object to represent a video and its URL for viewing.
 * 
 * You must annotate this object to make it a JPA entity.
 * 
 * 
 * Feel free to modify this with whatever other meta data that you want, such as
 * the
 * 
 * 
 * @author Jules, Mitchell
 */
@Entity
public class Video {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String title;
    private String url;
    private long duration;
    private String contentType;

    private double ratingCount;
    private int userCount;

    // We don't want to bother unmarshalling or marshalling
    // any owner data in the JSON. Why? We definitely don't
    // want the client trying to tell us who the owner is.
    // We also might want to keep the owner secret.
    @JsonIgnore
    private String owner;

    public Video() {
    }

    public Video(String owner, String name, String url, long duration, String contentType, double ratingCount,
	    int userCount) {
	super();
	this.owner = owner;
	this.title = name;
	this.url = url;
	this.duration = duration;
	this.contentType = contentType;
	this.ratingCount = ratingCount;
	this.userCount = userCount;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public long getDuration() {
	return duration;
    }

    public void setDuration(long duration) {
	this.duration = duration;
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public String getContentType() {
	return contentType;
    }

    public void setContentType(String contentType) {
	this.contentType = contentType;
    }

    public double getRatingCount() {
	return ratingCount;
    }

    public void setRatingCount(double ratingCount) {
	this.ratingCount = ratingCount;
    }

    public int getUserCount() {
	return userCount;
    }

    public void setUserCount(int userCount) {
	this.userCount = userCount;
    }

    public String getOwner() {
	return owner;
    }

    public void setOwner(String owner) {
	this.owner = owner;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     * 
     */
    @Override
    public int hashCode() {
	// Google Guava provides great utilities for hashing
	return Objects.hashCode(title, url, duration, owner);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     * 
     */
    @Override
    public boolean equals(Object obj) {
	if (obj instanceof Video) {
	    Video other = (Video) obj;
	    // Google Guava provides great utilities for equals too!
	    return Objects.equal(title, other.title) && Objects.equal(url, other.url)
		    && Objects.equal(owner, other.owner) && duration == other.duration;
	} else {
	    return false;
	}
    }

}