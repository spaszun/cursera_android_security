package org.magnum.mobilecloud.video.controller;

import com.google.common.collect.Lists;

import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRatingRepository;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.nio.file.AccessDeniedException;
import java.security.Principal;
import java.util.Collection;
import java.util.OptionalDouble;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class VideoController {

    @Autowired
    private VideoRepository repo;

    @Autowired
    private UserVideoRatingRepository videoRatingRepo;

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}", method = RequestMethod.GET)
    public Video getVideoById(@PathVariable(VideoSvcApi.ID_PARAMETER) long id) {
	return repo.findOne(id);
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.GET)
    public @ResponseBody Collection<Video> getVideoList() {
	return Lists.newArrayList(repo.findAll());
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.POST)
    public @ResponseBody Video addVideo(@RequestBody Video v, Principal p) throws AccessDeniedException {

	Video existing = repo.findOne(v.getId());
	if (existing != null && p.getName().equals(existing.getOwner())) {
	    throw new AccessDeniedException("You do not own this video");
	}

	// TODO: improve this code segment
	v.setOwner(p.getName());
	v = repo.save(v);
	String target = getVideoUrl(v);
	v.setUrl(target);

	return v;
    }

    private UserVideoRating getUserRatingOfVideo(long videoId, String user) {
	Collection<UserVideoRating> ratings = videoRatingRepo.findByUserAndVideoId(user, videoId);

	UserVideoRating rating = (ratings != null && !ratings.isEmpty()) ? ratings.iterator().next()
		: new UserVideoRating(videoId, 0, user);

	return rating;
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating/{rating}", method = RequestMethod.POST)
    public @ResponseBody AverageVideoRating rateVideo(@PathVariable("id") long id, @PathVariable("rating") int rating,
	    Principal p, HttpServletResponse resp) {

	// Get the video
	Video v = repo.findOne(id);
	String user = p.getName();

	if (v != null) {
	    UserVideoRating userRating = getUserRatingOfVideo(id, user);
	    userRating.setRating(rating);
	    videoRatingRepo.save(userRating);

	    return averageRatingOfVideo(id, resp);
	} else {
	    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
	    return null;
	}
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating", method = RequestMethod.GET)
    public @ResponseBody AverageVideoRating averageRatingOfVideo(@PathVariable("id") long id, HttpServletResponse resp) {
	// Get the video
	Video v = repo.findOne(id);

	if (v != null) {

	    Collection<UserVideoRating> ratings = videoRatingRepo.findByVideoId(v.getId());
	    OptionalDouble avg = ratings.stream().mapToDouble(r -> r.getRating()).average();
	    double rating = (avg.isPresent()) ? avg.getAsDouble() : -1;
	    AverageVideoRating ratingData = new AverageVideoRating(rating, v.getId(), ratings.size());
	    return ratingData;
	} else {
	    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
	    return null;
	}
    }

    private String getUrlBaseForLocalServer() {
	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
		.getRequest();
	String base = "https://" + "192.168.2.177"
		+ ((request.getServerPort() != 80) ? ":" + request.getServerPort() : "");
	return base;
    }

    public String getVideoUrl(Video v) {
	String base = getUrlBaseForLocalServer();
	return base + VideoSvcApi.VIDEO_DATA_PATH.replace("{" + VideoSvcApi.ID_PARAMETER + "}", "" + v.getId());
    }

}