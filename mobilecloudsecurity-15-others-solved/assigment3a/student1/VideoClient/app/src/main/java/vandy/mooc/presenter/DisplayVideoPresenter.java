package vandy.mooc.presenter;

import android.util.Log;

import java.lang.ref.WeakReference;

import vandy.mooc.MVP;
import vandy.mooc.common.ContextView;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;
import vandy.mooc.model.mediator.VideoDataMediator;
import vandy.mooc.model.mediator.webdata.AverageVideoRating;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.services.DownloadVideoService;
import vandy.mooc.utils.VideoMediaStoreUtils;

/**
 * This class implements all the operations related to displaying detailed
 * metadata about a video.  It plays the role of the "Presenter" in
 * the Model-View-Presenter (MVP) pattern.  It extends
 * GenericAsyncTaskOps so its doInBackground() method runs in a
 * background task.  It implements
 * MVP.DisplayVideoProvidedPresenterOps so it can be created/managed by the
 * GenericActivity framework and to decouple the Presenter and View
 * layers in the MVP pattern.
 */
public class DisplayVideoPresenter
        implements GenericAsyncTaskOps<Integer, Void, Video>,
        MVP.DisplayVideoProvidedPresenterOps {

    /**
     * Debugging tag used by the Android logger.
     */
    protected final static String TAG =
            DisplayVideoPresenter.class.getSimpleName();

    /**
     * This interface defines the minimum interface needed by the
     * DisplayVideoOps class in the "Presenter" layer to interact with
     * the DisplayVideoActivity in the "view" layer.
     */
    public interface View extends ContextView {
        /**
         * Update the Rating of Video in DisplayVideoActivity.
         *
         * @param video
         */
        public void updateRating(Video video);
    }

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<MVP.DisplayVideoRequiredViewOps> mDisplayVideoView;

    /**
     * The GenericAsyncTask used to add rating fro videos via
     * VideoService.
     */
    private GenericAsyncTask<Integer,
            Void,
            Video,
            DisplayVideoPresenter> mAsyncTask;

    /**
     * VideoDataMediator mediates the communication between Video
     * Service and the VideoContentProvider.
     */
    VideoDataMediator mVideoMediator;

    /**
     * Id of the video whose metadata is being displayed.
     */
    private long mVideoId;

    /**
     * Default constructor that's needed by the GenericActivity
     * framework.
     */
    public DisplayVideoPresenter() {
    }

    /**
     * Hook method called when a new instance of DisplayVideoPresenter is
     * created.  One time initialization code goes here, e.g., storing
     * a WeakReference to the View layer and initializing the Model
     * layer.
     *
     * @param view A reference to the View layer.
     */
    @Override
    public void onCreate(MVP.DisplayVideoRequiredViewOps view) {
        // Set the WeakReference.
        mDisplayVideoView = new WeakReference<>(view);

        // Create VideoDataMediator that will mediate the
        // communication between Server and VideoContentProvider.
        mVideoMediator =
                new VideoDataMediator
                        (mDisplayVideoView.get().getActivityContext());
    }

    /**
     * Called after a runtime configuration change occurs to finish
     * the initialization steps.
     */
    @Override
    public void onConfigurationChange(MVP.DisplayVideoRequiredViewOps view) {
        Log.d(TAG,
                "onConfigurationChange() called with view = "
                        + view);

        // (Re)set the mVideosView WeakReference.
        mDisplayVideoView = new WeakReference<>(view);
    }

    /**
     * Hook method called to shutdown the Model layer.
     *
     * @param isChangingConfigurations True if a runtime configuration triggered the onDestroy() call.
     */
    @Override
    public void onDestroy(boolean isChangingConfigurations) {
        // No-op.
    }

    /**
     * Get file path of video in local storage by video name.
     *
     * @param videoName
     * @return filePath of video
     */
    public String getPathByName(String videoName) {
        return VideoMediaStoreUtils.getPathByName
                (mDisplayVideoView.get()
                                .getApplicationContext(),
                        videoName);
    }

    /**
     * Get video from Id.
     *
     * @param videoId
     * @return video
     */
    public Video getVideoById(long videoId) {
        Video video = mVideoMediator.getVideo(videoId);
        AverageVideoRating rating = mVideoMediator.getRating(videoId);
        if(rating != null) {
            video.setRatingCount(rating.getRating());
            video.setUserCount(rating.getTotalRatings());
        }
        return video;
    }

    /**
     * Download the video having given videoId and video name.
     *
     * @param videoId
     * @param videoName
     */
    public void downloadVideo(long videoId,
                              String videoName) {
        // Sends an Intent command to the UploadVideoService.
        mDisplayVideoView.get().getApplicationContext().startService
                (DownloadVideoService.makeIntent
                        (mDisplayVideoView.get().getApplicationContext(),
                                videoId,
                                videoName));
    }

    /**
     * Runs an AsyncTask that updates the rating of video
     * in the Video Service.
     *
     * @param videoId
     * @param rating
     */
    public void addRating(long videoId,
                          int rating) {
        mVideoId = videoId;
        mAsyncTask = new GenericAsyncTask<>(this);
        mAsyncTask.execute(rating);
    }

    @Override
    public void getRating(long videoId) {

    }

    /**
     * Updates the video rating, which runs a two-way synchronous call
     * in a background thread to avoid blocking the UI thread.
     */
    @Override
    public Video doInBackground(Integer... params) {
        final int rating = params[0];
        return mVideoMediator.addRating(mVideoId,
                rating);
    }

    /**
     * Display the results in the UI Thread.
     *
     * @param result
     */
    @Override
    public void onPostExecute(Video result) {
        mDisplayVideoView.get().updateRating(result);
    }
}