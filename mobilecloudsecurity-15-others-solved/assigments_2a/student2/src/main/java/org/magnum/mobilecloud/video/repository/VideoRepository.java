package org.magnum.mobilecloud.video.repository;

import org.magnum.mobilecloud.video.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * An interface for a repository that can store Video
 * objects and allow them to be searched by title.
 */
@Repository
public interface VideoRepository extends JpaRepository<Video, Long> {
}
