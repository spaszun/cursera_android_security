package secure.video.upload.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.UUID;

import secure.video.upload.R;
import secure.video.upload.operations.UploadVideoOps;
import secure.video.upload.utils.GenericActivity;
import secure.video.upload.utils.PersProgressDialog;

public class UploadVideoActivity extends GenericActivity<UploadVideoOps>
{
    private EditText mTitle;
    private long mDuration;
    private String mVideoDataFile;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        setContentView (R.layout.activity_upload_video);

        mOpProgressDialog = new PersProgressDialog (UploadVideoActivity.this);

        mTitle = (EditText) findViewById (R.id.videoTitleEditText);
        mDuration = (long) getIntent().getIntExtra (CaptureVideoActivity.TAG_VIDEO_DURATION, 0);
        mVideoDataFile = getIntent().getStringExtra (CaptureVideoActivity.TAG_VIDEO_DATA_FILE);

        super.onCreate (savedInstanceState, UploadVideoOps.class);
    }

    public void notifyMediatorNotConnected()
    {
        Toast.makeText (this, "Impossible to download the video", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void uploadVideo (View view)
    {
        String title;
        if ((mTitle.getText() == null) || (mTitle.getText().toString().isEmpty())) {
            title = UUID.randomUUID().toString();
        }
        else {
            title = mTitle.getText().toString();
        }

        getOps().uploadVideo (title, mDuration, mVideoDataFile);
    }

    public void uploadFinished (String toastMessage)
    {
        Toast.makeText (this, toastMessage, Toast.LENGTH_SHORT).show();
        finish();
    }
}
