package org.magnum.mobilecloud.video.repository;

import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * An interface for a repository that can store Video
 * objects and allow them to be searched by title.
 */
@Repository
public interface RatingRepository extends JpaRepository<UserVideoRating, Long> {
    UserVideoRating findOneByUserAndVideoId(String user, Long VideoId);

    // Query that does all the work for finding the average rating for a video.
    // This query will throw an exception if no entries exist for the specified
    // video id; this case must be handled by the caller.
    @Query("SELECT new org.magnum.mobilecloud.video.model.AverageVideoRating(" +
            "coalesce(AVG(rating),0) as rating, " +
            "coalesce(MAX(videoId),videoId) as videoId, " +
            "coalesce(COUNT(rating),0) as totalRatings) " +
            "from #{#entityName} " +
            "where videoId=:videoId")
    AverageVideoRating findAverageVideoRatingByVideoId(@Param("videoId") long videoId);
}
