package vandy.mooc.model.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import vandy.mooc.model.mediator.VideoDataMediator;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DownloadVideoService extends IntentService {

    private static final String TAG = DownloadVideoService.class.getSimpleName();

    private VideoDataMediator mVideoMediator;

    /**
     * Manages the Notification displayed in System UI.
     */
    private NotificationManager mNotifyManager;

    /**
     * Builder used to build the Notification.
     */
    private NotificationCompat.Builder mBuilder;

    /**
     * It is used by Notification Manager to send Notifications.
     */
    private static final int NOTIFICATION_ID = 4;

    /**
     * Custom Action that will be used to send Broadcast to the VideoListActivity.
     */
    public static final String ACTION_DOWNLOAD_SERVICE_RESPONSE =
            "vandy.mooc.services.DownloadVideoService.RESPONSE";


    /**
     * Factory method that makes the explicit intent another Activity uses to call this Service.
     *
     * @param context
     * @param id identifier of the video to be downloaded
     * @return Intent for launching video download task in IntentService
     */
    public static Intent makeIntent(Context context, long id, String title) {
        return new Intent(context, DownloadVideoService.class)
                .putExtra("ID", id)
                .putExtra("TITLE", title);
    }


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */


    public DownloadVideoService() {
        super("DownloadVideoService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            startNotification();

            mVideoMediator = new VideoDataMediator();

            final long id = intent.getLongExtra("ID", 0);
            final String title = intent.getStringExtra("TITLE");

            Log.d(TAG, "Download video: ID = " + id + " Title = " + title);

            String downloadResult = mVideoMediator.downloadVideo(getApplicationContext(), id, title);

            Log.d(TAG, "Download result: " + downloadResult);

            finishNotification(downloadResult);
            sendBroadcast();
        }
    }


    /**
     * Send the Broadcast to Activity that the Video Upload is completed.
     */
    private void sendBroadcast() {
        // Use a LocalBroadcastManager to restrict the scope of this
        // Intent to the VideoClient application.
        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(new Intent(ACTION_DOWNLOAD_SERVICE_RESPONSE)
                        .addCategory(Intent.CATEGORY_DEFAULT));
    }


    private void startNotification() {
        // Gets access to the Android Notification Service.
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Create the Notification and set a progress indicator for an
        // operation of indeterminate length.
        mBuilder = new NotificationCompat
                .Builder(this)
                .setContentTitle("Video Download")
                .setContentText("Download in progress")
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setTicker("Downloading video")
                .setProgress(0, 0, true);

        // Build and issue the notification.
        mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    /**
     * Finish the Notification after the Video is downloaded.
     *
     * @param status
     */
    private void finishNotification(String status) {
        // When the loop is finished, updates the notification.
        mBuilder.setContentTitle(status)
                // Removes the progress bar.
                .setProgress(0, 0, false)
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setContentText("")
                .setTicker(status);

        // Build the Notification with the given Notification Id.
        mNotifyManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}
