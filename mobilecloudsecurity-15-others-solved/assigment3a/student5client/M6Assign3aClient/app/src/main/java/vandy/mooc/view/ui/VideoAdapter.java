package vandy.mooc.view.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import vandy.mooc.R;
import vandy.mooc.model.mediator.provider.VideoContract;

/**
 * Show the view for each Video's meta-data in a ListView.
 */
public class VideoAdapter extends CursorAdapter {

    public VideoAdapter (Context context, Cursor cursor, int idx) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context,
                        Cursor cursor,
                        ViewGroup parent) {
        return LayoutInflater
                .from(context)
                .inflate(R.layout.video_list_item,
                        parent,
                        false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view,
                         Context context,
                         Cursor cursor) {

        // Find fields to populate in inflated template
        TextView tvTitleVideo =
                (TextView) view.findViewById(R.id.tvVideoTitle);

        // Extract properties from cursor
        String videoTitle = cursor.getString
                (cursor.getColumnIndexOrThrow(VideoContract.VideoInfoEntry.COLUMN_TITLE));

        // Populate fields with extracted properties
        tvTitleVideo.setText(videoTitle);
    }



}
