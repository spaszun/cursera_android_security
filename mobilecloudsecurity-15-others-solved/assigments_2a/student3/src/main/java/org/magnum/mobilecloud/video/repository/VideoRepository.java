package org.magnum.mobilecloud.video.repository;

import org.magnum.mobilecloud.video.model.Video;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoRepository extends CrudRepository<Video, Long> {

}
