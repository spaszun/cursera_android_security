package vandy.mooc.model.mediator;

import android.content.Context;
import android.net.Uri;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedFile;
import vandy.mooc.model.mediator.provider.VideoCache;
import vandy.mooc.model.mediator.webdata.AverageVideoRating;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.mediator.webdata.VideoServiceProxy;
import vandy.mooc.model.mediator.webdata.VideoStatus;
import vandy.mooc.model.mediator.webdata.VideoStatus.VideoState;
import vandy.mooc.model.mediator.webdata.VideoSvc;
import vandy.mooc.utils.Constants;
import vandy.mooc.utils.VideoMediaStoreUtils;
import vandy.mooc.utils.VideoStorageUtils;

/**
 * Mediates communication between the Video Service, the local storage
 * on the Android device, and the Android Video Content Provider.  The
 * methods in this class can block for extended period of time when
 * they access the Video Service, so they should be called from a
 * background thread (e.g., via an AsyncTask) in the Presenter layer.
 */
public class VideoDataMediator {
    /**
     * Status code to indicate that file is successfully uploaded.
     */
    public static final String STATUS_UPLOAD_SUCCESSFUL =
            "Upload succeeded";

    /**
     * Status code to indicate that file upload failed due to large
     * video size.
     */
    public static final String STATUS_UPLOAD_ERROR_FILE_TOO_LARGE =
            "Upload failed: File size greater than 50MB";

    /**
     * Status code to indicate that file upload failed.
     */
    public static final String STATUS_UPLOAD_ERROR =
            "Upload failed";

    /**
     * Defines methods that communicate with the Video Service.
     */
    private VideoServiceProxy mVideoServiceProxy;

    /**
     * Gives access to application-specific resources.
     */
    private WeakReference<Context> mContext;

    /**
     * Defines methods that communicate with our Content Provider.
     */
    private VideoCache mVideoCache;

    /**
     * Constructor that initializes the VideoDataMediator.
     *
     * @param context
     */
    public VideoDataMediator(Context context) {
        // Initialize the Context.
        mContext = new WeakReference<>(context);

        // Initialize the VideoCache.
        mVideoCache = new VideoCache(mContext.get());

        // Initialize the VideoServiceProxy.
        mVideoServiceProxy = VideoSvc.getOrShowLogin(mContext.get());
    }

    /**
     * Downloads the Video having given Id and videoName.  This Id is
     * the unique ID of the Video in the Video Service.
     *
     * @param videoId
     * @param videoTitle
     * @return the filePath of stored Video.
     */
    public String downloadVideo(long videoId, String videoTitle) {
        try {
            // Request fro download and try to get the response from
            // Video Service.
            Response response =
                    mVideoServiceProxy.getData(videoId);

            // If the Response is success, then store the Video.
            if (response.getStatus() == 200) {
                // Store the video in Android's external Downloads
                // directory.
                File videoFile =
                        VideoStorageUtils.storeVideoInExternalDirectory
                                (mContext.get(),
                                        response,
                                        videoTitle);

                // Return the filePath of the stored Video.
                return videoFile.getAbsolutePath();
            }
        } catch (Exception e) {
            // Error occurred, return null.
            return null;
        }

        // Response is not success, return null.
        return null;
    }

    /**
     * Uploads the Video having the given Id.  This Id is the Id of
     * Video in Android Video Content Provider.
     *
     * @param videoUri Id of the Video to be uploaded.
     * @return result of the upload operation.
     * True - If the video is successfully uploaded.
     * False - If there was a failure while uploading Video.
     */
    public String uploadVideo(Uri videoUri) {
        // Get the path of video file from videoUri.
        String filePath =
                VideoMediaStoreUtils.getPath(mContext.get(),
                        videoUri);

        // Get the Video from Android Video Content Provider having
        // the given filePath.
        Video androidVideo =
                VideoMediaStoreUtils.getVideo(mContext.get(),
                        filePath);

        // Check if any such Video exists in Android Video Content
        // Provider.
        if (androidVideo != null) {
            // Prepare to Upload the Video data.

            // Create an instance of the file to upload.
            File videoFile = new File(filePath);

            // Check if the file size is less than the size of the
            // video that can be uploaded to the server.
            if (videoFile.length() < Constants.MAX_SIZE_MEGA_BYTE) {

                try {
                    // Add the metadata of the Video to the Video Service
                    // and get the resulting Video that contains
                    // additional meta-data (e.g., Id and ContentType)
                    // generated by the Video Service.
                    Video receivedVideo =
                            mVideoServiceProxy.addVideo(androidVideo);

                    // Check if the Server returns any Video metadata.
                    if (receivedVideo != null) {

                        // Finally, upload the Video data to the server
                        // and get the status of the uploaded video data.
                        VideoStatus status =
                                mVideoServiceProxy.uploadVideo
                                        (receivedVideo.getId(),
                                                new TypedFile(receivedVideo.getContentType(),
                                                        videoFile));

                        // Check if the Status of the Video or not.
                        if (status.getState() == VideoState.READY) {
                            // Video successfully uploaded.
                            mVideoCache.insert(receivedVideo);
                            return STATUS_UPLOAD_SUCCESSFUL;
                        }
                    }
                } catch (Exception e) {
                    return STATUS_UPLOAD_ERROR;
                }
            } else
                // Video can't be uploaded due to large video size.
                return STATUS_UPLOAD_ERROR_FILE_TOO_LARGE;
        }

        // Error occurred while uploading the video.
        return STATUS_UPLOAD_ERROR;
    }

    /**
     * Get the List of videos from Video Service and save it in Video
     * Content Provider.
     *
     * @return result of getting video operation.
     * True - If the Video is successfully loaded from server and saved
     * in Content Provider.
     * False - If there was a failure while loading Video from server or
     * while saving metadata in Content Provider.
     */
    public Boolean getAndSaveVideoMetadata() {
        try {
            // Get all videos from server.
            ArrayList<Video> videoList =
                    (ArrayList<Video>) mVideoServiceProxy.getVideoList();

            // Check if there any Videos.
            if (videoList != null) {
                // Insert all videos in Content Provider.
                mVideoCache.insertAllVideos(videoList);
                return true;
            } else
                // Error occurred while getting videos.
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Rates the Video having given videoId, rating.  This Id is the
     * unique ID of the Video in the Video Service.
     *
     * @param videoId
     * @param rating
     * @return Video that contains the updated video metadata.
     */
    public Video addRating(long videoId,
                           int rating) {
        try {
            // Update the rating if the Video and get the updated
            // video metadata.
            AverageVideoRating receivedAvgRatingVideo =
                    mVideoServiceProxy.addRating(videoId,
                            rating);

            // Check if the Server returns any metadata .
            if (receivedAvgRatingVideo != null) {

                Video mVideo = mVideoCache.getVideoById(videoId);
                mVideo.setRatingCount(receivedAvgRatingVideo.getRating());
                mVideo.setUserCount(receivedAvgRatingVideo.getTotalRatings());

                // Insert the video in Video Content Provider.
                mVideoCache.insert(mVideo);

                // Return the video received.
                return mVideo;
            } else
                // Error occurred while rating the video.
                return null;

        } catch (Exception e) {
            return null;
        }
    }

    public AverageVideoRating getRating(long videoId) {

        try {
            return mVideoServiceProxy.getRating(videoId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Get the Video metadata from the Video Content Provider having
     * given videoId. This Id is the unique Id of the Video in the Video
     * Service.
     *
     * @param videoId
     * @return video
     */
    public Video getVideo(long videoId) {
        return mVideoCache.getVideoById(videoId);
    }
}
