package org.magnum.mobilecloud.video.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// You might want to annotate this with Jpa annotations, add an id field,
// and store it in the database...
//
// There are also plenty of other solutions that do not require
// persisting instances of this...


@Entity

// We have unique composite index for videoId+user to find ratings by video and enforce uniqueness of rating

@Table( indexes = @Index(columnList="videoId,user", unique=true ) )
public class UserVideoRating {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private long videoId;

	private double rating;

	private String user;

	public UserVideoRating() {
	}

	public UserVideoRating(long videoId, double rating, String user) {
		super();
		this.videoId = videoId;
		this.rating = rating;
		this.user = user;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public long getVideoId() {
		return videoId;
	}

	public void setVideoId(long videoId) {
		this.videoId = videoId;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(user,rating,videoId);
	}
	
	public String toString() {
		return String.format("UserVideoRating(%s,%d,%f)", user, videoId, rating);
	}

}
