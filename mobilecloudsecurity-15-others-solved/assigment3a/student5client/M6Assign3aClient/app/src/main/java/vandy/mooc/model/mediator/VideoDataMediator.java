package vandy.mooc.model.mediator;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedFile;
import vandy.mooc.model.mediator.provider.VideoCache;
import vandy.mooc.model.mediator.webdata.AverageVideoRating;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.mediator.webdata.VideoServiceProxy;
import vandy.mooc.model.mediator.webdata.VideoStatus;
import vandy.mooc.model.mediator.webdata.VideoStatus.VideoState;
import vandy.mooc.model.mediator.webdata.VideoSvc;
import vandy.mooc.utils.VideoMediaStoreUtils;
import vandy.mooc.utils.VideoStorageUtils;
import vandy.mooc.view.SettingsActivity;

import static vandy.mooc.utils.Constants.MAX_SIZE_MEGA_BYTE;

/**
 * Mediates communication between the Video Service and the local
 * storage on the Android device.  The methods in this class block, so
 * they should be called from a background thread (e.g., via an
 * AsyncTask).
 */
public class VideoDataMediator {
    /**
     * Status code to indicate that file is successfully
     * uploaded.
     */
    public static final String STATUS_UPLOAD_SUCCESSFUL =
            "Upload succeeded";

    /**
     * Status code to indicate that file upload failed
     * due to large video size.
     */
    public static final String STATUS_UPLOAD_ERROR_FILE_TOO_LARGE =
            "Upload failed: File too big";

    /**
     * Status code to indicate that file upload failed.
     */
    public static final String STATUS_UPLOAD_ERROR =
            "Upload failed";


    /**
     * Defines methods that communicate with the Video Service.
     */
    private VideoServiceProxy mVideoServiceProxy;
    private WeakReference<Context> mContext;
    private VideoCache mVideoCache;

    /**
     * Constructor that initializes the VideoDataMediator.
     */
    public VideoDataMediator(Context context) {


        mContext = new WeakReference<Context>(context);

        mVideoCache = new VideoCache(mContext.get());

        SharedPreferences prefs =
                PreferenceManager
                        .getDefaultSharedPreferences(context.getApplicationContext());

        String protocol_ = prefs
                .getString(SettingsActivity.KEY_PREFERENCE_PROTOCOL,
                        "");
        String server_ = prefs
                .getString(SettingsActivity.KEY_PREFERENCE_IP_ADDRESS,
                        "");
        String port_ = prefs
                .getString(SettingsActivity.KEY_PREFERENCE_PORT,
                        "");
        String username_ = prefs
                .getString(SettingsActivity.KEY_PREFERENCE_USER_NAME,
                        "");
        String password_ = prefs
                .getString(SettingsActivity.KEY_PREFERENCE_PASSWORD,
                        "");

        mVideoServiceProxy =  VideoSvc.init(server_,
                username_,
                password_,
                protocol_,
                port_);
    }



    public String downloadVideo(long videoId, String videoTitle) {
        try {
            Response response =
                    mVideoServiceProxy.getVideoData(videoId);
            if(response.getStatus() == 200) {
                File videoFile =
                        VideoStorageUtils.storeVideoInExternalDirectory
                                (mContext.get(),
                                        response,
                                        videoTitle);
                if (videoFile != null) {
                    return videoFile.getAbsolutePath();
                }
                else return null;
            }
            else return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Uploads the Video having the given Id.  This Id is the Id of
     * Video in Android Video Content Provider.
     *
     * @param videoUri
     *            Id of the Video to be uploaded.
     *
     * @return A String indicating the status of the video upload operation.
     */
    public String uploadVideo(Uri videoUri) {
        // Get the path of video file from videoUri.
        String filePath =
                VideoMediaStoreUtils.getPath(mContext.get(),
                        videoUri);

        // Get the Video from Android Video Content Provider having
        // the given filePath.
        Video androidVideo =
                VideoMediaStoreUtils.getVideo(mContext.get(),
                        filePath);

        // Check if any such Video exists in Android Video Content
        // Provider.
        if (androidVideo != null) {
            // Prepare to Upload the Video data.

            // Create an instance of the file to upload.
            File videoFile = null;
            if (filePath != null) {
                videoFile = new File(filePath);
            }

            androidVideo.setLocation("temp");
            androidVideo.setSubject("temp");
            androidVideo.setUrl(filePath);

            // Check if the file size is less than the size of the
            // video that can be uploaded to the server.
            if (videoFile.length() < MAX_SIZE_MEGA_BYTE) {

                try {
                    // Add the metadata of the Video to the Video Service
                    // and get the resulting Video that contains
                    // additional meta-data (e.g., Id and ContentType)
                    // generated by the Video Service.
                    Video receivedVideo =
                            mVideoServiceProxy.addVideo(androidVideo);

                    // Check if the Server returns any Video metadata.
                    if (receivedVideo != null) {

                        // Finally, upload the Video data to the server
                        // and get the status of the uploaded video data.
                        VideoStatus status =
                                mVideoServiceProxy.setVideoData
                                        (receivedVideo.getId(),
                                                new TypedFile(receivedVideo.getContentType(),
                                                        videoFile));

                        // Check if the Status of the Video or not.
                        if (status.getState() == VideoState.READY) {

                            mVideoCache.insert(receivedVideo);

                            //@TODO   Add video to DB
                            return STATUS_UPLOAD_SUCCESSFUL;
                        }
                    }
                } catch (Exception e) {
                    // Error occured while uploading the video.
                    return STATUS_UPLOAD_ERROR;
                }
            } else
                // Video can't be uploaded due to large video size.
                return STATUS_UPLOAD_ERROR_FILE_TOO_LARGE;
        }

        // Error occured while uploading the video.
        return STATUS_UPLOAD_ERROR;
    }


    public boolean getAndSaveVideoMetadata() {
        try {
            ArrayList<Video> videoList =
                    (ArrayList<Video>) mVideoServiceProxy.getVideoList();

            if (videoList != null) {
                mVideoCache.insertAllVideos(videoList);
                return true;
            }
            else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }


    public AverageVideoRating rateVideo(long videoId,
                                        int rating) {
        try {

            AverageVideoRating avr = mVideoServiceProxy.rateVideo(videoId, rating);
            if (avr != null) {
                // get and update mVideoCache.insert(videoId, rating);
                return avr;
            }
            else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public Video getVideo(Long videoId) {
        return mVideoCache.getVideoById(videoId);
    }



}


