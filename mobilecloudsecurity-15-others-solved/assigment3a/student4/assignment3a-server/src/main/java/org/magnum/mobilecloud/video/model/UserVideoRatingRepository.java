package org.magnum.mobilecloud.video.model;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by
 * @author kaido on 27.08.2015.
 */

@Repository
public interface UserVideoRatingRepository extends CrudRepository<UserVideoRating, Long> {
    //  TODO investigate why this one gave core dumps sometimes
//    @Query("SELECT new org.magnum.mobilecloud.video.model.AverageVideoRating(AVG(rating) as rating, videoId as videoId, COUNT(rating) as totalRatings) from #{#entityName} where videoId=:id")
//    AverageVideoRating findAvgRatingByVideoId(@Param("id") long videoId);

    @Query("select avg(rating) from #{#entityName} where videoId = :id")
    double findAvgRatingByVideoId(@Param("id") long videoId);

    @Query("select count(rating) from #{#entityName} where videoId = :id")
    int findCountRatingByVideoId(@Param("id") long videoId);


    UserVideoRating findByVideoIdAndUser(long Id, String username);

}
