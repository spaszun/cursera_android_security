package org.magnum.mobilecloud.video.controller;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoRepository;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.model.VideoStatus.VideoState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class VideoBinaryDataController {

    @Autowired
    private VideoRepository videoRepository_;

    @Autowired
    private VideoFileManager videoDataRepository_;

    @RequestMapping(method = RequestMethod.GET, value = VideoSvcApi.VIDEO_DATA_PATH)
    public void downloadVideo(@PathVariable(VideoSvcApi.ID_PARAMETER) long id, HttpServletResponse response)
	    throws IOException {

	// Always check to ensure a video exists before attempting a download
	Video v = videoRepository_.findOne(id);

	if (v != null) {

	    // Set the content type of the output stream to match what was
	    // provided when the data was uploaded
	    response.setContentType(v.getContentType());

	    // Copy the video data to the output stream that is going back to the
	    // client
	    videoDataRepository_.copyVideoData(v, response.getOutputStream());
	} else {
	    response.sendError(HttpServletResponse.SC_NOT_FOUND, "Video not found");
	}
    }

    @RequestMapping(method = RequestMethod.POST, value = VideoSvcApi.VIDEO_DATA_PATH)
    public @ResponseBody VideoStatus uploadVideo(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
	    @RequestPart(VideoSvcApi.DATA_PARAMETER) MultipartFile videoData, Principal p, HttpServletResponse response)
	    throws IOException {

	VideoStatus status = null;

	Video video = videoRepository_.findOne(id);

	if (video != null && p.getName().equals(video.getOwner())) {
	    videoDataRepository_.saveVideoData(video, videoData.getInputStream());
	    status = new VideoStatus(VideoState.READY);
	} else if (video == null) {
	    response.sendError(HttpServletResponse.SC_NOT_FOUND, "Video not found");
	} else {
	    response.sendError(HttpServletResponse.SC_FORBIDDEN, "You do not own this video");
	}

	return status;
    }

}
