package vandy.mooc.view;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Collection;
import java.util.concurrent.Callable;

import vandy.mooc.R;
import vandy.mooc.common.CallableTask;
import vandy.mooc.common.TaskCallback;
import vandy.mooc.diagnostics.VideoSvc;
import vandy.mooc.model.mediator.VideoDataMediator;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.mediator.webdata.VideoSvcApi;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private static final int MIN_PASSWORD_LENGTH = 0;

    EditText editUserName;
    EditText editPassword;

    private UserLoginTask mAuthTask = null;

    VideoSvcApi videoService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUserName = (EditText) findViewById(R.id.edit_username);
        editPassword = (EditText) findViewById(R.id.edit_password);

        final Button btnLogin = (Button) findViewById(R.id.btn_login);

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
//    @OnClick(R.id.btn_login)
     */

    public void attemptLogin(View v) {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        editUserName.setError(null);
        editPassword.setError(null);

        // Store values at the time of the login attempt.
        String userName = editUserName.getText().toString();
        String password = editPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            editPassword.setError(getString(R.string.error_invalid_password));
            focusView = editPassword;
            cancel = true;
        }

        // Check for a valid username
        if (TextUtils.isEmpty(userName)) {
            editUserName.setError(getString(R.string.error_field_required));
            focusView = editUserName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Kick off a background task to perform the user login attempt.
            mAuthTask = new UserLoginTask(userName, password);
            Log.d(TAG, "mAuthTask initialized " + userName + ":" + password);
            mAuthTask.execute((Void) null);
            Log.d(TAG, "mAuthTask completed");
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= MIN_PASSWORD_LENGTH;
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Collection<Video>> {

        private final String mUserName;
        private final String mPassword;



        UserLoginTask(String userName, String password) {
            mUserName = userName;
            mPassword = password;

            videoService = VideoDataMediator.init(mUserName, mPassword);
        }


        @Override
        protected Collection<Video> doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            Log.d(TAG, "attempt authentication, start async task. videoService = "
                    + videoService.toString());

            Collection<Video> videos = videoService.getVideoList();
            Log.d(TAG, videos.toString());

            return videos;
        }

        @Override
        protected void onPostExecute(final Collection<Video> videos) {
            Log.d(TAG, "authentication onPostExecute");
            mAuthTask = null;

            if (videos != null) {
                Log.d(TAG, "authentication onPostExecute, videos = " + videos.toString());
                startActivity(new Intent(
                        LoginActivity.this,
                        VideoListActivity.class));
//                finish();
            } else {
                editPassword.setError(getString(R.string.error_incorrect_password));
                editPassword.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }


    /**
     * This one has no real reason to be here except for Genymotion Android 5.1 image being buggy
     * and crashing when executing https request for all of the stacks...
     * @param v
     */

    //  TODO remove at cleanup
    public void login(View v) {

        String server = "https://192.168.1.111:8443";

        String userName = editUserName.getText().toString();
        String password = editPassword.getText().toString();

        final VideoSvcApi svc = VideoSvc.init(server, userName, password);

        CallableTask.invoke(new Callable<Collection<Video>>() {

            @Override
            public Collection<Video> call() throws Exception {
                return svc.getVideoList();
            }
        }, new TaskCallback<Collection<Video>>() {

            @Override
            public void success(Collection<Video> result) {
                // OAuth 2.0 grant was successful and we
                // can talk to the server, open up the video listing
                startActivity(new Intent(
                        LoginActivity.this,
                        VideoListActivity.class));
            }

            @Override
            public void error(Exception e) {
                Log.e(LoginActivity.class.getName(), "Error logging in via OAuth.", e);

                Toast.makeText(
                        LoginActivity.this,
                        "Login failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}

