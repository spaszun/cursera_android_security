package com.sps.videoserviceapp;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sps.videoserviceapp.rest.video.VideoService;
import com.sps.videoserviceapp.rest.video.VideoSvcApi;
import com.sps.videoserviceapp.rest.video.model.AverageVideoRating;
import com.sps.videoserviceapp.videos.VideoWithRating;
import com.sps.videoserviceapp.videos.contentprovider.VideoContract;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedFile;


public class VideoDetailsActivity extends ActionBarActivity {

    private static final int FIFTY_MB = 1024*1024*50;
    private static final int VIDEO_REQUESTED_CODE = 100;
    VideoWithRating videoWithRating;

    ProgressDialog mProgressDialog;
    MediaPlayer mp;
    private ContentResolver mContentResolver;
    private TextView ratingTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContentResolver = getContentResolver();
        setContentView(R.layout.activity_video_details);
        mp = new MediaPlayer();
        //VideoService.init();
        initializeVideoData();
        initlizeProgress();
    }

    private void initlizeProgress() {
        mProgressDialog = new ProgressDialog(VideoDetailsActivity.this);
        mProgressDialog.setMessage("A message");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);
    }

    private void initializeVideoData() {
        videoWithRating = (VideoWithRating) getIntent().getSerializableExtra(VideoWithRating.VIDEO_OBJ);
        TextView titleTextView = (TextView) findViewById(R.id.title);
        titleTextView.setText(videoWithRating.getTitle());
        ratingTextView = (TextView) findViewById(R.id.rating);
        ratingTextView.setText(String.valueOf(videoWithRating.getRating()));
        TextView durationTextView = (TextView) findViewById(R.id.duration);
        durationTextView.setText(String.valueOf(videoWithRating.getDuration()));
        TextView locationTextView = (TextView) findViewById(R.id.location);
        locationTextView.setText(videoWithRating.getLocation());
        TextView subjectTextView = (TextView) findViewById(R.id.subject);
        subjectTextView.setText(videoWithRating.getSubject());
        TextView contentTypeTextView = (TextView) findViewById(R.id.content_type);
        contentTypeTextView.setText(videoWithRating.getContentType());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_download_video) {
            new DownloadTask(getApplicationContext()).execute(videoWithRating.getId());
        } else if (id == R.id.action_upload_video){
            Intent intent = new Intent(getApplicationContext(), PickVideoActivity.class);
            startActivityForResult(intent, VIDEO_REQUESTED_CODE);
        } else if (id == R.id.action_rate_video) {
            final Dialog rankDialog = new Dialog(VideoDetailsActivity.this);
            rankDialog.setContentView(R.layout.rank_dialog);
            rankDialog.setCancelable(true);
            final RatingBar ratingBar = (RatingBar) rankDialog.findViewById(R.id.dialog_ratingbar);
            ratingBar.setRating(0);

            TextView text = (TextView) rankDialog.findViewById(R.id.rank_dialog_text1);
            text.setText("Rate " + videoWithRating.getTitle());

            Button updateButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new RateTask(getApplicationContext(), (int)ratingBar.getRating()).execute(videoWithRating.getId());
                    rankDialog.dismiss();
                }
            });
            //now that the dialog is set up, it's time to show it
            rankDialog.show();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_REQUESTED_CODE) {
            if (resultCode == RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Video picked", Toast.LENGTH_LONG).show();
                Uri uri = data.getParcelableExtra(PickVideoActivity.VIDEO_URI);
                File file =  new File(uri.getPath());
                if (file.length() <= FIFTY_MB) {
                    new UploadTask(getApplicationContext(), file).execute(videoWithRating.getId());
                } else {
                    Toast.makeText(getApplicationContext(), "File is larger then 50MB", Toast.LENGTH_LONG).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
    }

    private  class DownloadTask extends AsyncTask<Long, Integer, String> {

            private Context context;
            File outputFile;

            public DownloadTask(Context context) {
                this.context = context;
            }

            @Override
            protected String doInBackground(Long ... ids) {
                Long id = ids[0];
                InputStream input = null;
                OutputStream output = null;
                try {
                    VideoSvcApi api = VideoService.getService();
                    Response response = api.getVideoData(id);
                    long fileLength = response.getBody().length();
                    input = response.getBody().in();

                    File outputDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
                    outputFile = new File(outputDir, "video-" + id + ".mpg");
                    output = new FileOutputStream(outputFile);

                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        // allow canceling with back button
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        total += count;
                        // publishing the progress....
                        if (fileLength > 0) // only if total length is known
                            publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                } catch (Exception e) {
                    return e.toString();
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } catch (IOException ignored) {
                    }

                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog.show();
            }

            @Override
            protected void onProgressUpdate(Integer... progress) {
                super.onProgressUpdate(progress);
                // if we get here, length is known, now set indeterminate to false
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setMax(100);
                mProgressDialog.setProgress(progress[0]);
            }

            @Override
            protected void onPostExecute(String result) {
                mProgressDialog.dismiss();
                if (result != null)
                    Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show();
                else {
                    Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
                    Uri uri = Uri.fromFile(outputFile);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setDataAndType(uri, "video/*");
                    startActivity(intent);
                }
            }
    }

    private  class UploadTask extends AsyncTask<Long, Integer, String> {

        private final File file;
        private Context context;

        public UploadTask(Context context, File file) {
            this.context = context;
            this.file = file;
        }

        @Override
        protected String doInBackground(Long... ids) {
            Long id = ids[0];
            try {
                VideoSvcApi api = VideoService.getService();
                TypedFile typedFile = new TypedFile("video/mpeg", file);
                api.setVideoData(id, typedFile);
            } catch (Exception e) {
                return e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            mProgressDialog.show();
        }


        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.dismiss();
            if (result != null)
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            else {
                Toast.makeText(context, "File uploaded", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private  class RateTask extends AsyncTask<Long, Integer, String> {

        private final int rating;
        private Context context;
        AverageVideoRating newRating;

        public RateTask(Context context, int rating) {
            this.rating = rating;
            this.context = context;
        }

        @Override
        protected String doInBackground(Long... ids) {
            Long id = ids[0];
            try {
                VideoSvcApi api = VideoService.getService();
                api.rateVideo(id, rating);
                newRating = api.getVideoRating(id);
                updateVideoRating(newRating);
            } catch (Exception e) {
                return e.getMessage();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null)
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            else {
                ratingTextView.setText(Double.toString(newRating.getRating()));
                Toast.makeText(context, "You just rated the video", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void updateVideoRating(AverageVideoRating averageVideoRating) throws RemoteException, OperationApplicationException {

        Uri existingUri = VideoContract.Entry.CONTENT_URI.buildUpon()
                .appendPath(Long.toString(averageVideoRating.getVideoId())).build();
        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();
        batch.add(ContentProviderOperation.newUpdate(existingUri)
                .withValue(VideoContract.Entry.COLUMN_NAME_RATING, averageVideoRating.getRating())
                .build());
        mContentResolver.applyBatch(VideoContract.CONTENT_AUTHORITY, batch);
        mContentResolver.notifyChange(
                VideoContract.Entry.CONTENT_URI, // URI where data was modified
                null,                           // No local observer
                false);
    }
}
