package org.magnum.mobilecloud.video.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.UserRatingRepository;
import org.magnum.mobilecloud.video.VideoRepository;
import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.model.VideoStatus.VideoState;
import org.magnum.mobilecloud.video.controller.VideoFileManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

@Controller
public class VideoService {

	// Video Repository stored with JPA
	@Autowired
	VideoRepository videoRepository_;
	
	// Rating Repository stored by VideoID and User
	@Autowired
	UserRatingRepository userRatings_;

	// Get Method returning all video meta-data objects
	@RequestMapping(method=RequestMethod.GET,
				value = VideoSvcApi.VIDEO_SVC_PATH + "/check/{check_user}")
	public @ResponseBody Integer getLoginConfirmation(
			@PathVariable("check_user") int check_user,
			HttpServletResponse resp	) throws IOException {
		
 		if (check_user != 0) {
 			return check_user;
 		}
 		else {
 			resp.setStatus(404);
	 		return null;
 		}
	}
 	
	
	// Get Method returning all video meta-data objects
	@RequestMapping(method=RequestMethod.GET,
				value = VideoSvcApi.VIDEO_SVC_PATH)
	public @ResponseBody Collection<Video> getVideoList() {
		// return List of all Video
		return Lists.newArrayList(videoRepository_.findAll());
	}
	
	
	
	@RequestMapping(method=RequestMethod.GET,
			value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}")
	public @ResponseBody Video getVideoById(
									@PathVariable("id") long id) {
		
		// return video with id listed in path
		return videoRepository_.findOne(id);
	}
	
	
	@RequestMapping(method=RequestMethod.POST,
			value=VideoSvcApi.VIDEO_SVC_PATH)
	public @ResponseBody Video addVideo(
									@RequestBody Video v,
									Principal p) 
	{
		// get the video id and confirm the state
	    v = videoRepository_.save(v);
		v.setOwner(p.getName());
        v = videoRepository_.save(v);
		return v;
	}

	
 	@RequestMapping(method = RequestMethod.POST,
 			value=VideoSvcApi.VIDEO_DATA_PATH)
	public @ResponseBody VideoStatus uploadVideo(
			@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
			@RequestParam(VideoSvcApi.DATA_PARAMETER) MultipartFile videoData,
			Principal p,
			HttpServletResponse resp	) throws IOException
 	{
 		// get the video id and confirm the state
 		VideoStatus vidStatus = null;
 		
 		// Search for video object in Repository
 		Video vid = videoRepository_.findOne(id);
 		
 		if (vid != null) {
 			
			// Video found - so save if owner matches
 			String userName = p.getName();
 			if (userName.equals(vid.getOwner())) {
 	 			VideoFileManager.get().saveVideoData(vid, videoData.getInputStream());
 	 			vidStatus = new VideoStatus(VideoState.READY);
 	 			return vidStatus;
 			}
 			else {
 	 			resp.setStatus(404);
 	 			return null;
 			}
 		}
 		else {
 			resp.setStatus(404);
 			return null;
 		}
	}
 	
 	@ResponseStatus(value = HttpStatus.NOT_FOUND)

 	public class NotFoundException extends RuntimeException {
 			private static final long serialVersionUID = 1L;
 		}
 	
 	@RequestMapping( value = VideoSvcApi.VIDEO_DATA_PATH,
 					method = RequestMethod.GET)
	public void downloadVideo(
			@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
			HttpServletResponse resp) throws IOException
 	{
 		// Search for video object in Repository
 		Video vid = videoRepository_.findOne(id);

 		if (vid == null) throw new NotFoundException();
 		else {
 			resp.setContentType(vid.getContentType());
 			VideoFileManager.get().copyVideoData(vid, resp.getOutputStream());
 		}
	}
 	
	@RequestMapping(method = RequestMethod.GET,
			value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating")
	public @ResponseBody AverageVideoRating averageRatingOfVideo(
			@PathVariable("id") long id,
			HttpServletResponse resp)
 	{
 		// Search for video object in Repository
	 	Video v = videoRepository_.findOne(id);
	 	
	 	
	 	if (v != null) {
	 		// Get collection of UserRating for Video object
			Collection<UserVideoRating> ratingPerVideo = userRatings_.findByVid(id);
			
			// Sum and average the ratings for the video
			int count = ratingPerVideo.size();
			double ratingSum = 0;
			for (UserVideoRating ratingUvr : ratingPerVideo)
				ratingSum = ratingSum + ratingUvr.getRating();
			ratingSum = ratingSum/count;
			
			// return the average video rating object
	 		return new AverageVideoRating (ratingSum, id, count);
	 	}
	 	else {
	 		resp.setStatus(404);
	 		return null;
	 	}
	} 	
	
	@RequestMapping(method=RequestMethod.POST,
			value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating/{rating}")
	public @ResponseBody AverageVideoRating rateVideo(
			@PathVariable("id") long id,
			@PathVariable("rating") double rating,
			Principal p,
			HttpServletResponse resp)
	{
		// Find the Video based on id
		Video v = videoRepository_.findOne(id);
		
		if (v != null) {
			// Video found - so get the owner
			String user = p.getName();
			
			// Find any previous rating from user
			UserVideoRating uvr = userRatings_.findOneByUserAndVid(user, id);
			
			//log.info("Set Rating Method", "User: " + user, "  Rating: " + rating);
			
			// Add new rating from this user
			if (uvr == null) {
				uvr = new UserVideoRating(id, rating, user);
				userRatings_.save(uvr);
			}
			else {
				// update the found rating
				uvr.setRating(rating);
				userRatings_.save(uvr);
			}
		
			
			// Sum the Rating to return the AverageVideoRating
			Collection<UserVideoRating> ratingPerVideo = userRatings_.findByVid(id);
			int count = ratingPerVideo.size();
			double ratingSum = 0;
			for (UserVideoRating ratingUvr : ratingPerVideo)
				ratingSum = ratingSum + ratingUvr.getRating();
			ratingSum = ratingSum/count;
			
			// Return the AverageVideoRating
			return new AverageVideoRating(ratingSum, id, count);
		}		
		else {
			resp.setStatus(404);
			return null;
		}
	}
	
	
	private String getUrlBaseForLocalServer() {

		HttpServletRequest request = 
		       ((ServletRequestAttributes) RequestContextHolder
		    		   .getRequestAttributes()).getRequest();
		String base =  "http://"
						+ request.getServerName() 
		                + ((request.getServerPort() != 80) ? ":"
						+ request.getServerPort() : "");
		return base;
	}
		
    private String getDataUrl(long videoId){
        String url = getUrlBaseForLocalServer() + "/video/" + videoId + "/data";
        return url;
    }

}
