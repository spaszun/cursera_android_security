package vandy.mooc.model.mediator.webdata;

import android.content.Context;
import android.content.Intent;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import vandy.mooc.utils.Constants;
import vandy.mooc.view.LoginScreenActivity;

public class VideoSvc {

    public static final String CLIENT_ID = "mobile";

    private static VideoServiceProxy videoSvc_;

    public static synchronized VideoServiceProxy getOrShowLogin(Context ctx) {
        if (videoSvc_ != null) {
            return videoSvc_;
        } else {
            Intent i = new Intent(ctx, LoginScreenActivity.class);
            ctx.startActivity(i);
            return null;
        }
    }

    public static synchronized VideoServiceProxy init(String user,
                                                      String pass) {
        videoSvc_ = new SecuredRestBuilder()
                .setLoginEndpoint(Constants.SERVER_URL + VideoServiceProxy.TOKEN_PATH)
                .setUsername(user)
                .setPassword(pass)
                .setClientId(CLIENT_ID)
                .setClient(
                        new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient()))
                .setEndpoint(Constants.SERVER_URL)
                .setLogLevel(LogLevel.FULL).build()
                .create(VideoServiceProxy.class);

        return videoSvc_;
    }
}
