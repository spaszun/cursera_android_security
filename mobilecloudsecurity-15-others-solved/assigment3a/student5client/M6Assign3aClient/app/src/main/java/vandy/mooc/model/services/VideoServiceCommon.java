package vandy.mooc.model.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import vandy.mooc.model.mediator.VideoDataMediator;

/**
 * Created by roy on 7/17/2015.
 */
public abstract class VideoServiceCommon
            extends IntentService {

    /**
     * It is used by Notification Manager to send Notifications.
     */
    protected static final int NOTIFICATION_ID = 1;

    /**
     * VideoDataMediator mediates the communication between Video
     * Service and local storage in the Android device.
     */
    protected VideoDataMediator mVideoMediator;

    /**
     * Manages the Notification displayed in System UI.
     */
    protected NotificationManager mNotifyManager;



    public VideoServiceCommon(String serviceName) {
        super(serviceName);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mVideoMediator =
                new VideoDataMediator(getApplicationContext());
        mNotifyManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public Notification makeNotification(int resId,
                                         String status) {


        // Create the Notification and set a progress indicator for an
        // operation of indeterminate length.
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this);

        builder.setContentTitle(status)
                .setProgress(0,0,false)
                .setSmallIcon(resId)
                .setContentText("")
                .setTicker(status);

        return builder.build();

    }

    public Notification makeNotification(int resId,
                                         String direction,
                                         String videoTitle,
                                         PendingIntent pendingIntent) {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this);

        // Create the Notification and set a progress indicator for an
        // operation of indeterminate length.
        builder.setContentTitle("Video " + direction)
                .setContentText(direction + " in progress")
                .setContentIntent(pendingIntent)
                .setSmallIcon(resId)
                .setTicker(direction + "ing video " + videoTitle)
                .setProgress(0, 0, true);

        // Build and issue the notification.
        return builder.build();
    }


}
