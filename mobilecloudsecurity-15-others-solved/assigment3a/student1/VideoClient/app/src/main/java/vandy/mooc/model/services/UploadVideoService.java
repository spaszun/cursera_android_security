package vandy.mooc.model.services;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Intent Service that will run in background and uploads the video
 * with a given Id.  After the upload completes the user is notified
 * via Notification Manager about the success or failure of the
 * operation.
 */
public class UploadVideoService
        extends VideoServiceCommon {

    /**
     * Constructor for UploadVideoService.
     */
    public UploadVideoService() {
        super("UploadVideoService");
    }

    /**
     * Factory method that makes the explicit intent another Activity
     * uses to call this Service.
     *
     * @param context
     * @param videoUri
     * @return Intent to start this Service
     */
    public static Intent makeIntent(Context context,
                                    Uri videoUri) {
        return new Intent(context,
                UploadVideoService.class)
                .setData(videoUri);
    }

    /**
     * Hook method that is invoked on the worker thread with a request
     * to process.  Only one Intent is processed at a time, but the
     * processing happens on a worker thread that runs independently
     * from other application logic.
     *
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // Starts the Notification to show the progress of video
        // upload.
        startNotification();

        // Check if Video Upload is successful.
        finishNotification(mVideoDataMediator.uploadVideo
                (intent.getData()));
    }

    /**
     * Starts the Notification to show the progress of video upload.
     */
    private void startNotification() {
        // Build and Issues the notification.
        mNotifyManager.notify(NOTIFICATION_ID,
                makeNotification(android.R.drawable.stat_sys_upload,
                        "Upload",
                        "",
                        null));
    }

    /**
     * Finish the Notification after the Video is Uploaded.
     *
     * @param status
     */
    private void finishNotification(String status) {

        // Build the Notification with the given Notification Id and
        // pass it to the NotificationManager.
        mNotifyManager.notify(NOTIFICATION_ID,
                makeNotification(android.R.drawable.stat_sys_upload_done,
                        status));
    }
}