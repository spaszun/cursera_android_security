package secure.video.upload.operations;

import android.app.Activity;
import android.content.ContentValues;
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.UUID;

import retrofit.mime.TypedFile;
import secure.video.upload.R;
import secure.video.upload.activities.UploadVideoActivity;
import secure.video.upload.model.mediator.VideoMediator;
import secure.video.upload.model.mediator.webdata.AverageVideoRating;
import secure.video.upload.model.mediator.webdata.Video;
import secure.video.upload.model.mediator.webdata.VideoStatus;
import secure.video.upload.model.mediator.webdata.VideoSvcApi;
import secure.video.upload.provider.VideoContract;
import secure.video.upload.utils.ConfigurableOps;
import secure.video.upload.utils.GenericAsyncTask;
import secure.video.upload.utils.GenericAsyncTaskOps;
import secure.video.upload.utils.PersProgressDialog;

public class UploadVideoOps implements ConfigurableOps, GenericAsyncTaskOps<Object[], Void, Boolean>
{
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * Retrofit api
     */
    private VideoSvcApi mVideoSvcApi;

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<UploadVideoActivity> mActivity;

    /**
     * The GenericAsyncTask used to upload the video
     */
    private GenericAsyncTask<Object[], Void, Boolean, UploadVideoOps> mAsyncTask;

    /**
     * True if the upload was successful
     */
    private Boolean mResult = null;

    /**
     * Default constructor that's needed by the GenericActivity framework.
     */
    public UploadVideoOps()
    {
    }

    @Override
    public void onConfiguration (Activity activity, boolean firstTimeIn)
    {
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d (TAG, "onConfiguration() called the " + time + " with activity = " + activity);

        mActivity = new WeakReference<>((UploadVideoActivity) activity);

        if (firstTimeIn) {
            try {
                mVideoSvcApi = VideoMediator.get();
            }
            catch (SecurityException e) {
                mActivity.get().notifyMediatorNotConnected();
                return;
            }
        }
        else {
            if (mResult != null) {
                mActivity.get().uploadFinished (mResult ?
                        "Video successfully uploaded" : "Video upload failed");
            }
        }
    }

    public void uploadVideo (String title, long duration, String videoDataFile)
    {
        if (mAsyncTask != null) {
            mAsyncTask.cancel (true);
        }

        // Creating the video
        String url = "http://coursera.org/some/video-"+title;
        Video video = new Video (null, title, url, duration, 0, null);
        video.setContentType ("video/mp4");

        Object[] params = new Object[2];
        params[0] = video;
        params[1] = videoDataFile;

        mAsyncTask = new GenericAsyncTask<> (this);
        mAsyncTask.execute (params);
    }

    @Override
    public void publishProgress (int progress)
    {
        mActivity.get().notifyProgressUpdate (progress, R.string.uploadDialogTitle,
                R.string.uploadDialogExpl);
    }

    @Override
    public Boolean doInBackground (Object... params)
    {
        Log.d (TAG, "Inside the doInBackground");
        Video video = (Video) params[0];
        String videoDataFile = (String) params[1];

        // Uploading the video info
        Video received = mVideoSvcApi.addVideo (video);

        // Uploading the video data
        VideoStatus status = mVideoSvcApi.setVideoData (received.getId(),
                new TypedFile (received.getContentType(), new File (videoDataFile)));
        switch (status.getState()) {
            case READY:
                mResult = true;
                break;
            case FAILED:
                mResult = false;
        }

        AverageVideoRating rating = null;
        if (mResult) {
            rating = mVideoSvcApi.getVideoRating (received.getId());
        }

        // Adding the video to the database if the upload was correct
        if (mResult) {
            ContentValues cv = new ContentValues();
            cv.put (VideoContract.VideoEntry.COLUMN_ID, received.getId());
            cv.put (VideoContract.VideoEntry.COLUMN_TITLE, received.getTitle());
            cv.put (VideoContract.VideoEntry.COLUMN_DURATION, received.getDuration());
            cv.put (VideoContract.VideoEntry.COLUMN_CONTENT_TYPE, received.getContentType());
            cv.put (VideoContract.VideoEntry.COLUMN_DATA_URL, received.getUrl());
            if (rating != null) {
                cv.put (VideoContract.VideoEntry.COLUMN_STAR_RATING, rating.getRating());
                cv.put (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS, rating.getTotalRatings());
            }
            else {
                cv.put (VideoContract.VideoEntry.COLUMN_STAR_RATING, 0);
                cv.put (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS, 0);
            }
            cv.put (VideoContract.VideoEntry.COLUMN_DATA_LOCAL_URI, videoDataFile);
            ContentValues[] cvs = new ContentValues[1];
            cvs[0] = cv;

            try {
                mActivity.get().getContentResolver().bulkInsert (VideoContract.VideoEntry.CONTENT_URI, cvs);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mResult;
    }

    @Override
    public void onPostExecute (Boolean success, Object[] param)
    {
        publishProgress (PersProgressDialog.OPERATION_COMPLETED);
        mActivity.get().uploadFinished (success ? "Video successfully uploaded" : "Video upload failed");
    }
}
