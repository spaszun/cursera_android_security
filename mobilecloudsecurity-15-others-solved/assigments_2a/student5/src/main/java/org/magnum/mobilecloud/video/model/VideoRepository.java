package org.magnum.mobilecloud.video.model;



import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoRepository  extends CrudRepository<Video, Long> {
	public List<Video> findByOwner(String owner);	
	public List<Video> findByOwnerAndTitle(String owner, String title);
	public List<Video> findByOwnerAndTitleAndDuration(String owner, String title, long duration);
	public Video findByOwnerAndTitleAndDurationAndUrl(String owner, String title, long duration, String url);	
}
