/* 
**
** Copyright 2014, Jules White
**
** 
*/
package vandy.mooc.model.mediator.webdata;

public interface TaskCallback<T> {

    void success(T result);

    void error(Exception e);

}
