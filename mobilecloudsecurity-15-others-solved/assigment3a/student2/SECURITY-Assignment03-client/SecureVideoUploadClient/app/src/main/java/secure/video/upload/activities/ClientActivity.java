package secure.video.upload.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import secure.video.upload.R;
import secure.video.upload.utils.LifecycleLoggingActivity;

public class ClientActivity extends LifecycleLoggingActivity
{
    public static final int CAPTURE_VIDEO_REQUEST = 0;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_client);
    }

    public void downloadVideosInfo (View view)
    {
        startActivity (new Intent (this, VideosInfoActivity.class));
    }

    public void uploadVideo (View view)
    {
        startActivityForResult (new Intent (this, CaptureVideoActivity.class), CAPTURE_VIDEO_REQUEST);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CAPTURE_VIDEO_REQUEST) {
            if (resultCode == RESULT_OK) {
                int duration = data.getIntExtra (CaptureVideoActivity.TAG_VIDEO_DURATION, 0);
                String videoDataFile = data.getStringExtra (CaptureVideoActivity.TAG_VIDEO_DATA_FILE);

                Intent intent = new Intent (this, UploadVideoActivity.class);
                intent.putExtra (CaptureVideoActivity.TAG_VIDEO_DURATION, duration);
                intent.putExtra (CaptureVideoActivity.TAG_VIDEO_DATA_FILE, videoDataFile);
                startActivity (intent);
            }
        }
    }
}
