package secure.video.upload.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

public class VideoProvider extends ContentProvider
{
    private static final String TAG = VideoProvider.class.getSimpleName();

    /**
     * Use VideoDatabaseHelper to manage database creation and version management.
     */
    private VideoDatabaseHelper mOpenHelper;

    /**
     * The code that is returned when a URI for 1 item is matched against the given components.
     * Must be positive.
     */
    private static final int VIDEO = 100;

    /**
     * The code that is returned when a URI for more than 1 items is matched against the given
     * components.  Must be positive.
     */
    private static final int VIDEOS = 101;

    /**
     * The URI Matcher used by this content provider.
     */
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    /**
     * Helper method to match each URI to the VIDEO integers constant defined above.
     * @return UriMatcher
     */
    private static UriMatcher buildUriMatcher()
    {
        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found. The code passed into the constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher (UriMatcher.NO_MATCH);

        // For each type of URI that is added, a corresponding code is created.
        matcher.addURI (VideoContract.CONTENT_AUTHORITY, VideoContract.PATH_VIDEO, VIDEO);
        matcher.addURI (VideoContract.CONTENT_AUTHORITY, VideoContract.PATH_VIDEO, VIDEOS);
        return matcher;
    }


    /**
     * Hook method called when Database is created to initialize the Database Helper that provides
     * access to the Video Database.
     */
    @Override
    public boolean onCreate()
    {
        mOpenHelper = new VideoDatabaseHelper (getContext());
        return true;
    }

    @Override
    public String getType (Uri uri)
    {
        // Match the id returned by UriMatcher to return appropriate MIME_TYPE.
        switch (sUriMatcher.match (uri)) {
            case VIDEO:
                return VideoContract.VideoEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException ("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert (Uri uri, ContentValues values)
    {
        Log.d (TAG, "Inserting row");

        // Create and/or open a database that will be used for reading and writing. Once opened
        // successfully, the database is cached, so you can call this method every time you need to
        // write to the database.
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        switch (sUriMatcher.match (uri)) {
            case VIDEO:
                long id = db.insert (VideoContract.VideoEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    // Notifies registered observers that a row was inserted.
                    getContext().getContentResolver().notifyChange (uri, null);

                    return VideoContract.VideoEntry.buildVideoUri (id);
                }
                throw new android.database.SQLException ("Failed to insert row into " + uri);

            default:
                throw new UnsupportedOperationException ("Unknown uri: " + uri);
        }
    }

    // Hook method to handle requests to insert a set of new rows, or the default implementation
    // will iterate over the values and call insert on each of them. As a courtesy, call notifyChange()
    // after inserting.
    @Override
    public int bulkInsert (Uri uri, ContentValues[] contentValues)
    {
        Log.d (TAG, "Bulk-inserting row");

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        if (contentValues == null) {
            return 0;
        }

        switch (sUriMatcher.match (uri)) {
            case VIDEOS:
                // Begins a transaction in EXCLUSIVE mode.
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues cv : contentValues) {
                        if (cv == null) {
                            continue;
                        }
                        long id = db.insert (VideoContract.VideoEntry.TABLE_NAME, null, cv);
                        if (id > 0) {
                            returnCount++;
                        }
                    }

                    // Marks the current transaction as successful.
                    db.setTransactionSuccessful();
                }
                finally {
                    // End a transaction.
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange (uri, null);

                return returnCount;

            default:
                throw new UnsupportedOperationException ("Unknown uri: " + uri);
                //return super.bulkInsert (uri, contentValues);
        }
    }

    /**
     * Hook method called to handle query requests from clients.
     */
    @Override
    public Cursor query (Uri uri, String[] projection, String selection, String[] selectionArgs,
                         String sortOrder)
    {
        Log.d (TAG, "Querying the db");

        Cursor retCursor;

        switch (sUriMatcher.match (uri)) {
            case VIDEOS:
                retCursor = mOpenHelper.getReadableDatabase().query (VideoContract.VideoEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;

            case VIDEO:
                final String rowId = "" + VideoContract.VideoEntry._ID + " = '" +
                        ContentUris.parseId (uri) + "'";
                String uSelection = (selection == null ? rowId : selection + " AND " + rowId);
                retCursor = mOpenHelper.getReadableDatabase().query (VideoContract.VideoEntry.TABLE_NAME,
                        projection, uSelection, selectionArgs, null, null, sortOrder);
                break;

            default:
                throw new UnsupportedOperationException ("Unknown uri: " + uri);
        }

        // Register to watch a content URI for changes.
        retCursor.setNotificationUri (getContext().getContentResolver(), uri);

        return retCursor;
    }

    /**
     * Hook method called to handle requests to update one or more rows. The implementation should
     * update all rows matching the selection to set the columns according to the provided values
     * map. As a courtesy, notifyChange() is called after updating.
     */
    @Override
    public int update (Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        Log.d (TAG, "Updating the db");

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int rowsUpdated;

        switch (sUriMatcher.match (uri)) {
            case VIDEO:
            case VIDEOS:
                rowsUpdated = db.update (VideoContract.VideoEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException ("Unknown uri: " + uri);
        }

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange (uri, null);
        }

        return rowsUpdated;
    }

    /**
     * Hook method to handle requests to delete one or more rows. The implementation should apply
     * the selection clause when performing deletion, allowing the operation to affect multiple
     * rows in a directory. As a courtesy, notifyChange() is called after deleting.
     */
    @Override
    public int delete (Uri uri, String selection, String[] selectionArgs)
    {
        Log.d (TAG, "Deleting row");

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int rowsDeleted = 0;

        switch (sUriMatcher.match (uri)) {
            case VIDEO:
            case VIDEOS:
                rowsDeleted = db.delete (VideoContract.VideoEntry.TABLE_NAME, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException ("Unknown uri: " + uri);
        }

        if ((selection == null) || (rowsDeleted != 0)) {
            getContext().getContentResolver().notifyChange (uri, null);
        }

        return rowsDeleted;
    }
}
