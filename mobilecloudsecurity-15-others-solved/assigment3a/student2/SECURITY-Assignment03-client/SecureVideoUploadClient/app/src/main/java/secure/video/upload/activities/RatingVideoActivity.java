package secure.video.upload.activities;


import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import secure.video.upload.model.mediator.webdata.AverageVideoRating;
import secure.video.upload.operations.RatingOps;
import secure.video.upload.utils.GenericActivity;
import secure.video.upload.utils.PersProgressDialog;

public class RatingVideoActivity extends GenericActivity<RatingOps>
{
    private long mVideoId;
    private int mPersonalRating;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        mOpProgressDialog = new PersProgressDialog (RatingVideoActivity.this);

        Intent intent = getIntent();
        mVideoId = intent.getLongExtra (VideosInfoActivity.TAG_VIDEO_ID, -1);
        mPersonalRating = intent.getIntExtra (VideosInfoActivity.TAG_VIDEO_STARS, -1);

        super.onCreate (savedInstanceState, RatingOps.class);
    }

    public void notifyMediatorNotConnected()
    {
        Toast.makeText (this, "Impossible to rate the video", Toast.LENGTH_SHORT).show();
        finish();
    }

    public long getVideoId()
    {
        return mVideoId;
    }

    public int getPersonalRating()
    {
        return mPersonalRating;
    }

    public void receivedAvgRating (AverageVideoRating avgRating, String errorMessage)
    {
        Intent intent = new Intent (this, SingleVideoActivity.class);

        if (errorMessage != null) {
            Toast.makeText (getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
            setResult (RESULT_CANCELED, intent);
        }
        else {
            intent.putExtra (VideosInfoActivity.TAG_VIDEO_STARS, avgRating.getRating());
            intent.putExtra (VideosInfoActivity.TAG_TOTAL_VIDEO_RATINGS, avgRating.getTotalRatings());
            setResult (RESULT_OK, intent);
        }

        finish();
    }
}
