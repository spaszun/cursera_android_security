/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package vandy.mooc.model.mediator.webdata;

import android.content.Context;
import android.content.Intent;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import vandy.mooc.view.LoginActivity;

public class VideoSvc {

	public static final String CLIENT_ID = "mobile";

	private static VideoServiceProxy videoSvc_;

	public static synchronized VideoServiceProxy getOrShowLogin(Context ctx) {
		if (videoSvc_ != null) {
			return videoSvc_;
		} else {
			Intent i = new Intent(ctx, LoginActivity.class);
			ctx.startActivity(i);
			return null;
		}
	}

	public static synchronized VideoServiceProxy init(String serverIp, String user,
			String pass, String protocol, String port) {

		//String server = protocol
		//		+ "://"
		//		+ serverIp
		//		+ ":"
		//		+ port;

        String server = "https"
        		+ "://"
        		+ "192.168.1.110"
        		+ ":"
        		+ "8443";

		videoSvc_ = new SecuredRestBuilder()
				.setLoginEndpoint(server + VideoServiceProxy.TOKEN_PATH)
				.setUsername(user)
				.setPassword(pass)
				.setClientId(CLIENT_ID)
                .setClient(new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient()))
                .setEndpoint(server)
                .setLogLevel(RestAdapter.LogLevel.FULL).build()
                .create(VideoServiceProxy.class);

		return videoSvc_;
	}
}
