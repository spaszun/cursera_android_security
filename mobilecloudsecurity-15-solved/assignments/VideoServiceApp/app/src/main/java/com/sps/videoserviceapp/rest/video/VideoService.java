package com.sps.videoserviceapp.rest.video;

import com.sps.videoserviceapp.rest.base.SecuredRestBuilder;
import com.sps.videoserviceapp.rest.base.UnsafeHttpsClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Q1SW on 21-08-2015.
 */
public class VideoService {


    private static final String CLIENT_ID = "mobile";
    private static final String TEST_URL = "https://10.140.12.20:8443";

    static private VideoSvcApi videoSvc;

    public static void init(String login, String password) {
        videoSvc = new SecuredRestBuilder()
                .setLoginEndpoint(TEST_URL + VideoSvcApi.TOKEN_PATH)
                .setUsername(login)
                .setPassword(password)
                .setClientId(CLIENT_ID)
                .setClient(new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient()))
                .setEndpoint(TEST_URL).setLogLevel(RestAdapter.LogLevel.FULL).build()
                .create(VideoSvcApi.class);
    }


    public static VideoSvcApi getService() {
        return videoSvc;
    }


}
