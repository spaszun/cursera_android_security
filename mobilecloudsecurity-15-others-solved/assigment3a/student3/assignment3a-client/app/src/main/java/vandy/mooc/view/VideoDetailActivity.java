package vandy.mooc.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;

import vandy.mooc.R;
import vandy.mooc.common.GenericActivity;
import vandy.mooc.common.Utils;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.services.DownloadVideoService;
import vandy.mooc.presenter.VideoRatingOps;

public class VideoDetailActivity
        extends GenericActivity<VideoRatingOps.View, VideoRatingOps>
        implements VideoRatingOps.View {

    private static final String ACTION_VIDEO_DISPLAY = "vandy.mooc.intent.action.DISPLAY_VIDEO";
    public static final String TYPE_VIDEO = "parcelable/video";
    public static final String KEY_VIDEO_DATA = "videoItem";

    private Video mVideoInstance;

    private TextView videoTitle;
    private TextView videoRating;
    private RatingBar userRatingBar;
    private VideoView mVideoView;

//    private VideoRatingOps videoRatingOps;

    /**
     * @param video Video object to open in new activity
     * @return intent to display activity containing video metadata
     */
    public static Intent makeIntent(Video video) {
        // Create an Intent with a custom action to display VideoDetailActivity.
        return new Intent(ACTION_VIDEO_DISPLAY)
                // Set MIME_TYPE to display Video.
                .setType(TYPE_VIDEO)
                        // Store the Video ID to send to the Video Details Activity
                .putExtra(KEY_VIDEO_DATA, video);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        initializeViewFields();

        final Intent intent = getIntent();

        // Invoke the special onCreate() method in GenericActivity,
        // passing in the VideoListOps class to instantiate/manage and
        // "this" to provide VideoListOps with the VideoListOps.View instance.
        super.onCreate(savedInstanceState, VideoRatingOps.class, this);

        //  I dont think there could be malicious intents fed into activity but then again
        //  this is security MOOC
        if (intent.getType().equals(TYPE_VIDEO)) {
            final Video video = intent.getParcelableExtra(KEY_VIDEO_DATA);
            setViewFields(video);
        }

    }

    private void setViewFields(Video video) {
        mVideoInstance = video;

        videoTitle.setText(mVideoInstance.getTitle());
        videoRating.setText(String.valueOf(mVideoInstance.getStarRating()));

        //  get video files storage location and check if its present there.
        File extStore = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File videoFile = new File(extStore.getAbsolutePath() + File.separator + video.getTitle());

        if (videoFile.exists()) {
            Uri uri = Uri.parse(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS) + File.separator + video.getTitle());
            mVideoView.setVideoURI(uri);
            mVideoView.start();

        } else {
            Utils.showToast(this, "Download video: " + mVideoInstance.getTitle());
        }

    }

    private void initializeViewFields() {
        videoTitle = (TextView) findViewById(R.id.textview_video_title);

        //  TODO it would be nice to somehow let user know if video is downloaded or not
        videoRating = (TextView) findViewById(R.id.tv_average_rating);
        userRatingBar = (RatingBar) findViewById(R.id.ratingbar_video_user_rating);

        userRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            // Called when the user swipes the RatingBar
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                //  TODO onRatingChanged needs implementation

                registerUserVideoRating(mVideoInstance.getId(), userRatingBar.getRating());

            }

        });

        mVideoView = (VideoView) findViewById(R.id.vv_video_view);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Rating bar listener
     *
     * @param id     video identifier
     * @param rating user given video rating
     */

    public void registerUserVideoRating(long id, float rating) {
        Toast.makeText(this, "Rating registered, activity notification not implemented", Toast.LENGTH_SHORT).show();
        getOps().rateVideo(id, rating);

    }

    /**
     * Download button listener
     *
     * @param v obligatory View parameter so method can be called by button
     */
    public void downloadVideo(View v) {
//        Toast.makeText(this, "Video download not implemented yet", Toast.LENGTH_LONG).show();
        Toast.makeText(this,
                "Downloading video: " + mVideoInstance.getTitle(),
                Toast.LENGTH_LONG).show();
        getApplicationContext().startService
                (DownloadVideoService.makeIntent
                        (getApplicationContext(),
                                mVideoInstance.getId(),
                                mVideoInstance.getTitle()));
    }


}
