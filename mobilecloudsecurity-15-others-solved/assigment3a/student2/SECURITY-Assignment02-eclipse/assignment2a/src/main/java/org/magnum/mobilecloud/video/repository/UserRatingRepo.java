package org.magnum.mobilecloud.video.repository;

import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.springframework.data.repository.CrudRepository;

public interface UserRatingRepo extends CrudRepository<UserVideoRating, Long>
{
}
