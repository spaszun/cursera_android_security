package vandy.mooc.model.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class VideoProvider extends ContentProvider {

    protected final String TAG = getClass().getCanonicalName();

    private static final int VIDEO = 101;
    private static final int VIDEOS = 100;

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    protected Context mContext;

    private VideoDatabaseHelper mOpenHelper;

    /**
     * Return true if successfully started.
     */
    @Override
    public boolean onCreate() {
        // Create the VideoDatabaseHelper.
        mOpenHelper = new VideoDatabaseHelper(getContext());
        return true;
    }

    @Override
    public Uri insert(Uri uri, ContentValues cvs) {

        // ignore anything except single video record
        if (sUriMatcher.match(uri) != VIDEO)
            throw new IllegalArgumentException("Unknown URI " + uri);

        final long id = mOpenHelper.getWritableDatabase()
                .insert(VideoContract.VideoEntry.TABLE_NAME, null, cvs);

        // Check if a new row is inserted or not.
        if (id == 0)
            throw new android.database.SQLException("Failed to insert row into " + uri);

        //  build uri to send to observers
        Uri resultUri = VideoContract.VideoEntry.buildUri(id);
        //  send notification to observers about inserted row
        getContext().getContentResolver().notifyChange(resultUri, null);
        //  send notification to observers about table changed
        getContext().getContentResolver().notifyChange(uri, null);

        return resultUri;

    }

    /**
     * Method that handles bulk insert requests.
     */
    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        // Fetch the db from the helper.
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        // Match the Uri against the table's uris to determine the
        // table in which table to insert the values.
        if (sUriMatcher.match(uri) != VIDEOS)
            throw new IllegalArgumentException("Unknown URI " + uri);

        // Insert the values into the table in one transaction by
        // beginning a transaction in EXCLUSIVE mode.
        db.beginTransaction();
        int returnCount = 0;

        try
        {
            for (ContentValues value : values) {
                final long id = db.insert(VideoContract.VideoEntry.TABLE_NAME, null, value);
                if (id != -1)
                    returnCount++;
            }
            // Marks the current transaction as successful.
            db.setTransactionSuccessful();
        } finally {
            // End the transaction
            db.endTransaction();
        }

        // Notifies registered observers that rows were inserted
        getContext().getContentResolver().notifyChange(uri, null);

        return returnCount;

    }

    @Override
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {

        switch (sUriMatcher.match(uri)) {
            case VIDEOS:
                // Expand the selection if necessary.
                selection = addSelectionArgs(selection, selectionArgs, "OR");
                break;
            case VIDEO:
                // Expand the selection if necessary.
                selection = addKeyIdCheckToWhereStatement(selection, ContentUris.parseId(uri));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        return mOpenHelper.getReadableDatabase().query(
                VideoContract.VideoEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        //  Should we have registered for updates from this particular cursor?
        //  TODO check from documentation how cursor is released

    }

    /**
     * Use Uri Matcher to determine what kind of URI this is asked
     * for and return the appropriate MIME type
     *
     * @param uri uri to be matched
     * @return mime type for given uri
     */
    @Override
    public String getType(Uri uri) {

        // Match the id returned by UriMatcher to return appropriate MIME_TYPE.
        switch (sUriMatcher.match(uri)) {
            case VIDEOS:
                return VideoContract.VideoEntry.VIDEO_ITEMS_TYPE;
            case VIDEO:
                return VideoContract.VideoEntry.VIDEO_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }


    @Override
    public int update(Uri uri,
                      ContentValues cvs,
                      String selection,
                      String[] selectionArgs) {

        // If the URI includes a  specific row to update, add that row to the where statement.
        switch (sUriMatcher.match(uri)) {
            case VIDEO:
                selection = addKeyIdCheckToWhereStatement(selection, ContentUris.parseId(uri));
                break;
            case VIDEOS:
                // Expand the selection if necessary.
                selection = addSelectionArgs(selection, selectionArgs, " OR ");
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        int rowsUpdated = mOpenHelper.getWritableDatabase().update
                (VideoContract.VideoEntry.TABLE_NAME, cvs, selection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;

    }


    @Override
    public int delete(Uri uri,
                      String selection,
                      String[] selectionArgs) {

        // Expand the selection if necessary.
        selection = addSelectionArgs(selection, selectionArgs, " OR ");
        return mOpenHelper.getWritableDatabase().delete
                (VideoContract.VideoEntry.TABLE_NAME, selection, selectionArgs);
    }

    private String addSelectionArgs(String selection, String[] selectionArgs, String operation) {
        // Handle the "null" case.
        if (selection == null || selectionArgs == null)
            return null;
        else {
            String selectionResult = "";

            // Properly add the selection args to the selectionResult.
            for (int i = 0; i < selectionArgs.length - 1; ++i)
                selectionResult += (selection + " = ? " + operation + " ");

            // Handle the final selection case.
            selectionResult += (selection + " = ?");

            // Output the selectionResults to Logcat.
            Log.d(TAG, "selection = " + selectionResult + " selectionArgs = ");
            for (String args : selectionArgs)
                Log.d(TAG, args + " ");

            return selectionResult;

        }
    }

    /**
     * Helper method that appends a given key id to the end of the WHERE statement parameter.
     */
    private static String addKeyIdCheckToWhereStatement(String whereStatement, long id) {
        String newWhereStatement;
        if (TextUtils.isEmpty(whereStatement))
            newWhereStatement = "";
        else
            newWhereStatement = whereStatement + " AND ";

        // Append the key id to the end of the WHERE statement.
        return newWhereStatement + VideoContract.VideoEntry._ID + " = '" + id + "'";

    }


    private static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.

        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        // For each type of URI that is added, a corresponding code is created.
        matcher.addURI(VideoContract.CONTENT_AUTHORITY, //  authority
                VideoContract.VideoEntry.TABLE_NAME,    //  path
                VIDEOS);                                //  code
        matcher.addURI(VideoContract.CONTENT_AUTHORITY,
                VideoContract.VideoEntry.TABLE_NAME
                        + "/#",
                VIDEO);

        return matcher;

    }

}
