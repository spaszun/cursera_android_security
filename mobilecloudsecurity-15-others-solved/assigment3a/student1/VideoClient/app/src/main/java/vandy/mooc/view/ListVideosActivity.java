package vandy.mooc.view;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;

import vandy.mooc.MVP;
import vandy.mooc.R;
import vandy.mooc.common.GenericActivity;
import vandy.mooc.common.Utils;
import vandy.mooc.model.mediator.provider.VideoContract;
import vandy.mooc.presenter.VideosPresenter;
import vandy.mooc.view.ui.FloatingActionButton;
import vandy.mooc.view.ui.VideoAdapter;

/**
 * The Activity can be used upload a selected video to a Video
 * Service and also displays a list of videos available at the Video
 * Service.  The user can record a video or get a video from gallery
 * and upload it.  It plays the role of the "View" in the
 * Model-View-Presenter (MVP) pattern.  It extends GenericActivity
 * that provides a framework to automatically handle runtime
 * configuration changes of a VideoPresenter object, which
 * plays the role of the "Presenter" in the MVP pattern.  The
 * MVP.VideoRequiredViewOps and MVP.VideoProviderPresenterOps
 * interfaces are used to minimize dependencies between the View and
 * Presenter layers.  It implements LoaderCallbacks so it can update
 * the list of videos available on the Video Service automatically and
 * implements OnItemClickListener so the user can select a video to
 * download/play.
 */
public class ListVideosActivity
        extends GenericActivity<MVP.VideoRequiredViewOps,
        MVP.VideosProvidedPresenterOps,
        VideosPresenter>
        implements MVP.VideoRequiredViewOps,
        LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

    /**
     * The Request Code used by startActivityWithResult to get Video
     * from Gallery or record video and return the result.
     */
    private final int REQUEST_GET_VIDEO = 1;

    /**
     * The Floating Action Button (FAB) that will show a Dialog
     * Fragment to upload Video when user clicks on it.
     */
    private FloatingActionButton mUploadVideoButton;

    /**
     * The ListView that contains a list of Videos available from
     * the Video Service.
     */
    private ListView mVideosList;

    /**
     * Cursor Adapter used to show the mVideosList in a ListView
     */
    private VideoAdapter mAdapter;

    /**
     * Factory method that makes teh explicit intent used by the
     * DownloadVideoService to call back to this Activity.
     *
     * @param context
     * @return Intent to start this Activity.
     */
    public static Intent makeIntent(Context context) {
        return new Intent(context, ListVideosActivity.class);
    }

    /**
     * Hook method called when a new instance of Activity is created.
     * One time initialization code goes here, e.g., storing Views and
     * initialize the LoaderManager and GenericActivity framework.
     *
     * @param savedInstanceState object that contains saved state information.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Call up to initialize the superclass.
        super.onCreate(savedInstanceState);

        // Initialize the default layout.
        setContentView(R.layout.list_videos_activity);

        // Initialize views and view-related fields.
        initializeViews();

        // Initialize the Cursor Loader that updates the video list of
        // videos available from Video Service.
        getLoaderManager().initLoader(0,
                null,
                this);

        // Invoke the special onCreate() method in GenericActivity,
        // passing in the VideoPresenter class to instantiate/manage
        // and "this" to provide VideoPresenter with the
        // MVP.VideosRequiredViewOps instance.
        super.onCreate(VideosPresenter.class,
                this);
    }

    /**
     * Initialize views and view-related fields.
     */
    private void initializeViews() {
        // Get reference to the ListView for displaying the results
        // entered.
        mVideosList =
                (ListView) findViewById(R.id.videoList);

        // Get reference to Floating Action Button.
        mUploadVideoButton =
                (FloatingActionButton) findViewById(R.id.fabButton);

        // Displays a chooser dialog that gives options to upload
        // video from either the Gallery or the VideoRecorder.
        mUploadVideoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                displayChooserDialog();
            }
        });

        // Initialize the Video Adapter.
        mAdapter = new VideoAdapter(this,
                null,
                0);

        // Set the ListView to the Adapter.
        mVideosList.setAdapter(mAdapter);

        // Set the listener that handles events when the user clicks on
        // a Video in the ListView.
        mVideosList.setOnItemClickListener(this);
    }

    /**
     * Displays a chooser dialog that gives the user tho option of
     * uploading the video from either the Gallery or the VideoRecorder.
     */
    private void displayChooserDialog() {
        // Create an Intent that will start an Activity to get a Video
        // from the Gallery.
        final Intent videoGalleryIntent =
                new Intent(Intent.ACTION_GET_CONTENT)
                        .setType("video/*")
                        .putExtra(Intent.EXTRA_LOCAL_ONLY,
                                true);

        // Create an Intent that will start an Activity to Record the
        // Video.
        final Intent recordVideoIntent =
                new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // Intent that wraps the given target Intent and shows a
        // chooser dialog containing the Apps that can handle the
        // target Intent.
        final Intent chooserIntent =
                Intent.createChooser(videoGalleryIntent,
                        "Upload Video via");

        // Add RecordVideo Intent to add app that can record video
        // to the chooser dialog.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                new Intent[]{
                        recordVideoIntent
                });

        // Starts an Activity to get the Video either by the Gallery
        // or the VideoRecorder.
        startActivityForResult(chooserIntent,
                REQUEST_GET_VIDEO);

    }

    /**
     * Hook method called immediately after onStart().
     */
    @Override
    protected void onResume() {
        // Call up to the super class.
        super.onResume();

        // Restart the Loader.
        getLoaderManager().restartLoader(0,
                null,
                this);
    }

    /**
     * Hook method called after the Activity launched by passing the
     * chooserIntent to startActivityForResult() finishes, providing
     * the requestCode it was started with, the resultCode it
     * returned, and any additional data from it.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        // Check if the Result is Ok and upload the given Video to the
        // Video Service.
        if (resultCode == Activity.RESULT_OK) {
            // Inform the user the Video is being uploaded.
            Utils.showToast(this,
                    "Uploading video");

            // Upload the Video to the Video Service. This calls runs
            // in the Presenter layer since it may be a long duration
            // operation.
            getPresenter().uploadVideo(data.getData());
        } else
            Utils.showToast(this,
                    "Could not get video to upload");
    }

    /**
     * Callback method invoked when a video in this AdapterView has
     * been clicked.  This method gets the details of the video
     * clicked from the Cursor associated with the VideoAdapter and
     * starts DisplayVideoActivity to provide more information about
     * the video.
     */
    @Override
    public void onItemClick(AdapterView<?> parent,
                            View view,
                            int position,
                            long id) {
        // Get the Cursor from the Adapter.
        Cursor cursor = mAdapter.getCursor();

        // Move Cursor to the position where the Video was clicked.
        if (cursor != null
                && cursor.moveToPosition(position)) {
            // Get the videoId from Cursor.  The videoId is used to
            // identify the video on the local Android device and the
            // Video Service.
            // @@
            Long videoId =
                    cursor.getLong(cursor.getColumnIndex
                            (VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID));

            // Start the DisplayVideoActivity to show more info about
            // this video.
            startActivity(DisplayVideoActivity.makeVideoIntent
                    (getApplicationContext(),
                            videoId));

        }
    }


    /**
     * Hook method that gives a final chance to release resources and
     * stop spawned threads.  onDestroy() may not always be
     * called-when system kills hosting process.
     */
    @Override
    protected void onPause() {
        // Call onPause() in superclass.
        super.onPause();

    }

    /**
     * Called by Loader Manager to instantiate and return a new Loader
     * that loads the video data from our Video ContentProvider at the
     * given ContentUri.
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id,
                                         Bundle args) {
        // @@
        return new CursorLoader(this,                           // Application context
                VideoContract.VideoInfoEntry.CONTENT_URI,   // Table to query
                null,                                       // No projection to return
                null,                                       // No selection clause
                null,                                       // No selection arguments
                //  @@
                null);
    }

    /**
     * Called when a previously created loader is being reset, thus
     * making its data unavailable.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    /**
     * Called when a previously created loader has finished its load.
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader,
                               Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    /**
     * Called by the Presenter layer to show the result of loading
     * videos in Activity.
     */
    @Override
    public void showGetVideosResult(Boolean success) {
        if (success == false) {
            // @@
            Utils.showToast(this,
                    "Please connect  to the Video Service");
            // Close down the Activity.
            super.finish();
        } else
            Utils.showToast(this,
                    "Videos available from the Video Service");
    }

    /**
     * Hook method called to initialize the contents of the Activity's
     * standard options menu.
     *
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it
        // is present.
        getMenuInflater().inflate(R.menu.video_list,
                menu);
        return true;
    }

    /**
     * Hook method called whenever an item in your options menu is
     * selected
     *
     * @param item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
