/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.magnum.mobilecloud.video.controller;

import org.magnum.mobilecloud.video.auth.User;
import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.repository.UserRatingRepo;
import org.magnum.mobilecloud.video.repository.VideoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class VideoSvcCtrl 
{
//	private static final AtomicLong currentId = new AtomicLong (0L);
//	private Map<Long, Video> videos = new HashMap<>();
//	private Map<Long, byte[]> videosData = new HashMap<>();
    @Autowired
    private VideoFileManager videoFileManager;
//    private List<UserVideoRating> userVideoRatings = new ArrayList<>();
//    private Map<Long, AverageVideoRating> averageVideoRatings = new HashMap<>();

    @Autowired
    private VideoRepo videoRepo;

    @Autowired
    private UserRatingRepo userRatingRepo;

    public static final String DATA_PARAMETER = VideoSvcApi.DATA_PARAMETER;
	public static final String ID_PARAMETER = VideoSvcApi.ID_PARAMETER;
    public static final String RATING_PARAMETER = VideoSvcApi.RATING_PARAMETER;
	public static final String VIDEO_SVC_PATH = VideoSvcApi.VIDEO_SVC_PATH;
	public static final String VIDEO_DATA_PATH = VideoSvcApi.VIDEO_DATA_PATH;


    /**
     * Returns the list of videos that have been added to the server as JSON
     * @param principal
     * @return
     */
	@RequestMapping(value = VIDEO_SVC_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList (Principal principal)
	{
        List<Video> videos = new ArrayList<>();
        for (Video video : videoRepo.findAll()) {
            videos.add (video);
        }
        return videos;

//		return videos.values();
	}

    /**
     * The "owner" member variable of the Video must be set to the name of the currently
     * authenticated Principal.
     * If a Video already exists, it should not be overwritten unless the name of the authenticated
     * Principal matches the name of the owner member variable of the Video.
     * The video metadata is provided as an application/json request body. The JSON should generate
     * a valid instance of the Video class when deserialized by Spring's default Jackson library.
     * @param video
     * @param principal
     * @return the JSON representation of the Video object that was stored along with any updates to
     * that object made by the server
     */
    @RequestMapping(value = VIDEO_SVC_PATH, method = RequestMethod.POST)
    public @ResponseBody Video addVideo (@RequestBody Video video, Principal principal)
    {
        String user = principal.getName();

        Video storedVideo = videoRepo.findOne (video.getId());
        if (storedVideo != null) {
            if (storedVideo.getOwner().equals (user)) {
                video.setOwner (user);
                videoRepo.delete (storedVideo);
                storedVideo = videoRepo.save (video);
            }
        }
        else {
//            video.setId (currentId.incrementAndGet());
            
            long max = 0;
            for (Video v : videoRepo.findAll()) {
            	if (max < v.getId()) {
            		max = v.getId();
            	}
            }
            max++;
            video.setId (max);
            
            video.setOwner (user);
            storedVideo = videoRepo.save (video);
        }

        return storedVideo;

//        if (videos.containsKey (video.getId())) {
//            Video storedVideo = videos.get (video.getId());
//            if (storedVideo.getOwner().equals (user)) {
//                video.setOwner (user);
//                videos.put (video.getId(), video);
//            }
//        }
//        else {
//            video.setId (currentId.incrementAndGet());
//            video.setOwner (user);
//
//            videos.put (video.getId(), video);
//        }
//
//        return videos.get (video.getId());
    }

    /**
     * Before saving the binary data received, the service should check to ensure that the name of
     * the currently authenticated Principal matches the owner of the Video. If not, a HTTP 403
     * response should be sent to the client.
     * The binary mpeg data for the video should be provided in a multipart request as a part with
     * the key "data". The id in the path should be replaced with the unique identifier generated
     * by the server for the Video. A client MUST create a Video first by sending a POST to "/video"
     * and getting the identifier for the newly created Video object before sending a POST to
     * "/video/{id}/data".
     * The endpoint should return a VideoStatus object with state=VideoState.READY if the request
     * succeeds and the appropriate HTTP error status otherwise. VideoState.PROCESSING is not used
     * in this assignment but is present in VideoState.
     * @param id
     * @param videoData
     * @param principal
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = VIDEO_DATA_PATH, method = RequestMethod.POST)
    public @ResponseBody VideoStatus setVideoData (@PathVariable(ID_PARAMETER) long id,
                                                   @RequestParam(DATA_PARAMETER) MultipartFile videoData,
                                                   Principal principal,
                                                   HttpServletResponse response) throws IOException
    {
        if (id <= 0) {
            response.sendError (404);
            return new VideoStatus (VideoStatus.VideoState.FAILED);
        }

        Video video = videoRepo.findOne (id);
        if (video == null) {
            response.sendError (404);
            return new VideoStatus (VideoStatus.VideoState.FAILED);
        }

        if (!video.getOwner().equals (principal.getName())) {
            response.sendError (403);
            return new VideoStatus (VideoStatus.VideoState.FAILED);
        }

//        videosData.put (id, videoData.getBytes());
        videoFileManager.saveVideoData (video, videoData.getInputStream());

        return new VideoStatus (VideoStatus.VideoState.READY);

//        if ((id <= 0) || (!videos.containsKey (id))) {
//            response.sendError (404);
//            return new VideoStatus (VideoStatus.VideoState.FAILED);
//        }
//
//        if (!videos.get (id).getOwner().equals (principal.getName())) {
//            response.sendError (403);
//            return new VideoStatus (VideoStatus.VideoState.FAILED);
//        }
//
//        videosData.put (id, videoData.getBytes());
//
////        VideoFileManager videoDataMgr = VideoFileManager.get();
////        videoDataMgr.saveVideoData (videos.get (id), videoData.getInputStream());
//
//        return new VideoStatus (VideoStatus.VideoState.READY);
    }

    /**
     * Returns the binary mpeg data (if any) for the video with the given identifier. If no mpeg
     * data has been uploaded for the specified video, then the server should return a 404 status
     * code
     * @param id
     * @param principal
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = VIDEO_DATA_PATH, method = RequestMethod.GET)
    public @ResponseBody void getVideoData (@PathVariable(ID_PARAMETER) long id,
                                            Principal principal,
                                            HttpServletResponse response) throws IOException
    {
        if (id <= 0) {
            response.sendError (404);
            return;
        }

        Video video = videoRepo.findOne (id);
        if (video == null) {
            response.sendError (404);
            return;
        }

//        if ((id <= 0) || (!videos.containsKey (id))) {
//            response.sendError (404);
//            return;
//        }

//        response.getOutputStream().write (videosData.get (id));
        videoFileManager.copyVideoData (video, response.getOutputStream());

//		VideoFileManager videoDataMgr = VideoFileManager.get();
//		videoDataMgr.copyVideoData (videos.get (id), response.getOutputStream());
    }

    /**
     * Returns the video with the given id or 404 if the video is not found
     * @param id
     * @param principal
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = VIDEO_SVC_PATH + "/{id}", method = RequestMethod.GET)
    public @ResponseBody Video getVideoById (@PathVariable(ID_PARAMETER) long id, Principal principal,
                                             HttpServletResponse response) throws IOException
    {
        if (id <= 0) {
            response.sendError (404);
            return null;
        }

        Video video = videoRepo.findOne (id);
        if (video == null) {
            response.sendError (404);
            return null;
        }

        return video;

//        if ((id <= 0) || (!videos.containsKey (id)))
//        {
//            response.sendError (404);
//            return null;
//        }
//
//        return videos.get (id);
    }

    /**
     * Only allows each user (e.g., authenticated Principal) to have a single rating for a video.
     * If a user has an existing rating for a Video, the existing rating should be overwritten
     * @param id
     * @param rating
     * @param principal
     * @param response
     * @return 200 Ok on success or 404 if the video is not found
     * @throws IOException
     */
    @RequestMapping(value = VIDEO_SVC_PATH + "/{id}/rating/{rating}", method = RequestMethod.POST)
    public @ResponseBody AverageVideoRating rateVideo (@PathVariable(ID_PARAMETER) long id,
                                                       @PathVariable(RATING_PARAMETER) int rating,
                                                       Principal principal,
                                                       HttpServletResponse response) throws IOException
    {
        if ((id <= 0) || (videoRepo.findOne (id) == null)) {
            response.sendError (404);
            return null;
        }

        String user = principal.getName();

        int count = 0;
        double ratingSum = 0;
        boolean found = false;

        for (UserVideoRating userVideoRating : userRatingRepo.findAll()) {
            if (id == userVideoRating.getVideoId()) {
                if (user.equals (userVideoRating.getUser())) {
                    found = true;
                    userVideoRating.setRating (rating);
                    userRatingRepo.delete (userVideoRating.getId());
                    userRatingRepo.save (userVideoRating);
                }
                count++;
                ratingSum += userVideoRating.getRating();
            }
        }
        if (!found) {
            userRatingRepo.save(new UserVideoRating(id, rating, user));
            count++;
            ratingSum += rating;
        }

        return new AverageVideoRating (ratingSum/count, id, count);

//        if ((id <= 0) || (!videos.containsKey (id)))
//        {
//            response.sendError (404);
//            return null;
//        }
//
//        String user = principal.getName();
//
//        int count = 0;
//        double ratingSum = 0;
//        boolean found = false;
//        for (UserVideoRating userVideoRating : userVideoRatings) {
//            if (id == userVideoRating.getVideoId()) {
//                if (user.equals (userVideoRating.getUser())) {
//                    found = true;
//                    userVideoRating.setRating (rating);
//                }
//                count++;
//                ratingSum += userVideoRating.getRating();
//            }
//        }
//        if (!found) {
//            userVideoRatings.add (new UserVideoRating (id, rating, user));
//            count++;
//            ratingSum += rating;
//        }
//
//        AverageVideoRating avgRating = new AverageVideoRating (ratingSum/count, id, count);
//        averageVideoRatings.put (id, avgRating);
//
//        return avgRating;
    }

    /**
     * Returns the AverageVideoRating for a Video, which contains the average star count for the
     * video across all users and the total number of users that have rated the video
     * @param id
     * @param principal
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = VIDEO_SVC_PATH + "/{id}/rating", method = RequestMethod.GET)
    public @ResponseBody AverageVideoRating getVideoRating (@PathVariable(ID_PARAMETER) long id,
                                                            Principal principal,
                                                            HttpServletResponse response) throws IOException
    {
        if ((id <= 0) || (videoRepo.findOne (id) == null)) {
            response.sendError (404);
            return null;
        }

        int count = 0;
        double ratingSum = 0;
        for (UserVideoRating userVideoRating : userRatingRepo.findAll()) {
            if (id == userVideoRating.getVideoId()) {
                count++;
                ratingSum += userVideoRating.getRating();
            }
        }

        double rating;
        if (count == 0) {
        	rating = 0;
        }
        else {
        	rating = ratingSum/count;
        }
        return new AverageVideoRating (rating, id, count);


//        if ((id <= 0) || (!videos.containsKey (id)))
//        {
//            response.sendError (404);
//            return null;
//        }
//
//        if (!averageVideoRatings.containsKey (id)) {
//            int count = 0;
//            double ratingSum = 0;
//            for (UserVideoRating userVideoRating : userVideoRatings) {
//                if (id == userVideoRating.getVideoId()) {
//                    count++;
//                    ratingSum += userVideoRating.getRating();
//                }
//            }
//
//            AverageVideoRating avgRating = new AverageVideoRating (ratingSum/count, id, count);
//            averageVideoRatings.put (id, avgRating);
//        }
//
//        return averageVideoRatings.get(id);
    }





    private String getUrl (long videoId)
    {
        String base_url = getUrlBaseForLocalServer();
        //String base_url = "http://10.100.0.192:8080;
        return base_url + "/video/" + videoId + "/data";
    }

    private String getUrlBaseForLocalServer() 
    {
       HttpServletRequest request = 
           ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
       
       return "http://" + request.getServerName() + 
    		   ((request.getServerPort() != 80) ? ":" + request.getServerPort() : "");
    }
	
}
