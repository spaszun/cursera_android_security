/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sps.videoserviceapp.videos.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.sps.videoserviceapp.rest.base.SecuredRestException;
import com.sps.videoserviceapp.rest.video.VideoService;
import com.sps.videoserviceapp.rest.video.VideoSvcApi;
import com.sps.videoserviceapp.rest.video.model.AverageVideoRating;
import com.sps.videoserviceapp.rest.video.model.Video;
import com.sps.videoserviceapp.videos.contentprovider.VideoContract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Define a sync adapter for the app.
 *
 * <p>This class is instantiated in {@link SyncService}, which also binds SyncAdapter to the system.
 * SyncAdapter should only be initialized in SyncService, never anywhere else.
 *
 * <p>The system calls onPerformSync() via an RPC call through the IBinder object supplied by
 * SyncService.
 */
class SyncAdapter extends AbstractThreadedSyncAdapter {
    public static final String TAG = "SyncAdapter";

    /**
     * Content resolver, for performing database operations.
     */
    private final ContentResolver mContentResolver;

    /**
     * Project used when querying content provider. Returns all known fields.
     */
    private static final String[] PROJECTION = new String[] {
            VideoContract.Entry.COLUMN_NAME_ID,
            VideoContract.Entry.COLUMN_NAME_TITLE,
            VideoContract.Entry.COLUMN_NAME_URL,
            VideoContract.Entry.COLUMN_NAME_DURATION,
            VideoContract.Entry.COLUMN_NAME_LOCATION,
            VideoContract.Entry.COLUMN_NAME_SUBJECT,
            VideoContract.Entry.COLUMN_NAME_CONTENT_TYPE,
            VideoContract.Entry.COLUMN_NAME_RATING};

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
    public static final int COLUMN_TITLE = 1;
    public static final int COLUMN_URL = 2;
    public static final int COLUMN_DURATION = 3;
    public static final int COLUMN_LOCATION = 4;
    public static final int COLUMN_SUBJECT = 5;
    public static final int COLUMN_CONTENT_TYPE = 6;
    public static final int COLUMN_RATING = 7;

    private final VideoService videoService = new VideoService();

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
        //videoService.init();
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
        //videoService.init();
    }

    /**
     * Called by the Android system in response to a request to run the sync adapter. The work
     * required to read data from the network, parse it, and store it in the content provider is
     * done here. Extending AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter
     * run on a background thread. For this reason, blocking I/O and other long-running tasks can be
     * run <em>in situ</em>, and you don't have to set up a separate thread for them.
     .
     *
     * <p>This is where we actually perform any work required to perform a sync.
     * {@link AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread,
     * so it is safe to peform blocking I/O here.
     *
     * <p>The syncResult argument allows you to pass information back to the method that triggered
     * the sync.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        Log.i(TAG, "Beginning network synchronization");
        try {
            VideoSvcApi api = videoService.getService();
            if (api != null) {
                Collection<Video> videos = api.getVideoList();
                updateLocalVideoData(videos, syncResult);
            }
            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (RemoteException e) {
            Log.e(TAG, "Error updating database: " + e.toString());
            syncResult.databaseError = true;
            return;
        } catch (OperationApplicationException e) {
            Log.e(TAG, "Error updating database: " + e.toString());
            syncResult.databaseError = true;
            return;
        } catch (SecuredRestException e) {
            Log.e(TAG, "Server Unavailable " + e.toString());
            syncResult.databaseError = true;
            return;
        }
        Log.i(TAG, "Network synchronization complete");
    }

    /**
     * Read XML from an input stream, storing it into the content provider.
     *
     * <p>This is where incoming data is persisted, committing the results of a sync. In order to
     * minimize (expensive) disk operations, we compare incoming data with what's already in our
     * database, and compute a merge. Only changes (insert/update/delete) will result in a database
     * write.
     *
     * <p>As an additional optimization, we use a batch operation to perform all database writes at
     * once.
     *
     * <p>Merge strategy:
     * 1. Get cursor to all items in feed<br/>
     * 2. For each item, check if it's in the incoming data.<br/>
     *    a. YES: Remove from "incoming" list. Check if data has mutated, if so, perform
     *            database UPDATE.<br/>
     *    b. NO: Schedule DELETE from database.<br/>
     * (At this point, incoming database only contains missing items.)<br/>
     * 3. For any items remaining in incoming list, ADD to database.
     */
    public void updateLocalVideoData(final Collection<Video> videos, final SyncResult syncResult) throws OperationApplicationException, RemoteException {

        final ContentResolver contentResolver = getContext().getContentResolver();

        Log.i(TAG, "Videos to synchronize " + videos.size() + " entries");


        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        // Build hash table of incoming entries
        HashMap<Long, Video> entryMap = new HashMap<Long, Video>();
        for (Video v : videos) {
            entryMap.put(v.getId(), v);
        }

        // Get list of all items
        Log.i(TAG, "Fetching local entries for merge");
        Uri uri = VideoContract.Entry.CONTENT_URI; // Get all entries
        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        assert c != null;
        Log.i(TAG, "Found " + c.getCount() + " local entries. Computing merge solution...");

        // Find stale data
        int id;
        String title;
        String url;
        long duration;
        String location;
        String subject;
        String contentType;
        double rating;
        while (c.moveToNext()) {
            syncResult.stats.numEntries++;
            id = c.getInt(COLUMN_ID);
            title = c.getString(COLUMN_TITLE);
            url = c.getString(COLUMN_URL);
            duration = c.getInt(COLUMN_DURATION);
            location = c.getString(COLUMN_LOCATION);
            subject = c.getString(COLUMN_SUBJECT);
            contentType = c.getString(COLUMN_CONTENT_TYPE);
            rating = c.getDouble(COLUMN_RATING);
            Video match = entryMap.get(id);
            if (match != null) {
                // Entry exists. Remove from entry map to prevent insert later.
                entryMap.remove(id);
                // Check to see if the entry needs to be updated
                Uri existingUri = VideoContract.Entry.CONTENT_URI.buildUpon()
                        .appendPath(Integer.toString(id)).build();
                double newRating = getRating(id);
                if ((match.getTitle() != null && !match.getTitle().equals(title)) ||
                        (match.getTitle() != null && !match.getTitle().equals(title)) ||
                        (match.getUrl() != null && !match.getUrl().equals(url)) ||
                        (match.getDuration() != duration) ||
                        (newRating != rating) ||
                        (match.getLocation() != null && !match.getLocation().equals(location)) ||
                        (match.getSubject()!= null && !match.getSubject().equals(subject)) ||
                        (match.getContentType() != null && !match.getContentType().equals(contentType))) {

                    // Update existing record
                    Log.i(TAG, "Scheduling update: " + existingUri);
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(VideoContract.Entry.COLUMN_NAME_TITLE, match.getTitle())
                            .withValue(VideoContract.Entry.COLUMN_NAME_URL, match.getUrl())
                            .withValue(VideoContract.Entry.COLUMN_NAME_DURATION, match.getDuration())
                            .withValue(VideoContract.Entry.COLUMN_NAME_LOCATION, match.getLocation())
                            .withValue(VideoContract.Entry.COLUMN_NAME_SUBJECT, match.getSubject())
                            .withValue(VideoContract.Entry.COLUMN_NAME_CONTENT_TYPE, match.getContentType())
                            .withValue(VideoContract.Entry.COLUMN_NAME_RATING, newRating)
                            .build());
                    syncResult.stats.numUpdates++;
                } else {
                    Log.i(TAG, "No action: " + existingUri);
                }
            } else {
                // Entry doesn't exist. Remove it from the database.
                Uri deleteUri = VideoContract.Entry.CONTENT_URI.buildUpon()
                        .appendPath(Integer.toString(id)).build();
                Log.i(TAG, "Scheduling delete: " + deleteUri);
                batch.add(ContentProviderOperation.newDelete(deleteUri).build());
                syncResult.stats.numDeletes++;
            }
        }
        c.close();

        // Add new items
        for (Video e : entryMap.values()) {
            Log.i(TAG, "Scheduling insert: entry_id=" + e.getId());
            double newRating = getRating(e.getId());
            batch.add(ContentProviderOperation.newInsert(VideoContract.Entry.CONTENT_URI)
                    .withValue(VideoContract.Entry.COLUMN_NAME_ID, e.getId())
                    .withValue(VideoContract.Entry.COLUMN_NAME_TITLE, e.getTitle())
                    .withValue(VideoContract.Entry.COLUMN_NAME_URL, e.getUrl())
                    .withValue(VideoContract.Entry.COLUMN_NAME_DURATION, e.getDuration())
                    .withValue(VideoContract.Entry.COLUMN_NAME_LOCATION, e.getLocation())
                    .withValue(VideoContract.Entry.COLUMN_NAME_SUBJECT, e.getSubject())
                    .withValue(VideoContract.Entry.COLUMN_NAME_CONTENT_TYPE, e.getContentType())
                    .withValue(VideoContract.Entry.COLUMN_NAME_RATING, newRating)
                    .build());
            syncResult.stats.numInserts++;
        }
        Log.i(TAG, "Merge solution ready. Applying batch update");
        mContentResolver.applyBatch(VideoContract.CONTENT_AUTHORITY, batch);
        mContentResolver.notifyChange(
                VideoContract.Entry.CONTENT_URI, // URI where data was modified
                null,                           // No local observer
                false);                         // IMPORTANT: Do not sync to network
        // This sample doesn't support uploads, but if *your* code does, make sure you set
        // syncToNetwork=false in the line above to prevent duplicate syncs.
    }

    private double getRating(long id) {
        try {
            AverageVideoRating rating = VideoService.getService().getVideoRating(id);
            return rating.getRating();
        } catch (Exception e) {
            //ignore
        }
        return 0.0;
    }
}

