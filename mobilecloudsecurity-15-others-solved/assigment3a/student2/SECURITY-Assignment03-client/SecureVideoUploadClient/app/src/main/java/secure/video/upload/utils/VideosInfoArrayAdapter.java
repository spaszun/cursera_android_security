package secure.video.upload.utils;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import secure.video.upload.R;
import secure.video.upload.model.mediator.webdata.Video;

public class VideosInfoArrayAdapter extends ArrayAdapter<Video>
{
    public VideosInfoArrayAdapter (Context context)
    {
        super (context, R.layout.video_info_row);
    }

    public VideosInfoArrayAdapter (Context context, List<Video> objects)
    {
        super (context, R.layout.video_info_row, objects);
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent)
    {
        Video video = getItem (position);
        if (convertView == null) {
            convertView = LayoutInflater.from (getContext()).inflate (R.layout.video_info_row,
                    parent, false);
        }

        TextView id = (TextView) convertView.findViewById (R.id.videoInfoID);
        TextView title  = (TextView) convertView.findViewById (R.id.videoInfoTitle);
        TextView duration = (TextView) convertView.findViewById (R.id.videoInfoDuration);
        TextView contentType = (TextView) convertView.findViewById (R.id.videoInfoContentType);
        TextView dataUrl = (TextView) convertView.findViewById (R.id.videoInfoDataUrl);
        TextView totalRatings = (TextView) convertView.findViewById (R.id.videoTotalRatings);
        RatingBar ratingBar = (RatingBar) convertView.findViewById (R.id.videoInfoRatingBar);

        id.setText ("ID: " + String.valueOf (video.getId()));
        title.setText ("Title: " + video.getTitle());

        long min = TimeUnit.MILLISECONDS.toMinutes (video.getDuration());
        long sec = TimeUnit.MILLISECONDS.toSeconds (video.getDuration()) -
                TimeUnit.MINUTES.toSeconds (TimeUnit.MILLISECONDS.toMinutes (video.getDuration()));

        duration.setText (String.format ("Duration: %d:%d", min, sec));
        contentType.setText ("Content type: " + video.getContentType());
        dataUrl.setText ("Url: " + video.getUrl());
        if (video.getAverageVideoRating() != null) {
            totalRatings.setText ("Number of voters: " + String.valueOf (
                    video.getAverageVideoRating().getTotalRatings()));
            ratingBar.setNumStars (5);
            ratingBar.setRating((float) video.getAverageVideoRating().getRating());
            ratingBar.setOnTouchListener (new View.OnTouchListener() {
                @Override
                public boolean onTouch (View v, MotionEvent event)
                {
                    return true;
                }
            });
        }

        return convertView;
    }
}
