package secure.video.upload.activities;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;

import secure.video.upload.R;
import secure.video.upload.model.mediator.webdata.AverageVideoRating;
import secure.video.upload.model.mediator.webdata.Video;
import secure.video.upload.provider.VideoContract;
import secure.video.upload.utils.LifecycleLoggingActivity;
import secure.video.upload.utils.VideosInfoArrayAdapter;

public class VideosInfoActivity extends LifecycleLoggingActivity
{
    public static final int CAPTURE_VIDEO_REQUEST = 0;

    /**
     * The ListView that will display the results to the user.
     */
    private ListView mListView;

    /**
     * A custom ArrayAdapter used to display the list of Video objects
     */
    private VideosInfoArrayAdapter mAdapter;

    public static final String TAG_VIDEO_ID = "ID";
    public static final String TAG_VIDEO_TITLE = "TITLE";
    public static final String TAG_VIDEO_DURATION = "DURATION";
    public static final String TAG_VIDEO_CONTENT_TYPE = "CONTENT_TYPE";
    public static final String TAG_VIDEO_DATA_URL = "DATA_URL";
    public static final String TAG_VIDEO_STARS = "STARS";
    public static final String TAG_TOTAL_VIDEO_RATINGS = "TOTAL_VIDEO_RATINGS";

    public static final String VIDEOS_TAG = "Videos";


    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_videos_info);

        mListView = (ListView) findViewById (R.id.videosInfoList);
        mAdapter = new VideosInfoArrayAdapter (this);
        mListView.setAdapter (mAdapter);
        mListView.setOnItemClickListener (new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick (AdapterView<?> parent, View view, int position, long id)
            {
                for(int a = 0; a < parent.getChildCount(); a++)
                {
                    parent.getChildAt(a).setBackgroundColor (Color.WHITE);
                }

                view.setBackgroundColor (Color.LTGRAY);

                showVideoInfo ((Video) parent.getItemAtPosition (position));
            }
        });

//        VideosCollection videos = getIntent().getParcelableExtra (VIDEOS_TAG);
//        displayResults (videos.get());


    }

    @Override
    protected void onStart()
    {
        super.onStart();

        Cursor cursor = getContentResolver().query (VideoContract.VideoEntry.CONTENT_URI, null,
                null, null, null);
        if (!cursor.moveToFirst()) {
            displayResults (null);
            return;
        }

        Collection<Video> videos = new ArrayList<>();
        do {
            Video video = new Video();
            video.setTitle (cursor.getString (cursor.getColumnIndex (VideoContract.VideoEntry.COLUMN_TITLE)));
            video.setUrl (cursor.getString (cursor.getColumnIndex (VideoContract.VideoEntry.COLUMN_DATA_URL)));
            video.setDuration (cursor.getLong (cursor.getColumnIndex (VideoContract.VideoEntry.COLUMN_DURATION)));
            video.setId (cursor.getLong (cursor.getColumnIndex (VideoContract.VideoEntry.COLUMN_ID)));
            video.setContentType (cursor.getString (cursor.getColumnIndex (VideoContract.VideoEntry.COLUMN_CONTENT_TYPE)));
            double rating = cursor.getDouble (cursor.getColumnIndex (VideoContract.VideoEntry.COLUMN_STAR_RATING));
            int nRatings = cursor.getInt (cursor.getColumnIndex (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS));
            video.setAverageVideoRating (new AverageVideoRating (rating, video.getId(), nRatings));

            videos.add (video);
        }
        while (cursor.moveToNext());
        cursor.close();
        displayResults (videos);
    }

    public void notifyMediatorNotConnected()
    {
        Toast.makeText (this, "Impossible to download the video", Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     * Displays the video info in the list view
     * @param results list of <code>Video</code> info object to display
     */
    private void displayResults (Collection<Video> results)
    {
        if ((results == null) || (results.size() == 0)) {
            Toast.makeText (this, "No videos found", Toast.LENGTH_SHORT).show();
            return;
        }

        mAdapter.clear();
        mAdapter.addAll (results);
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Method called when one of the video info is selected
     * @param selectedVideo selected video
     */
    public void showVideoInfo (Video selectedVideo)
    {
        if (selectedVideo == null) {
            Toast.makeText (getApplicationContext(), "Please select one of the videos",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent (getApplicationContext(), SingleVideoActivity.class);
        intent.putExtra (TAG_VIDEO_ID, selectedVideo.getId());
        intent.putExtra (TAG_VIDEO_TITLE, selectedVideo.getTitle());
        intent.putExtra (TAG_VIDEO_DURATION, selectedVideo.getDuration());
        intent.putExtra (TAG_VIDEO_CONTENT_TYPE, selectedVideo.getContentType());
        intent.putExtra (TAG_VIDEO_DATA_URL, selectedVideo.getUrl());
        if (selectedVideo.getAverageVideoRating() != null) {
            intent.putExtra (TAG_VIDEO_STARS, selectedVideo.getAverageVideoRating().getRating());
            intent.putExtra (TAG_TOTAL_VIDEO_RATINGS, selectedVideo.getAverageVideoRating().getTotalRatings());
        }
        startActivity (intent);
    }

    public void uploadVideo (View view)
    {
        startActivityForResult (new Intent (this, CaptureVideoActivity.class), CAPTURE_VIDEO_REQUEST);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CAPTURE_VIDEO_REQUEST) {
            if (resultCode == RESULT_OK) {
                int duration = data.getIntExtra (CaptureVideoActivity.TAG_VIDEO_DURATION, 0);
                String videoDataFile = data.getStringExtra (CaptureVideoActivity.TAG_VIDEO_DATA_FILE);
                Log.d (TAG, "Video file: " + videoDataFile);

                Intent intent = new Intent (this, UploadVideoActivity.class);
                intent.putExtra (CaptureVideoActivity.TAG_VIDEO_DURATION, duration);
                intent.putExtra (CaptureVideoActivity.TAG_VIDEO_DATA_FILE, videoDataFile);
                startActivity (intent);
            }
        }
    }
}
