package org.magnum.mobilecloud.video.repository;

import org.magnum.mobilecloud.video.model.Video;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * Created by Q1SW on 14-08-2015.
 */
@Component
public interface VideoRepository extends CrudRepository<Video, Long>{
}
