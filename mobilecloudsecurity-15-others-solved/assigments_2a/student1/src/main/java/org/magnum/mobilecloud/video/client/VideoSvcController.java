/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.magnum.mobilecloud.video.client;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.controller.VideoFileManager;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.model.VideoStatus.VideoState;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class VideoSvcController {

	private static final AtomicLong currentId = new AtomicLong(0L);

	private Map<Long, Video> videos = new HashMap<Long, Video>();
	
	private Map<Long, List<String>> users = new HashMap<Long, List<String>>();
	
	private Map<Long, String> usersData = new HashMap<Long, String>();
	
	//private AverageVideoRating vR;

	public Video save(Video entity) {
		checkAndSetId(entity);
		videos.put(entity.getId(), entity);
		return entity;
	}

	private void checkAndSetId(Video entity) {
		if (entity.getId() == 0) {
			entity.setId(currentId.incrementAndGet());
		}
	}

	@RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList() {
		return videos.values();
	}

	@RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video v,Principal p) {
		save(v);
		v.setUrl(getDataUrl(v.getId()));
		String username = p.getName();
		if(!usersData.containsKey(v.getId())){
			usersData.put(v.getId(), username);
		}
		
		return v;
	}

	@RequestMapping(value = VideoSvcApi.VIDEO_DATA_PATH, method = RequestMethod.POST)
	public @ResponseBody VideoStatus setVideoData(@PathVariable("id") long id,Principal p,
			@RequestParam("data") MultipartFile file,
			HttpServletResponse response) throws IOException {
		String username = p.getName();
		if(usersData.containsKey(id)){
			if(!usersData.get(id).equals(username)){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return null;
			}
		}
		
		
		if (videos.containsKey(id)) {
			VideoFileManager.get().saveVideoData(videos.get(id),
					file.getInputStream());
			return new VideoStatus(VideoState.READY);
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}

	}

	@RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH+"/{id}/rating/{rating}", method = RequestMethod.POST)
	public @ResponseBody AverageVideoRating rateVideo(@PathVariable("id") long id,
			@PathVariable("rating") int rating,Principal p, HttpServletResponse response)
			throws IOException {
		String username = p.getName();
		if(users.containsKey(id)){
			List<String> list=users.get(id);
			if(!list.contains(username)){
				list.add(username);
				users.remove(users.get(id));
				users.put(id, list);
			}
			else{
				Video video = videos.get(id);
				return video.getRating();
			}
		}
		else{
			List<String> list= new ArrayList<String>();
			list.add(username);
			users.put(id, list);
		}
		
		if (videos.containsKey(id) ) {
			Video video = videos.get(id);
			if (video.getRating()==null || (video.getRating()!=null && video.getRating().getTotalRatings() != 0)) {
				AverageVideoRating vR = new AverageVideoRating(Double.valueOf(rating), id, video.getRating()!=null?video.getRating().getTotalRatings()+1:1);
				video.setRating(vR);
				}
			return video.getRating();
		} else {
			List<String> list= new ArrayList<String>();
			list.add(username);
			users.put(id, list);
			Video video = videos.get(id);
			return video.getRating();
		}

	}
	
	@RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH+"/{id}/rating", method = RequestMethod.GET)
	public @ResponseBody AverageVideoRating getVideoRating(@PathVariable("id") long id){
		if(videos.get(id)==null){
			return new AverageVideoRating(Double.valueOf(0), id, 1);
		}else{
			return videos.get(id).getRating();
		}
	
		
	}

	@RequestMapping(value = VideoSvcApi.VIDEO_DATA_PATH, method = RequestMethod.GET)
	public void getData(@PathVariable("id") long id,
			HttpServletResponse response) throws IOException {
		if (videos.containsKey(id)) {
			VideoFileManager.get().copyVideoData(videos.get(id),
					response.getOutputStream());
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	private String getDataUrl(long videoId) {
		String url = getUrlBaseForLocalServer() + "/video/" + videoId + "/data";
		//return url;
		return "https://192.168.56.1:8443" + "/video/" + videoId + "/data";
	}

	private String getUrlBaseForLocalServer() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		String base = "http://"
				+ request.getServerName()
				+ ((request.getServerPort() != 80) ? ":"
						+ request.getServerPort() : "");
		return base;
	}

	/**
	 * You will need to create one or more Spring controllers to fulfill the
	 * requirements of the assignment. If you use this file, please rename it to
	 * something other than "AnEmptyController"
	 * 
	 * 
	 * ________ ________ ________ ________ ___ ___ ___ ________ ___ __ |\
	 * ____\|\ __ \|\ __ \|\ ___ \ |\ \ |\ \|\ \|\ ____\|\ \|\ \ \ \ \___|\ \
	 * \|\ \ \ \|\ \ \ \_|\ \ \ \ \ \ \ \\\ \ \ \___|\ \ \/ /|_ \ \ \ __\ \ \\\
	 * \ \ \\\ \ \ \ \\ \ \ \ \ \ \ \\\ \ \ \ \ \ ___ \ \ \ \|\ \ \ \\\ \ \ \\\
	 * \ \ \_\\ \ \ \ \____\ \ \\\ \ \ \____\ \ \\ \ \ \ \_______\ \_______\
	 * \_______\ \_______\ \ \_______\ \_______\ \_______\ \__\\ \__\
	 * \|_______|\|_______|\|_______|\|_______|
	 * \|_______|\|_______|\|_______|\|__| \|__|
	 * 
	 * 
	 */

}
