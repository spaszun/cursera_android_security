package vandy.mooc.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import vandy.mooc.MVP;
import vandy.mooc.R;
import vandy.mooc.common.GenericActivity;
import vandy.mooc.common.Utils;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.services.DownloadVideoService;
import vandy.mooc.presenter.DisplayVideoPresenter;
import vandy.mooc.view.ui.FloatingActionButton;

/**
 * This Activity shows the details of a Video.  A user can play a
 * video if (1) it's already on the device or (2) it's downloaded if
 * not yet present.  It extends GenericActivity that provides a
 * framework for automatically handling runtime configuration changes
 * of an DisplayVideoOps object, which plays the role of the
 * "Presenter" in the MVP pattern.  The VideoOps.View interface is
 * used to minimize dependencies between the View and Presenter
 * layers.  It also implements the OnRatingBarChangeListener, which
 * triggers a callback to onRatingChanged() when the user updates a
 * rating.  This Activity only runs in Portrait mode, which simplifies
 * certain aspects of its design since it needn't handle runtime
 * configuration changes.
 */
public class DisplayVideoActivity
        extends GenericActivity<MVP.DisplayVideoRequiredViewOps,
        MVP.DisplayVideoProvidedPresenterOps,
        DisplayVideoPresenter>
        implements RatingBar.OnRatingBarChangeListener,
        MVP.DisplayVideoRequiredViewOps {

    /**
     * Key used to add deoId as an extra to the Intent.  The videoId
     * is needed to download the video from the video Service.
     */
    private static final String KEY_VIDEO_ID =
            "video_id";

    /**
     * Default Id is used when the videoId returned from Intent is null.
     */
    private static final long DEFAULT_VIDEO_ID = 0;

    /**
     * BroadcastReceiver that handles the result after a video is
     * downloaded.
     */
    private DownloadResultReceiver mDownloadResultReceiver;

    /**
     * FloatingActionButton (FAB) used to play video.
     */
    private FloatingActionButton mPlayVideoButton;

    /**
     * FloatingActionButton used to download video.
     */
    private FloatingActionButton mDownloadVideoButton;

    /**
     * Fade-in animation used to fade in the image when the thumbnail
     * of video is loaded.
     */
    private Animation mFadeAnimation;

    /**
     * Display the thumbnail of the video.
     */
    private ImageView mVideoImg;

    /**
     * Display the name of the video.
     */
    private TextView mTitleText;

    /**
     * Display the rating of the video.
     */
    private RatingBar mRatingBar;

    /**
     * Video metadata object being displayed by this Class.
     */
    private Video mVideo;

    /**
     * Id of the video that's being displayed.
     */
    private long mVideoId;

    /**
     * File path of the video in local storage of device.
     */
    private String mFilePath;

    /**
     * Factory method that makes teh explicit intent used by the
     * DownloadVideoService to call back to this Activity.
     *
     * @param context
     * @param videoId
     * @return Intent to start this Activity.
     */
    public static Intent makeVideoIntent(Context context,
                                         Long videoId) {
        return new Intent(context,
                DisplayVideoActivity.class)
                // Add videoId as an extra to the Intent.
                .putExtra(KEY_VIDEO_ID,
                        videoId);
    }

    /**
     * Hook method called when a new instance of Activity is created.
     * One time initialization code goes here, e.g., storing Views.
     *
     * @param savedInstanceState object that contains saved state information.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Call up to initialize the superclass.
        super.onCreate(savedInstanceState);

        // Initialize teh default layout.
        setContentView(R.layout.display_video_activity);

        // Invoke the special onCreate() method in GenericActivity,
        // passing in the DisplayVideoPresenter class to
        // instantiate/manage and "this" to provide VideoPresenter
        // with the MVP.DisplayVideosRequiredViewOps instance.
        super.onCreate(DisplayVideoPresenter.class,
                this);

        // Get the videoId from the Intent.  The videoId is used to
        // identify the video on the local Android device and on the
        // Video Service.
        mVideoId =
                getIntent().getLongExtra(KEY_VIDEO_ID,
                        DEFAULT_VIDEO_ID);

        // Hide the ActionBar.
        getActionBar().hide();

        // Initialize all views the view-related fields.
        initializeViews();

        // Initialize the BroadcastReceiver.
        mDownloadResultReceiver =
                new DownloadResultReceiver();

        // Get (possibly download) and display the requested video.
        getAndDisplayVideo();
    }

    /**
     * Initialize all views the view-related fields.
     */
    private void initializeViews() {
        mVideoImg =
                (ImageView) findViewById(R.id.imgVideo);

        mTitleText =
                (TextView) findViewById(R.id.textTitle);

        mRatingBar =
                (RatingBar) findViewById(R.id.ratingBar);

        mPlayVideoButton = (FloatingActionButton)
                findViewById(R.id.playFabButton);

        mDownloadVideoButton = (FloatingActionButton)
                findViewById(R.id.downloadFabButton);

        // Initialize the fade-in animation.
        mFadeAnimation =
                AnimationUtils.loadAnimation(this,
                        R.anim.image_fade_in);

        // This button is only displayed if we need to download the
        // video from Video Service.
        mDownloadVideoButton.setOnClickListener(null);

        // Set the listener for the Play button.
        mPlayVideoButton.setOnClickListener(new PlayVideoListener());
    }

    /**
     * Listener that plays the video when Play Button is clicked.
     */
    private class PlayVideoListener
            implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // Check if file is present in device and start the
            // activity that plays the video with the given file path.
            if (mFilePath != null)
                startVideoPlayingActivity
                        (Uri.parse(mFilePath));
            else
                // Stream the video from the Video Service.
                // @@
                startVideoPlayingActivity
                        (Uri.parse(mVideo.getUrl()));
        }
    }

    /**
     * Get and display the requested video.
     */
    private void getAndDisplayVideo() {
        // Get the video metadata by the given videoId.
        mVideo = getPresenter().getVideoById(mVideoId);

        // Display the title of the video.
        mTitleText.setText(mVideo.getTitle());

        // Display the rating of the video.
        mRatingBar.setRating
                ((float) mVideo.getRatingCount());

        // Set the listener for the rating bar.
        mRatingBar.setOnRatingBarChangeListener(this);

        // Get filePath by video name from VideoMediaStoreUtils.
        mFilePath = getPresenter().getPathByName(mVideo.getTitle());

        // Load the video thumbnail and display it.
        setImageBitmap();
    }

    /**
     * Hook method that is called when user resumes activity from
     * paused state (i.e., after a call to onPause()).  This method
     * register a BroadcastReceiver that receives result from the
     * DownloadVideoService when a video download completes.
     */
    @Override
    protected void onResume() {
        // Call up to the superclass.
        super.onResume();

        // Create an Intent filter that handles Intents from the
        // DownloadVideoService.
        IntentFilter intentFilter =
                new IntentFilter
                        (DownloadVideoService.ACTION_DOWNLOAD_SERVICE_RESPONSE);

        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);

        // Register BroadcastReceiver with the LocalBroadcastManager
        // to enhance security and performance.
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mDownloadResultReceiver,
                        intentFilter);
    }

    /**
     * Hook method that releases resources.
     */
    @Override
    protected void onPause() {
        // Call onPause() in superclass.
        super.onPause();

        // Unregister BroadcastReceiver.
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver
                        (mDownloadResultReceiver);
    }

    /**
     * Broadcast Receiver used to handle the result after the video is
     * downloaded by the DownloadVideoService.
     */
    private class DownloadResultReceiver
            extends BroadcastReceiver {
        /**
         * Hook method called by BroadcastReceiver that handles the
         * result broadcast by the DownloadVideoService.
         */
        @Override
        public void onReceive(Context context,
                              Intent intent) {
            // Get the status of the Video download.
            final int status =
                    intent.getIntExtra
                            (DownloadVideoService.KEY_STATUS,
                                    0);


            // Get the downloaded video.
            final long receivedVideoId =
                    intent.getLongExtra
                            (DownloadVideoService.KEY_DOWNLOAD_VIDEO_ID,
                                    DownloadVideoService.DEFAULT_VIDEO_ID);

            // Check if (1) the download operation is successful and
            // (2) the videoId that DownloadVideoService received is
            // same as the Id of the video the user is currently
            // viewing.
            if (status == DownloadVideoService.RESULT_OK
                    && receivedVideoId == mVideoId) {

                // Hide the DownloadVideoService button.
                mDownloadVideoButton.hideFloatingActionButton();

                // Get the file path of the video obtained from
                // DownloadVideoService.
                mFilePath =
                        intent.getStringExtra
                                (DownloadVideoService.KEY_FILE_PATH);
            }

            // Display the thumbnail of the video.
            setImageBitmap();
        }
    }

    /**
     * Sets the ImageView to display the Thumbnail of the Video.
     */
    // @@
    @SuppressWarnings("deprecation")
    private void setImageBitmap() {
        // Get the width of screen.
        final DisplayMetrics metrics =
                getResources().getDisplayMetrics();
        final int width = metrics.widthPixels;

        // Check if the file path of video is present.
        if (mFilePath != null) {
            // Starts an anonymous AsyncTask to load video Thumbnail
            // in a background thread.
            new AsyncTask<Void, Void, Bitmap>() {
                // Load the Video Thumbnail in background.
                @Override
                protected Bitmap doInBackground(Void... params) {
                    // Create mini thumbnail of the video.
                    final Bitmap thumbnail =
                            ThumbnailUtils.createVideoThumbnail
                                    (mFilePath,
                                            MediaStore.Images.Thumbnails.MINI_KIND);

                    // Scale the Bitmap to fill the width of the
                    // screen and return it.
                    return Bitmap.createScaledBitmap(thumbnail,
                            width,
                            350,
                            false);
                }

                // Display the Video thumbnail in UI Thread.
                @Override
                protected void onPostExecute(Bitmap result) {
                    // Check if there is a thumbnail.
                    if (result != null) {
                        // Display the Thumbnail with a fade-in
                        // animation.
                        mVideoImg.setImageBitmap(result);
                        mVideoImg.startAnimation(mFadeAnimation);
                    }
                }
            }.execute();

            // Filepath of video is not present.
        } else {
            // Show the DownloadVideo button.
            mDownloadVideoButton.setVisibility(View.VISIBLE);

            // Update the drawable icon of the video download button.
            mDownloadVideoButton.setFloatingActionButtonDrawable
                    (getResources().getDrawable(R.drawable.ic_cloud_download_white_24dp));

            // Set the listener for DownloadVideo button so it will
            // react properly if the user clicks it.
            mDownloadVideoButton.setOnClickListener(new DownloadVideoListener());
        }
    }

    /**
     * Listener that downloads the video when Download Button is
     * clicked.
     */
    private class DownloadVideoListener
            implements View.OnClickListener {
        @SuppressWarnings("deprecation")
        @Override
        public void onClick(View v) {
            // Disable the listener for the Download Button.
            mDownloadVideoButton.setOnClickListener(null);

            // Change the drawable of the Button to show
            // the progress of the download.
            mDownloadVideoButton
                    .setFloatingActionButtonDrawable
                            (getResources().getDrawable
                                    (R.drawable.ic_cloud_download_white_24dp));

            // Download the video with given videoId and title.
            getPresenter().downloadVideo(mVideoId,
                    mVideo.getTitle());
        }
    }

    /**
     * Start the Activity by Implicit intent to play the Video.
     *
     * @param videoLocation
     */
    public void startVideoPlayingActivity(Uri videoLocation) {
        // Implicit intent used to play the video.
        Intent videoIntent =
                new Intent(Intent.ACTION_VIEW)
                        .setDataAndType(videoLocation,
                                "video/*");

        // Verify that teh intent will resolve to an Activity.
        if (videoIntent.resolveActivity(getPackageManager()) != null)
            startActivity(videoIntent);
    }

    /**
     * Listener that updates a video's rating via the Video Service.
     */
    @Override
    public void onRatingChanged(final RatingBar ratingBar,
                                float rating,
                                boolean fromUser) {
        // Check if the rating was initiated by user or
        // programmatically.
        if (fromUser)
            // Update rating of video via the Video Service.
            getPresenter().addRating(mVideoId,
                    (int) rating);
    }

    @Override
    public void updateRating(Video receivedVideo) {
        // Check if any video is received as result.
        if (receivedVideo != null) {
            // Store the received Video.
            mVideo = receivedVideo;

            // Pop a Toast saying that the rating is updated.
            Utils.showToast(DisplayVideoActivity.this,
                    "Rating Updated..");
        } else
            // Pop a Toast saying that the rating is not updated.
            Utils.showToast(DisplayVideoActivity.this,
                    "Rating could not be Updated..");

        // Update the RatingBar to show the ratings of the video.
        mRatingBar.setRating
                ((float) mVideo.getRatingCount());
    }
}
