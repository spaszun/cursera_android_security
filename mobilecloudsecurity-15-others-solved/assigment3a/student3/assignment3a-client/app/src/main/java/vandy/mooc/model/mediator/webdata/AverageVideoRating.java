package vandy.mooc.model.mediator.webdata;

/**
 * Created by kaido on 31.08.2015.
 */
public class AverageVideoRating {

    private final double rating;

    private final long videoId;

    private final int totalRatings;

    public AverageVideoRating(double rating, long videoId, int totalRatings) {
        super();
        this.rating = rating;
        this.videoId = videoId;
        this.totalRatings = totalRatings;
    }

    public AverageVideoRating(double rating, long videoId, long totalRatings) {
        super();
        this.rating = rating;
        this.videoId = videoId;
        this.totalRatings = (int) totalRatings;
    }

    public double getRating() {
        return rating;
    }

    public long getVideoId() {
        return videoId;
    }

    public int getTotalRatings() {
        return totalRatings;
    }

}