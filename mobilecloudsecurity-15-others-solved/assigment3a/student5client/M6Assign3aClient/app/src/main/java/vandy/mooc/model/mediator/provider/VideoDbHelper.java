package vandy.mooc.model.mediator.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;


public class VideoDbHelper extends SQLiteOpenHelper {
    /**
     * Database name.
     */
    private static String DATABASE_NAME =
            "vandy_mooc_video_db";

    /**
     * Database version number, which is updated with each schema
     * change.
     */
    private static int DATABASE_VERSION = 2;

    /*
     * SQL create table statements.
     */

    /**
     * SQL statement used to create the Video table.
     */
    final String SQL_CREATE_VIDEO_INFO_TABLE =
            "CREATE TABLE "
                    + VideoContract.VideoInfoEntry.TABLE_NAME + " ("
                    + VideoContract.VideoInfoEntry._ID + " INTEGER PRIMARY KEY, "
                    + VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID + " INTEGER NOT NULL, "
                    + VideoContract.VideoInfoEntry.COLUMN_TITLE + " TEXT NOT NULL, "
                    + VideoContract.VideoInfoEntry.COLUMN_URL + " TEXT NOT NULL, "
                    + VideoContract.VideoInfoEntry.COLUMN_DURATION + " INTEGER NOT NULL, "
                    + VideoContract.VideoInfoEntry.COLUMN_LOCATION + " TEXT NOT NULL, "
                    + VideoContract.VideoInfoEntry.COLUMN_SUBJECT + " TEXT NOT NULL, "
                    + VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE + " TEXT NOT NULL, "
                    + VideoContract.VideoInfoEntry.COLUMN_RATING + " DOUBLE  "
                    +  " );";

    /**
     * Constructor - initialize database name and version, but don't
     * actually construct the database (which is done in the
     * onCreate() hook method). It places the database in the
     * application's cache directory, which will be automatically
     * cleaned up by Android if the device runs low on storage space.
     *
     */
    public VideoDbHelper(Context context) {
        super(context,
                context.getCacheDir()
                        + File.separator
                        + DATABASE_NAME,
                null,
                DATABASE_VERSION);
    }

    /**
     * Hook method called when the database is created.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Create the table.
        final String SQL_CREATE_VIDEO_INFO_TABLE =
                "CREATE TABLE "
                        + VideoContract.VideoInfoEntry.TABLE_NAME + " ("
                        + VideoContract.VideoInfoEntry._ID + " INTEGER PRIMARY KEY, "
                        + VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID + " INTEGER NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_TITLE + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_URL + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_DURATION + " INTEGER NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_LOCATION + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_SUBJECT + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE + " TEXT NOT NULL, "
                        + VideoContract.VideoInfoEntry.COLUMN_RATING + " REAL , "
                        + " UNIQUE (" + VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID + ", "
                        + VideoContract.VideoInfoEntry.COLUMN_TITLE
                        +" ) ON CONFLICT REPLACE);";
        sqLiteDatabase.execSQL(SQL_CREATE_VIDEO_INFO_TABLE);
    }

    /**
     * Hook method called when the database is upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,
                          int oldVersion,
                          int newVersion) {
        // Delete the existing tables.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "
                + VideoContract.VideoInfoEntry.TABLE_NAME);
        // Create the new tables.
        onCreate(sqLiteDatabase);
    }
}
