package vandy.mooc.view;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Collection;
import java.util.concurrent.Callable;

import vandy.mooc.R;
import vandy.mooc.model.mediator.webdata.CallableTask;
import vandy.mooc.model.mediator.webdata.TaskCallback;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.mediator.webdata.VideoServiceProxy;
import vandy.mooc.model.mediator.webdata.VideoSvc;
import vandy.mooc.utils.Constants;

/**
 *
 *
 */
public class LoginActivity extends Activity {
//        GenericActivity<MVP.LoginRequiredViewOps,
//                        MVP.LoginRequiredPresenterOps,
//                        LoginPresenter>
//         implements MVP.LoginRequiredViewOps {


    private Button mLoginButton;
    private EditText mUserName;
    private EditText mPassWord;
    private EditText mServer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        PreferenceManager.setDefaultValues
                (this, R.xml.preferences, false);

        /** Set the Content View to the Login Screen **/
        setContentView(R.layout.login_activity);
        initializeViews();
        //super.onCreate(LoginPresenter.class, this);
    }

    private void initializeViews() {
        /** Initialize variable to login Screen **/
        mUserName = (EditText) findViewById(R.id.username);
        mPassWord = (EditText) findViewById(R.id.password);
        mServer = (EditText)findViewById(R.id.server);



        mLoginButton = (Button) findViewById(R.id.loginButton);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loginClicked();
            }
        });
    }


    public void loginClicked() {

        String username_ = mUserName.getText().toString();
        String password_ = mPassWord.getText().toString();
        String server_ = mServer.getText().toString();
        String protocol_ = "HTTPS";
        String port_ = "8443";

        SharedPreferences spOptions;
        SharedPreferences.Editor spOptionEditor;

        spOptions = getSharedPreferences(Constants.PREF_KEY, 0);
        spOptionEditor = spOptions.edit();

        spOptionEditor.putString(SettingsActivity.KEY_PREFERENCE_USER_NAME, username_ );
        spOptionEditor.putString(SettingsActivity.KEY_PREFERENCE_PASSWORD, password_ );
        spOptionEditor.putString(SettingsActivity.KEY_PREFERENCE_IP_ADDRESS, server_ );
        spOptionEditor.putString(SettingsActivity.KEY_PREFERENCE_PORT, port_ );
        spOptionEditor.putString(SettingsActivity.KEY_PREFERENCE_PROTOCOL, protocol_ );

        spOptionEditor.commit();

        final VideoServiceProxy svc = VideoSvc.init(server_,
                                                    username_,
                                                    password_,
                                                    protocol_,
                                                    port_);

        CallableTask.invoke(new Callable<Collection<Video>>() {

            @Override
            public Collection<Video> call() throws Exception {
                return svc.getVideoList();
            }
        }, new TaskCallback<Collection<Video>>() {

            @Override
            public void success(Collection<Video> result) {
                // OAuth 2.0 grant was successful and web
                // can talk to the server, open up the video listing
                startActivity(new Intent(
                        LoginActivity.this,
                        VideoListActivity.class));
            }

            @Override
            public void error(Exception e) {
                Log.e(LoginActivity.class.getName(), "Error logging in via OAuth.", e);

                Toast.makeText(
                        LoginActivity.this,
                        "Login failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        // Call up to the superclass.
        super.onResume();
    }


}

