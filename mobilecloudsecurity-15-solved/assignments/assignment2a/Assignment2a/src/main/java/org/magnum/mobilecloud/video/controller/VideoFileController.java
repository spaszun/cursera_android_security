package org.magnum.mobilecloud.video.controller;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.repository.UserVideoRatingRepository;
import org.magnum.mobilecloud.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Q1SW on 14-08-2015.
 */
@Controller
public class VideoFileController {

    @Autowired
    VideoFileManager videoFileManager;

    @Autowired
    UserVideoRatingRepository userVideoRatingRepository;

    @Autowired
    VideoRepository videoRepository;

    @RequestMapping(value = "/video", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody() Iterable<Video> getVideos() {
        return videoRepository.findAll();
    }


    @RequestMapping(value = "/video/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody() Video getVideo(@PathVariable("id") long id) {
        return videoRepository.findOne(id);
    }

    @RequestMapping(value = "/video", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Video saveVideo(@RequestBody Video video, Principal principal) {
        video.setOwner(principal.getName());
        //If a Video already exists, it should not be overwritten unless the name of the authenticated Principal matches the name of the owner member variable of the Video
        final Video orginalVideo = videoRepository.findOne(video.getId());
        if (video.getId() > 0 && orginalVideo != null) {
            if (!orginalVideo.getOwner().equals(video.getOwner())) {
                return orginalVideo;
            }
        }
        videoRepository.save(video);
        return video;
    }

    @RequestMapping(value = "/video/{id}/data", method = RequestMethod.POST )
         public @ResponseBody ResponseEntity<VideoStatus> handleFileUpload(
            @PathVariable("id") long id,
            @RequestParam("data") MultipartFile file, Principal principal){
       final Video video = videoRepository.findOne(id);
        if (!file.isEmpty() && video != null) {
            try {
                if(!principal.getName().equals(video.getOwner())) {
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
                videoFileManager.saveVideoData(video, file.getInputStream());
                VideoStatus videoStatus = new VideoStatus(VideoStatus.VideoState.READY);
                return new ResponseEntity<>(videoStatus, HttpStatus.CREATED);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/video/{id}/data", method = RequestMethod.GET, produces = "video/mpeg")
    public @ResponseBody void videoDownload(
            @PathVariable("id") long id, HttpServletResponse response){
        final Video video = videoRepository.findOne(id);
        if (video != null && videoFileManager.hasVideoData(video)) {
            try {
                videoFileManager.copyVideoData(video, response.getOutputStream());
            } catch (IOException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }

        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

    }

    @RequestMapping(value = "/video/{id}/rating/{rating}", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody void saveRating(@PathVariable("id") long id,
                                         @PathVariable("rating") double rating,
                                         Principal principal,
                                         HttpServletResponse response) {
        String user = principal.getName();
        final Video video = videoRepository.findOne(id);
        if (video != null) {
            UserVideoRating userRating = userVideoRatingRepository.findOneByVideoIdAndUser(id, user);
            if (userRating == null) {
                userRating = new UserVideoRating(id, rating, user);
            } else {
                userRating.setRating(rating);
            }
            userVideoRatingRepository.save(userRating);
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/video/{id}/rating", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody() AverageVideoRating getVideos(@PathVariable("id") long id) {
        return new AverageVideoRating(
                userVideoRatingRepository.getAvgRating(id),
                id,
                userVideoRatingRepository.countByVideoId(id));
    }


}
