package vandy.mooc.model.mediator.provider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 */
public final class VideoContract {
    /**
     * This ContentProvider's unique identifier.
     */
    public static final String CONTENT_AUTHORITY =
            "vandy.mooc";

    /**
     * Use CONTENT_AUTHORITY to create the base of all URI's which
     * apps will use to contact the content provider.
     */
    public static final Uri BASE_CONTENT_URI =
            Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_VIDEO =
            VideoInfoEntry.TABLE_NAME;

    /*
     * Columns
     */

    /**
     * Inner class that defines the table contents of the Hobbit
     * table.
     */
    public static final class VideoInfoEntry implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_VIDEO)
                        .build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_VIDEO;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_VIDEO;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        VideoContract.VideoInfoEntry._ID,
                        VideoContract.VideoInfoEntry.COLUMN_VIDEO_ID,
                        VideoContract.VideoInfoEntry.COLUMN_TITLE,
                        VideoContract.VideoInfoEntry.COLUMN_URL,
                        VideoContract.VideoInfoEntry.COLUMN_DURATION,
                        VideoContract.VideoInfoEntry.COLUMN_LOCATION,
                        VideoContract.VideoInfoEntry.COLUMN_SUBJECT,
                        VideoContract.VideoInfoEntry.COLUMN_CONTENT_TYPE,
                        VideoContract.VideoInfoEntry.COLUMN_RATING
                };

        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "video_info";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_VIDEO_ID = "video_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_URL = "url";
        public static final String COLUMN_DURATION = "duration";
        public static final String COLUMN_LOCATION = "location";
        public static final String COLUMN_SUBJECT = "subject";
        public static final String COLUMN_CONTENT_TYPE = "content_type";
        public static final String COLUMN_RATING = "rating_count";


        /**
         * Return a Uri that points to the row containing a given id.
         *
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
