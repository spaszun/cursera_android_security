package org.magnum.mobilecloud.video.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Using Spring data jpa
 */
@Repository
public interface UserVideoRatingRepository  extends CrudRepository<UserVideoRating, Long> {

    // Find all user video rating with a matching videoId
    public Collection<UserVideoRating> findByVideoId(long videoId);

    // Find a user video rating with a matching videoId and user
    public UserVideoRating findByUserAndVideoId(String user, long videoId);
}
