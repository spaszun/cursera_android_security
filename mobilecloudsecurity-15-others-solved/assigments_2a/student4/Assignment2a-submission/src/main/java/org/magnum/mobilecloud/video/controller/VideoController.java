package org.magnum.mobilecloud.video.controller;

import com.google.common.collect.Lists;
import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Collection;

@Controller
public class VideoController {

    @Autowired
    VideoRepository videoRepo_;

    @Autowired
    UserVideoRatingRepository ratingRepo_;

    @Autowired
    VideoFileManager fileManager_;

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.GET)
    public @ResponseBody Collection<Video> getVideoList() {
        return Lists.newArrayList(videoRepo_.findAll());
    }


    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{" + VideoSvcApi.ID_PARAMETER + "}", method = RequestMethod.GET)
    public @ResponseBody Video getVideoById(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
                                            HttpServletResponse response) throws IOException {
        // Get the video
        Video video = videoRepo_.findOne(id);

        if (null == video) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        return video;
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.POST)
    public @ResponseBody Video addVideo(@RequestBody Video v, HttpServletResponse response, Principal p)
            throws IOException {
        // Query the database to check whether the video already exists, in order to update
        // Assuming that a video could be identified by it unique URL
        // An alternative way, would be by checking whether the video ID equals to 0
        // Then query the DB to find its owner. But since we don't know if there are
        // some other data that were not sent from te client (let's for now).
        Video storedVideo = videoRepo_.findByUrl(v.getUrl());

        if (storedVideo != null) {
            // Keep the ID in order to update the record
            v.setId(storedVideo.getId());

            // Check whether the video was uploaded by the current user
            if (!p.getName().equals(storedVideo.getOwner())) {
                // Send an error
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Unauthorized user : current user does not match" +
                        " the owner");
            }
        }

        //INFO TASK : may uncomment this code for the next assignment
        // Get video URL
        // String target = getVideoUrl(v);
        // v.setUrl(target);

        // Set the owner of the video
        v.setOwner(p.getName());

        // Create or overWrite the video in case metadata has been modified
        return videoRepo_.save(v);
    }

    //INFO other methods for further use
//    @RequestMapping(value = VideoSvcApi.VIDEO_TITLE_SEARCH_PATH, method = RequestMethod.GET)
//    public @ResponseBody Collection<Video> findByTitle(@RequestParam(VideoSvcApi.TITLE_PARAMETER) String title) {
//        return videoRepo_.findByTitle(title);
//    }

//    @RequestMapping(value = VideoSvcApi.VIDEO_TITLE_SEARCH_PATH, method = RequestMethod.GET)
//    public @ResponseBody Collection<Video> findByDurationLessThan(@RequestParam(VideoSvcApi.DURATION_PARAMETER) long
//                                                                          maxDuration) {
//        return videoRepo_.findByDurationLessThan(maxDuration);
//    }

    //INFO may change the type of the rating
    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating/{rating}", method = RequestMethod.POST)
    public @ResponseBody AverageVideoRating rateVideo(@PathVariable("id") long id, @PathVariable("rating") int
            rating, HttpServletResponse response, Principal p) {
        // Get the video
        Video v = videoRepo_.findOne(id);

        if (v != null) {
            // Get the current user
            String user = p.getName();

            // Check if the user has already rated the video
            UserVideoRating userVideoRating = ratingRepo_.findByUserAndVideoId(user, id);
            if (userVideoRating != null) {
                // Update the user rating
                userVideoRating.setRating(rating);
            } else {
                // Create the new user rating
                userVideoRating = new UserVideoRating(id, rating, p.getName());
            }

            // Save the user rating
            ratingRepo_.save(userVideoRating);

            // Calculate the new avg rating
            Collection<UserVideoRating> videoRatings = ratingRepo_.findByVideoId(id);
            double avgRating = getAvgRating(videoRatings);

            response.setStatus(HttpServletResponse.SC_OK);
            return new AverageVideoRating(avgRating, id, videoRatings.size());
        } else {
            //INFO Alternative to response.sendError()
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating", method = RequestMethod.GET)
    public @ResponseBody AverageVideoRating getVideoRating(@PathVariable("id") long id, HttpServletResponse response) {
        // Get the video
        Video v = videoRepo_.findOne(id);

        if (v != null) {
            // Calculate the new avg rating
            Collection<UserVideoRating> videoRatings = ratingRepo_.findByVideoId(id);
            double avgRating = getAvgRating(videoRatings);

            response.setStatus(HttpServletResponse.SC_OK);
            return new AverageVideoRating(avgRating, id, videoRatings.size());
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_DATA_PATH, method = RequestMethod.POST)
    @ResponseBody public VideoStatus setVideoData(
            @PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            @RequestParam(VideoSvcApi.DATA_PARAMETER) MultipartFile videoData,
            HttpServletResponse response, Principal p) throws IOException {

        // Check if the video ID is Valid
        VideoStatus status = null;
        Video video = videoRepo_.findOne(id);

        if (null == video) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid video ID");
        } else {
            //INFO is it possible that p.getName() may be null
            // Ensure that the name of the currently authenticated Principal matches the owner of the video
            if (!p.getName().equals(video.getOwner())) {
                // TODO (optional) find an alternative way
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Unauthorized user : current user does not match" +
                        " the owner");
            }
            fileManager_.saveVideoData(video, videoData.getInputStream());
            status = new VideoStatus(VideoStatus.VideoState.READY);
        }

        return status;
    }

    @RequestMapping(value = VideoSvcApi.VIDEO_DATA_PATH, method = RequestMethod.GET)
    public void getVideoData(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
                             HttpServletResponse response) throws IOException {

        // Check if the video ID is Valid
        Video video = videoRepo_.findOne(id);

        if (null == video) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid video ID");
        } else {
            boolean hasVideoData = fileManager_.hasVideoData(video);
            if (!hasVideoData) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "No mpeg data has been uploaded for the specified video");
            } else {
                // TODO (later) security issue: add control of content type (content flaw)
                response.setContentType(video.getContentType());
                fileManager_.copyVideoData(video, response.getOutputStream());
            }
        }
    }

    //TODO (later) find the best way to calculate the avg in a context of high traffic, use sharded counter
    private double getAvgRating(Collection<UserVideoRating> videoRatings) {
        double avgRating = 0;
        if (videoRatings != null && !videoRatings.isEmpty()) {
            double ratingSum = 0;
            for (UserVideoRating userRating : videoRatings) {
                ratingSum += userRating.getRating();
            }

            avgRating = ratingSum / videoRatings.size();
        }
        return avgRating;
    }

    //INFO : code for further use
//    private String getVideoUrl(Video video) {
//        String base = getUrlBaseForLocalServer();
//        return base + VideoSvcApi.VIDEO_DATA_PATH.replace("{" + VideoSvcApi.ID_PARAMETER + "}", "" + video.getId());
//    }
//
//    private String getUrlBaseForLocalServer() {
//        HttpServletRequest request =
//                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        return "https://" + request.getServerName()
//                + ((request.getServerPort() != 80) ? ":" + request.getServerPort() : "");
//    }

}
