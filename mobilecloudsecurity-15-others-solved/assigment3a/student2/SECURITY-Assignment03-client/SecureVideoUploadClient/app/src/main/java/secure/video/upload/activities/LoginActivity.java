package secure.video.upload.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Collection;

import secure.video.upload.R;
import secure.video.upload.model.mediator.webdata.Video;
import secure.video.upload.operations.LoginOps;
import secure.video.upload.utils.GenericActivity;
import secure.video.upload.utils.PersProgressDialog;
import secure.video.upload.utils.VideosCollection;


public class LoginActivity extends GenericActivity<LoginOps>
{
    private final String TAG = this.getClass().getSimpleName();

    private EditText mUser;
    private EditText mPassword;
    private CheckBox mShowPw;
    private Button mLoginBtn;

//    private PersProgressDialog mOpProgressDialog;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        setContentView (R.layout.activity_login);

        mOpProgressDialog = new PersProgressDialog (LoginActivity.this);

        mUser = (EditText) findViewById (R.id.loginUser);
        mPassword = (EditText) findViewById (R.id.loginPassword);
        mShowPw = (CheckBox) findViewById (R.id.loginShowPassword);

        mShowPw.setOnClickListener (new View.OnClickListener()
        {
            @Override
            public void onClick (View v)
            {
                checkPasswordFormat();
            }
        });

        mLoginBtn = (Button) findViewById (R.id.loginButton);

        super.onCreate (savedInstanceState, LoginOps.class);

    }

    private void checkPasswordFormat()
    {
        int start = mPassword.getSelectionStart();
        int end = mPassword.getSelectionEnd();
        if (mShowPw.isChecked()) {
            mPassword.setTransformationMethod (null);
        }
        else {
            mPassword.setTransformationMethod (new PasswordTransformationMethod());
        }
        mPassword.setSelection (start, end);
    }

    public void login (View view)
    {
        mLoginBtn.setEnabled (false);

        hideKeyboard (this, mUser.getWindowToken());
        hideKeyboard (this, mPassword.getWindowToken());

        String user = mUser.getText().toString();
        String pw = mPassword.getText().toString();
        if ((user == null) || (pw == null)) {
            Toast.makeText (this, getString(R.string.nullUserAndPwMsg), Toast.LENGTH_SHORT).show();
            return;
        }

        getOps().login (user, pw);
    }

    private void hideKeyboard (Activity activity, IBinder windowToken)
    {
        InputMethodManager mgr = (InputMethodManager) activity.getSystemService (
                Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow (windowToken, 0);
    }

//    public void notifyProgressUpdate (int progress, int dialogTitle, int dialogExpl)
//    {
//        mOpProgressDialog.updateProgressDialog (progress, dialogTitle, dialogExpl);
//    }

    public void notifySuccessfulLogin (Collection<Video> videos)
    {
        mLoginBtn.setEnabled (true);
        Intent intent = new Intent (LoginActivity.this, VideosInfoActivity.class);
//        intent.putExtra (VideosInfoActivity.VIDEOS_TAG, new VideosCollection (videos));
        startActivity(intent);
    }

    public void notifyFailedLogin (int error)
    {
        String toastMsg = getString (R.string.loginFailed) + ": " + getString (error);
        Toast.makeText (this, toastMsg, Toast.LENGTH_SHORT).show();
        mLoginBtn.setEnabled (true);
    }
}
