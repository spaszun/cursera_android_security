package secure.video.upload.utils;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collection;

import secure.video.upload.model.mediator.webdata.AverageVideoRating;
import secure.video.upload.model.mediator.webdata.Video;

public class VideosCollection implements Parcelable
{
    private Collection<Video> videos = new ArrayList<>();

    private VideosCollection (Parcel source)
    {
        final int N = source.readInt();
        for (int i=0; i<N; i++) {
            Video video = new Video();
            video.setTitle (source.readString());
            video.setUrl (source.readString());
            video.setDuration (source.readLong());
            video.setId (source.readLong());
            video.setOwner (source.readString());
            video.setLocation (source.readString());
            video.setSubject (source.readString());
            video.setContentType (source.readString());
            video.setAverageVideoRating (new AverageVideoRating (source.readDouble(), video.getId(),
                    source.readInt()));

            videos.add (video);
        }
    }

    public VideosCollection (Collection<Video> c)
    {
        videos = c;
    }

    public Collection<Video> get()
    {
        return videos;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel (Parcel dest, int flags)
    {
        final int n = videos.size();
        dest.writeInt (n);
        if (n < 0) {
            return;
        }

        for (Video video : videos) {
            dest.writeString (video.getTitle() == null ? "" : video.getTitle());
            dest.writeString (video.getUrl() == null ? "" : video.getUrl());
            dest.writeLong (video.getDuration());
            dest.writeLong (video.getId());
            dest.writeString (video.getOwner() == null ? "" : video.getOwner());
            dest.writeString (video.getLocation() == null ? "" : video.getLocation());
            dest.writeString (video.getSubject() == null ? "" : video.getSubject());
            dest.writeString (video.getContentType() == null ? "" : video.getContentType());
            if (video.getAverageVideoRating() != null) {
                dest.writeDouble (video.getAverageVideoRating().getRating());
                dest.writeInt (video.getAverageVideoRating().getTotalRatings());
            }
            else {
                dest.writeDouble (0);
                dest.writeInt (0);
            }
        }
    }

    public static final Creator<VideosCollection> CREATOR = new Creator<VideosCollection>()
    {
        public VideosCollection createFromParcel (Parcel source)
        {
            return new VideosCollection (source);
        }
        public VideosCollection[] newArray (int size)
        {
            return new VideosCollection[size];
        }
    };
}
