package vandy.mooc.model.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import vandy.mooc.model.mediator.VideoDataMediator;

/**
 * This superclass factors out code that's common to the
 * DownloadVideoService and UploadVideoService.
 */
public abstract class VideoServiceCommon
        extends IntentService {

    /**
     * It is used by Notification Manager to send Notifications.
     */
    protected static final int NOTIFICATION_ID = 1;

    /**
     * VideoDataMediator mediates the communication between Video
     * Service and local storage in the Android device.
     */
    protected VideoDataMediator mVideoDataMediator;

    /**
     * Manages the Notification displayed is System UI.
     */
    protected NotificationManager mNotifyManager;

    protected VideoServiceCommon(String serviceName) {
        // Initialize the superclass.
        super(serviceName);
    }

    /**
     * Called by the system when the Service is first created.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        // Create VideoDataMediator that wil mediate the
        // communication between Server and VideoContent Provider.
        mVideoDataMediator =
                new VideoDataMediator(getApplicationContext());

        // Gets the access to System Notification Service.
        mNotifyManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    /**
     * Factory method that makes /returns a Notification from the @a
     * resId.
     */
    public Notification makeNotification(int resId,
                                         String status) {
        // Builder used to build the Notification.
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this);

        // Update notification.
        builder.setContentTitle(status)
                // Removes the progress bar.
                .setProgress(0,
                        0,
                        false)
                .setSmallIcon(resId)
                .setContentText("")
                .setTicker(status);
        return builder.build();
    }

    /**
     * Factory method that makes /returns a Notification fro the @a
     * resId.
     */
    public Notification makeNotification(int resId,
                                         String direction,
                                         String videoTitle,
                                         PendingIntent pendingIntent) {
        // Builder used to build the Notification.
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this);

        // Build the Notification and sets an activity indicator for
        // an operation of indeterminate length.
        builder.setContentTitle("Video "
                + direction)
                .setContentText(direction
                        + " in progress")
                .setContentIntent(pendingIntent)
                .setSmallIcon(resId)
                .setContentText("")
                .setTicker(direction
                        + "ing video "
                        + videoTitle)
                .setProgress(0,
                        0,
                        true);
        return builder.build();
    }
}
