package vandy.mooc.model.services;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import vandy.mooc.view.VideoListActivity;


public class DownloadVideoService extends VideoServiceCommon {

    public static final String ACTION_DOWNLOAD_SERVICE_RESPONSE =
            "vandy.mooc.services.DownloadService.RESPONSE";

    public static final int RESULT_OK = 200;

    public static final int RESULT_ERROR = 404;

    public static final String KEY_FILE_PATH = "file_path";

    public static final String KEY_STATUS = "status";

    public static final String KEY_DOWNLOAD_VIDEO_TITLE =
            "download_title";

    public static final String KEY_DOWNLOAD_VIDEO_ID =
            "download_videoId";

    public static final long DEFAULT_VIDEO_ID = 0L;

    private long mVideoId;

    private String mVideoTitle;

    public DownloadVideoService() {
        super("DownloadVideoService");
    }

    public static Intent makeIntent(Context context,
                                    long videoId,
                                    String videoTitle) {
        return new Intent(context,
                DownloadVideoService.class)
                .putExtra(KEY_DOWNLOAD_VIDEO_ID, videoId)
                .putExtra(KEY_DOWNLOAD_VIDEO_TITLE, videoTitle);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        mVideoId =
                intent.getLongExtra(KEY_DOWNLOAD_VIDEO_ID,
                        DEFAULT_VIDEO_ID);
        mVideoTitle =
                intent.getStringExtra(KEY_DOWNLOAD_VIDEO_TITLE);

        startNotification();

        String filePath =
                mVideoMediator.downloadVideo(mVideoId, mVideoTitle);

        if (filePath != null) {
            finishNotification("Download complete");
            sendBroadcast(RESULT_OK, filePath);
        } else {
            finishNotification("Download failed");
            sendBroadcast(RESULT_ERROR, null);
        }
    }

    private void sendBroadcast(int status, String filePath) {
        Intent intentResponse =
                new Intent(ACTION_DOWNLOAD_SERVICE_RESPONSE)
                .addCategory(Intent.CATEGORY_DEFAULT)
                .putExtra(KEY_STATUS, status)
                .putExtra(KEY_DOWNLOAD_VIDEO_ID, mVideoId);
        if (status == RESULT_OK)
            intentResponse.putExtra(KEY_FILE_PATH, filePath);

        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(intentResponse);
    }

    private void startNotification() {
        final PendingIntent pendingIntent =
                PendingIntent.getActivity
                                (this,
                                0,
                                VideoListActivity.makeIntent(getApplicationContext()),
                                PendingIntent.FLAG_UPDATE_CURRENT);

        mNotifyManager.notify
                (NOTIFICATION_ID,
                        makeNotification(android.R.drawable.stat_sys_download,
                                        "Download,",
    mVideoTitle,
    pendingIntent));
}

    private void finishNotification(String status) {
        mNotifyManager.notify
                (NOTIFICATION_ID,
                        makeNotification(android.R.drawable.stat_sys_download_done,
                                status));
    }
}
