package vandy.mooc.common;

public interface TaskCallback<T> {

    void success(T result);

    void error(Exception e);

}