/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.magnum.mobilecloud.video;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.controller.VideoFileManager;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.magnum.mobilecloud.video.model.VideoStatus.VideoState;
import org.magnum.mobilecloud.video.repository.UserVideoRatingRepository;
import org.magnum.mobilecloud.video.repository.VideoRepository;

@Controller
public class VideoController {

	public static final String RATING_PARAMETER = "rating";
	
	public static final String VIDEO_RATING_PATH = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating";
	
	@Autowired
	private VideoRepository videos;
	
	@Autowired
	private UserVideoRatingRepository videoRatings;
	
	private static final AtomicLong currentId = new AtomicLong(0L);
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList(){
		return  Lists.newArrayList(videos.findAll());
	}
	
	@RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.POST)
    public @ResponseBody Video addVideo(@RequestBody Video v, Principal principal){
		
		if (v.getId() == 0) {
			v.setLocation(getDataUrl(currentId.get()));
			v.setOwner(principal.getName());
		}
		videos.save(v);
        return v;
    }
	
	@RequestMapping (value=VideoSvcApi.VIDEO_DATA_PATH, method=RequestMethod.POST)
	public @ResponseBody VideoStatus setVideoData(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
			@RequestPart(VideoSvcApi.DATA_PARAMETER) MultipartFile videoData, 
			HttpServletResponse response, Principal principal) throws IOException {
		
		VideoStatus videoStatus = new VideoStatus(VideoState.PROCESSING);
		Video video = videos.findOne(id);
		VideoFileManager manager = new VideoFileManager();
		if(video != null){
			if (video.getOwner().equalsIgnoreCase(principal.getName())) {
				manager.saveVideoData(video, videoData.getInputStream());
				videoStatus.setState(VideoState.READY);
				response.setStatus(200);
			}
			else {
				response.setStatus(403);
			}
		}else{
			response.setStatus(404);
		}
		
		return videoStatus;
	}
	
	@RequestMapping (value=VideoSvcApi.VIDEO_DATA_PATH, method=RequestMethod.GET)
	public void getVideoData(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            HttpServletResponse response)throws IOException{
		
		OutputStream out = response.getOutputStream();
		Video video = videos.findOne(id);
		VideoFileManager manager = new VideoFileManager();
		if(video != null && manager.hasVideoData(video)){
			manager.copyVideoData(video, out);
			response.setStatus(200);
		}else{
			response.setStatus(404);
		}
		
	}
	
	@RequestMapping (value=VIDEO_RATING_PATH+"/{rating}", method=RequestMethod.POST)
	public @ResponseBody AverageVideoRating setVideoRate(@PathVariable(VideoSvcApi.ID_PARAMETER) long id, 
			@PathVariable(RATING_PARAMETER) int rating,
            HttpServletResponse response, Principal principal) throws IOException{
		
		Video video = videos.findOne(id);
		
		if(video != null){
			UserVideoRating userVideoRating = videoRatings.findByVideoIdAndUser(id, principal.getName());
			if (userVideoRating != null) {
				userVideoRating.setRating(rating);
				videoRatings.save(userVideoRating);
			}
			else {
				videoRatings.save(new UserVideoRating(id,rating,principal.getName()));
			}
			response.setStatus(200);
			return findAverageRating(id);
		}else{
			response.setStatus(404);
			return new AverageVideoRating(0,id,0);
		}
	
	}
	
	@RequestMapping (value=VIDEO_RATING_PATH, method=RequestMethod.GET)
	public @ResponseBody AverageVideoRating getAverageVideoRate(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
            HttpServletResponse response)throws IOException{
		
		if(videoRatings.findAllByVideoId(id) != null){
			response.setStatus(200);
			return findAverageRating(id);
		}else{
			response.setStatus(404);
			return null;
		}
		
	}

	// Utils
	
    private String getUrlBaseForLocalServer() {
        HttpServletRequest request = 
            ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String base = "http://"
            + request.getServerName() 
            + ((request.getServerPort() != 80) ? ":"+request.getServerPort() : "");
        return base;
     }
	
	private String getDataUrl(long id){
        String url = getUrlBaseForLocalServer() + "/video/" + id + "/data";
        return url;
    }
	
    private AverageVideoRating findAverageRating(Long id) {
		List<UserVideoRating> userRatings = Lists.newArrayList(videoRatings.findAllByVideoId(id));
    	double totalRating = 0;
    	for (UserVideoRating userVideoRating : userRatings) {
    		totalRating += userVideoRating.getRating();
    	}
		return new AverageVideoRating(totalRating/userRatings.size(),id,userRatings.size());
	}

	
}
