package vandy.mooc.view.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import vandy.mooc.R;
import vandy.mooc.model.mediator.provider.VideoContract;

/**
 * Show the view for each Video's meta-data in a ListView.
 */
public class VideoAdapter
        extends CursorAdapter {
    /**
     * Allows access to application-specific resources and classes.
     */
    private final Context mContext;

    /**
     *
     */
    private LayoutInflater mLayoutInflater;

    /**
     * Constructor that stores the Application Context.
     *
     * @param context
     * @param cursor
     * @param flag
     */
    public VideoAdapter(Context context,
                        Cursor cursor,
                        int flag) {
        super(context, cursor, flag);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    /**
     * @param context
     * @param cursor
     * @param parent
     * @return View
     */
    @Override
    public View newView(Context context,
                        Cursor cursor,
                        ViewGroup parent) {
        View v = mLayoutInflater.inflate(R.layout.video_list_item,
                parent,
                false);
        return v;
    }

    /**
     * @param view
     * @param context
     * @param cursor
     */
    @Override
    public void bindView(View view,
                         Context context,
                         Cursor cursor) {
        String title =
                cursor.getString(cursor.getColumnIndexOrThrow
                        (VideoContract.VideoInfoEntry.COLUMN_TITLE));

        TextView titleText =
                (TextView) view.findViewById(R.id.tvVideoTitle);
        titleText.setText(title);
    }
}
