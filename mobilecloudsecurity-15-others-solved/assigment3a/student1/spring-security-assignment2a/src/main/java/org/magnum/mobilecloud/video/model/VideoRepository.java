package org.magnum.mobilecloud.video.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * An interface for a repository that can store Video objects and allow them to
 * be searched by title.
 */
@Repository
public interface VideoRepository extends CrudRepository<Video, Long> {

    // When we add a video, we return that video

    // Find all videos with a matching title (e.g., Video.name)
    public Collection<Video> findByTitle(String title);

    // Find all videos that are shorter that a specified duration
    public Collection<Video> findByDurationLessThan(Long maxduration);

}