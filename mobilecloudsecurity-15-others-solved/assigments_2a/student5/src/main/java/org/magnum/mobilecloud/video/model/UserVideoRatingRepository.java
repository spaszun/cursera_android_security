package org.magnum.mobilecloud.video.model;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  UserVideoRatingRepository extends CrudRepository<UserVideoRating, Long> {
	 
	
	// Query for calculating Average rating + Count of ratings for given video
	
	@Query(value = "SELECT NEW org.magnum.mobilecloud.video.model.AverageVideoRating(AVG(rating) as rating, videoId, COUNT(*) as totalRatings) FROM UserVideoRating  WHERE videoId = ?1 GROUP BY videoId")
	public AverageVideoRating findAvgRatingByVideoId(long videoId);
		
	// Find (unique) UserVideoRating of given videoId for given user
	
	public UserVideoRating findByVideoIdAndUser(long videoId, String user);
}
