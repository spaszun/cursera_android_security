package vandy.mooc.presenter;

import android.util.Log;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import vandy.mooc.common.ConfigurableOps;
import vandy.mooc.common.ContextView;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;
import vandy.mooc.model.mediator.VideoDataMediator;
import vandy.mooc.model.mediator.webdata.AverageVideoRating;
import vandy.mooc.model.mediator.webdata.VideoRating;

/**
 * Created for dealing with video ratings as I didnt get it how to put mutliple asyncs into one class
 * <p>
 * by kaido on 3.09.2015.
 */
public class VideoRatingOps
        implements GenericAsyncTaskOps<VideoRating, Void, AverageVideoRating>,
        ConfigurableOps<VideoRatingOps.View> {

    private static final String TAG = VideoRatingOps.class.getSimpleName();


    @Override
    public void onConfiguration(VideoRatingOps.View view, boolean firstTimeIn) {
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d(TAG, "onConfiguration() called the " + time + " with view = " + view);

        mVideoView = new WeakReference<>(view);

        if (firstTimeIn) {
            // Create VideoDataMediator that will mediate the
            // communication between Server and Android Storage.
            mVideoMediator = new VideoDataMediator();
        }

    }

    @Override
    public AverageVideoRating doInBackground(VideoRating... params) {
        Log.d(TAG, "Rating asynctask fired...");
        return mVideoMediator.rateVideo(params[0].getVideoId(), params[0].getRating());
    }

    @Override
    public void onPostExecute(final AverageVideoRating videoRating) {
        //  TODO update model
        //  TODO notify observers

        //  instead shortcut to see it on screen
        Toast.makeText(mVideoView.get().getActivityContext(),
                "New average rating: " + videoRating.getRating(),
                Toast.LENGTH_LONG).show();


    }

    public interface View extends ContextView {

    }

    private WeakReference<View> mVideoView;

    private GenericAsyncTask<VideoRating,
            Void,
            AverageVideoRating,
            VideoRatingOps> mAsyncTaskRateVideo;

    VideoDataMediator mVideoMediator;

    public void rateVideo(final long id, final double rating) {


        mAsyncTaskRateVideo = new GenericAsyncTask<>(this);
        mAsyncTaskRateVideo.execute(new VideoRating(id, rating));

    }

}
