package vandy.mooc.model.mediator.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import vandy.mooc.R;

/**
 * Defines table and column names for the Video database.
 */
public class VideoContract {
    /**
     * This "Content authority" is a name for the entire content
     * provider, similar to the relationship between a domain name and
     * its website.  A convenient string to use for the content
     * authority is the package name for the app, which is guaranteed
     * to the unique on the device.
     */
    public static final String CONTENT_AUTHORITY =
            "vandy.mooc";

    /**
     * Use CONTENT_AUTHORITY to create the base of all URI's which
     * apps will use to contact the content provider.
     */
    public static final Uri BASE_CONTENT_URI =
            Uri.parse("content://"
                    + CONTENT_AUTHORITY);

    /**
     * Possible paths (appended to base content URI for possible
     * URI's) for instance, content://vandy.mooc/video_info/ is a
     * valid path for looking at video_info.
     * data. content://vandy.mooc/givemeroot/ will fail, as the
     * ContentProvider hasn't been given any information on what to do
     * with "givemeroot".
     */
    public static final String PATH_VIDEO =
            VideoInfoEntry.TABLE_NAME;

    /**
     * Inner class that defines the table contents of the video table.
     */
    public static final class VideoInfoEntry
            implements BaseColumns {
        /**
         * Content Uri to access the Video Table.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_VIDEO)
                        .build();

        /**
         * Content Type for more than one item in Video Table.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE
                        + "/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_VIDEO;

        /**
         * Content Type for one item of the Video Table.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE
                        + "/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_VIDEO;

        /**
         * Table name.
         */
        public static final String TABLE_NAME = "video_info";

        /**
         * Columns for the Video_Info Table.
         */
        public static final String COLUMN_VIDEO_ID = "Video_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DURATION = "duration";
        public static final String COLUMN_CONTENT_TYPE = "content_type";
        public static final String COLUMN_DATA_URL = "data_url";
        public static final String COLUMN_RATING_COUNT = "rating_count";
        public static final String COLUMN_USER_COUNT = "user_count";

        /**
         * Build ContentUri for the given videoId.
         *
         * @param videoId
         * @return Uri with the given VideoId.
         */
        public static Uri buildUri(long videoId) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    videoId);
        }
    }
}
