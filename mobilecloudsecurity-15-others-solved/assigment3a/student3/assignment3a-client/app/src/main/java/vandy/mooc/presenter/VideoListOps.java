package vandy.mooc.presenter;

import android.net.Uri;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.List;

import vandy.mooc.common.ConfigurableOps;
import vandy.mooc.common.ContextView;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;
import vandy.mooc.common.Utils;
import vandy.mooc.model.mediator.VideoDataMediator;
import vandy.mooc.model.mediator.webdata.Video;
import vandy.mooc.model.services.DownloadVideoService;
import vandy.mooc.model.services.UploadVideoService;
import vandy.mooc.view.ui.VideoAdapter;

/**
 * Provides all the Video-related operations.
 * <p>
 * It implements ConfigurableOps so it can be created/managed by the GenericActivity framework.
 * <p>
 * It extends GenericAsyncTaskOps so its doInBackground() method runs in a background task.
 * It plays the role of the "Abstraction" in Bridge pattern
 * and the role of the "Presenter" in the Model-View-Presenter pattern.
 */
public class VideoListOps
        implements GenericAsyncTaskOps<Void, Void, List<Video>>,
        ConfigurableOps<VideoListOps.View> {
    /**
     * Debugging tag used by the Android logger.
     */
    private static final String TAG = VideoListOps.class.getSimpleName();

    /**
     * This interface defines the minimum interface needed by the
     * VideoListOps class in the "Presenter" layer to interact with the
     * VideoListActivity in the "View" layer.
     */
    public interface View extends ContextView {
        /**
         * Finishes the Activity the VideoListOps is associated with.
         */
        void finish();

        /**
         * Sets the Adapter that contains List of Videos.
         */
        void setAdapter(VideoAdapter videoAdapter);

        /**
         * Sets the OnItemClickListener.
         */
        void setOnItemClickListener(VideoAdapter videoAdapter);
    }

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<VideoListOps.View> mVideoListView;

    /**
     * The GenericAsyncTask used to get a list of Videos in a background
     * thread via the Video web service.
     */
    private GenericAsyncTask<Void,  //  params
            Void,                   //  progress
            List<Video>,            //  result
            VideoListOps> mAsyncTaskGetList;

    /**
     * VideoDataMediator mediates the communication between Video
     * Service and local storage on the Android device.
     */
    VideoDataMediator mVideoMediator;

    /**
     * The Adapter that is needed by ListView to show the list of Videos.
     */
    private VideoAdapter mAdapter;

    /**
     * Default constructor that's needed by the GenericActivity framework.
     */
    public VideoListOps() {
    }

    /**
     * Called after a runtime configuration change occurs to finish
     * the initialisation steps.
     */
    public void onConfiguration(VideoListOps.View view, boolean firstTimeIn) {
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d(TAG, "onConfiguration() called the " + time + " with view = " + view);

        // (Re)set the mVideoListView WeakReference.
        mVideoListView = new WeakReference<>(view);

        if (firstTimeIn) {
            // Create VideoDataMediator that will mediate the
            // communication between Server and Android Storage.
            mVideoMediator = new VideoDataMediator();

            // Create a local instance of our custom Adapter for our ListView.
            mAdapter = new VideoAdapter(mVideoListView.get().getApplicationContext());

            // Get the VideoList from Server. 
            getVideoList();
        }

        // Set the adapter to the ListView.
        mVideoListView.get().setAdapter(mAdapter);
        // Set the OnItemClickListener to the ListView.
        mVideoListView.get().setOnItemClickListener(mAdapter);
    }

    /**
     * Start a service that Uploads the Video having given Id.
     *
     * @param videoUri Uri used to compose intent
     */
    public void uploadVideo(Uri videoUri) {
        // Sends an Intent command to the UploadVideoService.
        mVideoListView.get().getApplicationContext().startService
                (UploadVideoService.makeIntent(mVideoListView.get().getApplicationContext(), videoUri));
    }


    /**
     * Start a service that Downloads the Video having given uri encoded id.
     *
     * @param videoId    video id
     * @param videoTitle title is used as filename for video storage
     */
    public void downloadVideo(long videoId, String videoTitle) {
        // Sends an Intent command to the DownloadVideoService.
        Log.d(TAG, "mVideoListView: " + mVideoListView);
        mVideoListView.get().getApplicationContext().startService
                (DownloadVideoService.makeIntent(mVideoListView.get().getApplicationContext(),
                        videoId,
                        videoTitle));
    }


    /**
     * Gets the VideoList from Server by executing the AsyncTask not blocking the caller.
     */
    public void getVideoList() {
        mAsyncTaskGetList = new GenericAsyncTask<>(this);
        mAsyncTaskGetList.execute();
    }

    /**
     * Retrieve the List of Videos by help of VideoDataMediator via a
     * synchronous two-way method call, which runs in a background
     * thread to avoid blocking the UI thread.
     */

    @Override
    public List<Video> doInBackground(Void... params) {
        return mVideoMediator.getVideoList();
    }

    /**
     * Display the results in the UI Thread.
     */
    @Override
    public void onPostExecute(List<Video> videos) {
        if (null != videos)

            displayVideoList(videos);
        else
            Log.d(TAG, "getVideoList onPostExecute received null object");
    }

    /**
     * Display the Videos in ListView.
     *
     * @param videos
     */
    public void displayVideoList(List<Video> videos) {
        if (videos != null) {
            // Update the adapter with the List of Videos.
            mAdapter.setVideos(videos);

            Utils.showToast(mVideoListView.get().getActivityContext(),
                    "Videos available from the Video Service");
        } else {
            Utils.showToast(mVideoListView.get().getActivityContext(),
                    "Please connect to the Video Service");

            // Close down the Activity.
            mVideoListView.get().finish();
        }
    }

}
