package secure.video.upload.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

public class PersProgressDialog extends ProgressDialog
{
    private static final String TAG = PersProgressDialog.class.getSimpleName();

    private int mOpProgress;
    public static final int OPERATION_COMPLETED = 100;
    private Context mContext;

    public PersProgressDialog(Context context)
    {
        super (context);
        mContext = context;
    }

    public void updateProgressDialog (int progress, int dialogTitle, int dialogExpl)
    {
        mOpProgress = progress;
        Log.i (TAG, "Progress " + progress);
        if (mOpProgress == OPERATION_COMPLETED) {
            dismiss();
            return;
        }

        setProgress (mOpProgress);
        setTitle (dialogTitle);
        setMessage (mContext.getString (dialogExpl));
        show();
    }
}
