package com.sps.videoserviceapp;

/**
 * Created by Q1SW on 28-08-2015.
 */
import android.app.Activity;
        import android.content.Intent;
        import android.net.Uri;
        import android.os.Bundle;
        import java.io.File;

public class PickVideoActivity extends Activity implements
        VideosFragment.Contract {

    public static final String VIDEO_URI = "VIDEO_URI";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pick_video_activity);
    }

    @Override
    public void onVideoSelected(String uri, String mimeType) {
        Uri video=Uri.fromFile(new File(uri));
        Intent i= new Intent();
        i.putExtra(VIDEO_URI, video);
        setResult(RESULT_OK, i);
        finish();
    }
}
