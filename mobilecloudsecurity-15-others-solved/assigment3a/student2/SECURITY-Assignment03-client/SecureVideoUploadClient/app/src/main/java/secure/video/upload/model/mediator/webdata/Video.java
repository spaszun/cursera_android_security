package secure.video.upload.model.mediator.webdata;

import java.util.Set;

public class Video
{
    private long id;

    private String title;
    private String url;
    private long duration;
    private String location;
    private String subject;
    private String contentType;
    private AverageVideoRating averageVideoRating;

    // We don't want to bother unmarshalling or marshalling
    // any owner data in the JSON. Why? We definitely don't
    // want the client trying to tell us who the owner is.
    // We also might want to keep the owner secret.
    private String owner;

    public Video() {
    }

    public Video (String owner, String name, String url, long duration, long likes, Set<String> likedBy)
    {
        super();
        this.owner = owner;
        this.title = name;
        this.url = url;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setAverageVideoRating (AverageVideoRating rating)
    {
        averageVideoRating = rating;
    }

    public AverageVideoRating getAverageVideoRating()
    {
        return averageVideoRating;
    }

}
