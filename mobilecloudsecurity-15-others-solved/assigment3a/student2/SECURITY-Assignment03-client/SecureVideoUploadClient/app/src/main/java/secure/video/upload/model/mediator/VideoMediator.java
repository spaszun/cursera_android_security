package secure.video.upload.model.mediator;

import android.util.Log;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import secure.video.upload.model.mediator.webdata.SecuredRestBuilder;
import secure.video.upload.model.mediator.webdata.SecuredRestException;
import secure.video.upload.model.mediator.webdata.UnsafeHttpsClient;
import secure.video.upload.model.mediator.webdata.VideoSvcApi;
import secure.video.upload.utils.Constants;

public class VideoMediator
{
    private static final String TAG = VideoMediator.class.getSimpleName();

    private static VideoSvcApi mVideoServiceProxy;

    private static boolean isConnected = false;

    public static void connect (String user, String pw)
    {
        try {
            mVideoServiceProxy = new SecuredRestBuilder()
                    .setLoginEndpoint (Constants.SERVER_URL + VideoSvcApi.TOKEN_PATH)
                    .setEndpoint (Constants.SERVER_URL)
                    .setUsername (user)
                    .setPassword (pw)
                    .setClientId (Constants.CLIENT_ID)
                    .setClient (new OkClient (UnsafeHttpsClient.getUnsafeOkHttpClient()))
                    .setLogLevel (RestAdapter.LogLevel.FULL)
                    .build()
                    .create (VideoSvcApi.class);
        }
        catch (SecuredRestException e) {
            Log.e (TAG, "Problem in connecting to the server", e);
            isConnected = false;
            throw new SecuredRestException (e);
        }

        isConnected = true;
    }

    public static VideoSvcApi get()
    {
        if (!isConnected) {
            throw new SecuredRestException ("The proxy is not connected");
        }
        return mVideoServiceProxy;
    }

}
