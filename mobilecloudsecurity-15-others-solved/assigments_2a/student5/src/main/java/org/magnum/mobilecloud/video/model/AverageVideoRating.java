package org.magnum.mobilecloud.video.model;

import com.google.common.base.Objects;


public class AverageVideoRating {

	private final double rating;

	private final long videoId;

	private final int totalRatings;


	public AverageVideoRating(double rating, long videoId, int totalRatings) {
		super();
		this.rating = rating;
		this.videoId = videoId;
		this.totalRatings = totalRatings;
	}
	
	// Other constructor to handle query result (where totalRatings can be long)
	
	public AverageVideoRating(double rating, long videoId, long totalRatings) {
		this(rating, videoId, (int) totalRatings);
	}


	public double getRating() {
		return rating;
	}

	public long getVideoId() {
		return videoId;
	}

	public int getTotalRatings() {
		return totalRatings;
	}
	
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(rating, videoId, totalRatings);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Video) {
			AverageVideoRating other = (AverageVideoRating) obj;
			// Google Guava provides great utilities for equals too!
			return totalRatings == other.totalRatings
					&& videoId == other.videoId
					&& rating == other.rating;
		} else {
			return false;
		}
	}
	
	public String toString() {
		return String.format("AverageVideoRating(%d,%f,%d)", videoId, rating, totalRatings); 
	}

}
