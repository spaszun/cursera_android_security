package org.magnum.mobilecloud.video.model;

public class VideoLocation {
    
    private String location;

    public VideoLocation() {
    }

    public VideoLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String toString() {
        return "Location:" + location;
    }
}
