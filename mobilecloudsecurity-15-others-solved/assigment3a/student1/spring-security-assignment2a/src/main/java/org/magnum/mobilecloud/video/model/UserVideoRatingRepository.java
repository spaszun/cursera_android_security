package org.magnum.mobilecloud.video.model;

import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

/**
 * An interface for a repository that can store UserVideoRating objects and
 * allow them to be searched by user and video id.
 */
public interface UserVideoRatingRepository extends CrudRepository<UserVideoRating, Long> {

    // Find all videos with a user and video id
    public Collection<UserVideoRating> findByUserAndVideoId(String user, long videoId);

    // Find all videos ratings with a matching video id
    public Collection<UserVideoRating> findByVideoId(long videoId);

}
