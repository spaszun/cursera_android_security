package secure.video.upload.operations;

import android.app.Activity;
import android.content.ContentValues;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.Collection;

import secure.video.upload.R;
import secure.video.upload.activities.VideosInfoActivity;
import secure.video.upload.model.mediator.VideoMediator;
import secure.video.upload.model.mediator.webdata.AverageVideoRating;
import secure.video.upload.model.mediator.webdata.Video;
import secure.video.upload.model.mediator.webdata.VideoSvcApi;
import secure.video.upload.provider.VideoContract;
import secure.video.upload.utils.ConfigurableOps;
import secure.video.upload.utils.GenericAsyncTask;
import secure.video.upload.utils.GenericAsyncTaskOps;
import secure.video.upload.utils.PersProgressDialog;

public class DownloadVideosInfoOps implements ConfigurableOps, GenericAsyncTaskOps<Void, Void, Collection<Video>>
{
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * Retrofit api
     */
    private VideoSvcApi mVideoSvcApi;

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<VideosInfoActivity> mActivity;

    /**
     * List of results to display (if any).
     */
    private Collection<Video> mResults;

    /**
     * The GenericAsyncTask used to display the video available on the server in a background
     * thread via the video web service.
     */
    private GenericAsyncTask<Void, Void, Collection<Video>, DownloadVideosInfoOps> mAsyncTask;

    /**
     * Default constructor that's needed by the GenericActivity framework.
     */
    public DownloadVideosInfoOps()
    {
    }

    public void onConfiguration (Activity activity, boolean firstTimeIn)
    {
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d (TAG, "onConfiguration() called the " + time + " with activity = " + activity);

        // (Re)set the mActivity WeakReference.
        mActivity = new WeakReference<>((VideosInfoActivity) activity);

        if (firstTimeIn) {
            try {
                mVideoSvcApi = VideoMediator.get();
            }
            catch (SecurityException e) {
                mActivity.get().notifyMediatorNotConnected();
                return;
            }

            if (mAsyncTask != null) {
                mAsyncTask.cancel (true);
            }
            mAsyncTask = new GenericAsyncTask<> (this);
            mAsyncTask.execute();
        }
        else {
            // Update the results on the UI.
            if (mResults != null) {
//                mActivity.get().displayResults (mResults, null);
            }
        }
    }

    @Override
    public void publishProgress (int progress)
    {
//        mActivity.get().notifyProgressUpdate (progress, R.string.downloadVideoInfoDialogTitle,
//                R.string.downloadVideoInfoDialogExpl);
    }

    @Override
    public Collection<Video> doInBackground (Void param)
    {
        Collection<Video> videos = mVideoSvcApi.getVideoList();

        // Deleting all the rows in the db
        int rows = mActivity.get().getContentResolver().delete (VideoContract.VideoEntry.CONTENT_URI,
                null, null);
        Log.d (TAG, "Deleted " + rows + " rows from the db");

        ContentValues[] cvs = new ContentValues[videos.size()];
        int i=0;
        for (Video video : videos) {
            AverageVideoRating videoRating = mVideoSvcApi.getVideoRating (video.getId());
            Log.d (TAG, "Rating for " + video.getId() + ": " + videoRating.getRating() + " for " + videoRating.getTotalRatings());
            video.setAverageVideoRating (videoRating);

            ContentValues cv = new ContentValues();
            cv.put (VideoContract.VideoEntry.COLUMN_ID, video.getId());
            cv.put (VideoContract.VideoEntry.COLUMN_TITLE, video.getTitle());
            cv.put (VideoContract.VideoEntry.COLUMN_DURATION, video.getDuration());
            String contentType = video.getContentType();
            cv.put (VideoContract.VideoEntry.COLUMN_CONTENT_TYPE, (contentType == null ? "" : contentType));
            String dataUrl = video.getUrl();
            cv.put (VideoContract.VideoEntry.COLUMN_DATA_URL, (dataUrl == null ? "" : dataUrl));
            if (video.getAverageVideoRating() != null) {
                cv.put (VideoContract.VideoEntry.COLUMN_STAR_RATING, video.getAverageVideoRating().getRating());
                cv.put (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS, video.getAverageVideoRating().getTotalRatings());
            }
            else {
                cv.put (VideoContract.VideoEntry.COLUMN_STAR_RATING, 0);
                cv.put (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS, 0);
            }
            cv.put (VideoContract.VideoEntry.COLUMN_DATA_LOCAL_URI, "");
            cvs[i] = cv;
            i++;
        }
        rows = mActivity.get().getContentResolver().bulkInsert (VideoContract.VideoEntry.CONTENT_URI, cvs);
        Log.d (TAG, "Added " + rows + " rows to the db");

        return videos;
    }

    @Override
    public void onPostExecute (Collection<Video> videos, Void param)
    {
        // Store the video in anticipation of runtime configuration changes
        mResults = videos;
        publishProgress (PersProgressDialog.OPERATION_COMPLETED);
//        mActivity.get().displayResults (videos, "No videos found");
        mAsyncTask = null;
    }
}
