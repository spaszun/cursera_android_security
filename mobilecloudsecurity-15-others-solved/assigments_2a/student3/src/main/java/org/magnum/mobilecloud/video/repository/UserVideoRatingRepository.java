package org.magnum.mobilecloud.video.repository;

import java.util.Collection;

import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.springframework.data.repository.CrudRepository;

public interface UserVideoRatingRepository extends CrudRepository<UserVideoRating, Long> {
	Collection<UserVideoRating> findAllByVideoId(Long videoId);
	UserVideoRating findByVideoIdAndUser(Long videoId,String user);
}
