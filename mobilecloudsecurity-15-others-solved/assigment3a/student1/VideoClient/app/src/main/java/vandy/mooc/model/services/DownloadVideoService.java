package vandy.mooc.model.services;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import vandy.mooc.view.ListVideosActivity;

/**
 * Intent Service that will run in background and Downloads the Video
 * having a given Id and video name.  After the operation, it will
 * broadcasts an Intent that sends  the result of the Download to the
 * DisplayVideoActivity.
 */
public class DownloadVideoService
        extends VideoServiceCommon {
    /**
     * Custom Action that will be used to send Broadcast to the
     * DisplayVideoActivity.
     */
    public static final String ACTION_DOWNLOAD_SERVICE_RESPONSE =
            "vandy.mooc.services.DownloadVideoService.RESPONSE";

    /**
     * Result code used by Broadcast Receiver to notify the success of
     * video download operation.
     */
    public static final int RESULT_OK = 200;

    /**
     * Result code used by Broadcast Receiver to notify the failure of
     * video download operation.
     */
    public static final int RESULT_ERROR = 404;

    /**
     * Key of the filePath to be passed as an extra by the Intent of
     * BroadcastReceiver.
     */
    public static final String KEY_FILE_PATH =
            "file_path";

    /**
     * Key of the Status of download operation to be passed as an
     * extra by the Intent of BroadcastReceiver.
     */
    public static final String KEY_STATUS = "status";

    /**
     * Key of the videoId to be passed as an extra by the Intent to
     * start this Service.
     */
    public static final String KEY_DOWNLOAD_VIDEO_ID =
            "download_videoId";

    /**
     * Key of the video title to be passed as an extra by the Intent
     * to start this Service.
     */
    public static final String KEY_DOWNLOAD_VIDEO_TITLE =
            "download_title";

    /**
     * Default videoId used when the videoId got from Intent is null.
     */
    public static final int DEFAULT_VIDEO_ID = 0;

    /**
     * The unique Id of the Video.
     */
    private long mVideoId;

    /**
     * The title of the Video.  In our App
     * the file name of the video is the title.
     */
    private String mVideoTitle;

    /**
     * Constructor for DownloadVideoService.
     */
    public DownloadVideoService() {
        super("DownloadVideoService");
    }

    /**
     * Factory method that makes the explicit intent another Activity
     * uses to call this Service.
     *
     * @param context
     * @param videoId
     * @param videoTitle
     * @return Intent to start this service.
     */
    public static Intent makeIntent(Context context,
                                    long videoId,
                                    String videoTitle) {

        return new Intent(context,
                DownloadVideoService.class)
                .putExtra(KEY_DOWNLOAD_VIDEO_ID,
                        videoId)
                .putExtra(KEY_DOWNLOAD_VIDEO_TITLE,
                        videoTitle);
    }

    /**
     * Hook method that is invoked on the worker thread with a request to
     * process. Only one Intent is processed at a time, but the
     * processing happens on a worker thread that runs in the
     * background, independently from other application logic.
     *
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // Get the VideoId from the Intent.
        mVideoId =
                intent.getLongExtra(KEY_DOWNLOAD_VIDEO_ID,
                        DEFAULT_VIDEO_ID);

        // Get the Video title from the Intent.
        mVideoTitle =
                intent.getStringExtra(KEY_DOWNLOAD_VIDEO_TITLE);

        // Starts the Notification to show the progress of video
        // download.
        startNotification();

        // Get the filePath in Local Storage after downloading Video.
        String filePath =
                mVideoDataMediator.downloadVideo(mVideoId,
                        mVideoTitle);

        if (filePath != null) {
            // Update the notification to show that Video Download is
            // successful.
            finishNotification("Download complete");

            // Send Broadcast to the DisplayVideoActivity to notify
            // that video download was success.
            sendBroadcast(RESULT_OK,
                    filePath);
        } else {
            // Update the notification to show that Video Download has
            // failed.
            finishNotification("Download failed");

            // Send Broadcast to the DisplayVideoActivity to notify
            // that video download has failed..
            sendBroadcast(RESULT_ERROR,
                    null);
        }
    }

    /**
     * Broadcast to DisplayVideoActivity to notify the result of
     * video download operation.
     */
    private void sendBroadcast(int status,
                               String filePath) {
        // Create the Intent to be sent to the DisplayVideoActivity.
        Intent intentResponse =
                new Intent(ACTION_DOWNLOAD_SERVICE_RESPONSE)
                        .addCategory(Intent.CATEGORY_DEFAULT)
                        .putExtra(KEY_STATUS,
                                status)
                        .putExtra(KEY_DOWNLOAD_VIDEO_ID,
                                mVideoId);

        // if the status is success, then add filePath of the stored
        // video as an extra to the Intent.
        if (status == RESULT_OK)
            intentResponse.putExtra(KEY_FILE_PATH,
                    filePath);

        // Use a LocalBroadcastManager to restrict the scope of this
        // Intent to the VideoUploadClient application.
        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(intentResponse);
    }

    /**
     * Starts the Notification to show the progress of video download.
     */
    private void startNotification() {
        // Create a PendingIntent so that when user clicks on
        // Notification, they are directed to the ListVideoActivity.
        final PendingIntent pendingIntent =
                PendingIntent.getActivity(this,
                        0,
                        ListVideosActivity.makeIntent(getApplicationContext()),
                        PendingIntent.FLAG_UPDATE_CURRENT);

        // Gets access to the Android Notification Service.
        mNotifyManager.notify
                (NOTIFICATION_ID,
                        makeNotification(android.R.drawable.stat_sys_download,
                                "Download",
                                mVideoTitle,
                                pendingIntent));
    }

    /**
     * Finish the Notification after the video is downloaded.
     *
     * @param status
     */
    private void finishNotification(String status) {
        // Build the Notification with the given Notification Id.
        mNotifyManager.notify(NOTIFICATION_ID,
                makeNotification
                        (android.R.drawable.stat_sys_download_done,
                                status));
    }
}