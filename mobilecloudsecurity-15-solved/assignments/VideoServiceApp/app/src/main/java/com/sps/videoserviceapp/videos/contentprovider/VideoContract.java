package com.sps.videoserviceapp.videos.contentprovider;

import android.content.ContentResolver;
import android.net.Uri;

/**
 * Created by Q1SW on 26-08-2015.
 */
public class VideoContract {

    /**
     * Content provider authority.
     */
    public static final String CONTENT_AUTHORITY = "com.sps.videoserviceapp";

    /**
     * Base URI. (content://com.sps.videoserviceapp)
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Path component for "entry"-type resources..
     */
    private static final String PATH_ENTRIES = "videos";

    public static class Entry {
        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.videoserviceapp.videos";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.videoserviceapp.video";

        /**
         * Fully qualified URI for "entry" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ENTRIES).build();

        // Database table
        public static final String TABLE_NAME = "video";
        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_URL = "url";
        public static final String COLUMN_NAME_DURATION = "duration";
        public static final String COLUMN_NAME_LOCATION = "location";
        public static final String COLUMN_NAME_SUBJECT = "subject";
        public static final String COLUMN_NAME_CONTENT_TYPE = "contentType";
        public static final String COLUMN_NAME_RATING = "rating";
    }
}
