/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.magnum.mobilecloud.video.controller;

import com.google.common.collect.Lists;

import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.model.AverageVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRating;
import org.magnum.mobilecloud.video.model.UserVideoRatingRepository;
import org.magnum.mobilecloud.video.model.Video;
import org.magnum.mobilecloud.video.model.VideoRepository;
import org.magnum.mobilecloud.video.model.VideoStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
public class VideoSvc {
    @Autowired
    private VideoRepository videos;

    @Autowired
    private UserVideoRatingRepository ratings;

    private VideoFileManager videoDataMgr;


    public VideoSvc() {
        try {
            videoDataMgr = new VideoFileManager();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such video")  // 404
    public class VideoNotFoundException extends RuntimeException {

        /**
         *
         */
        private static final long serialVersionUID = 40404L;

    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Access forbidden")    //  403
    public class VideoForbiddenException extends RuntimeException {

        private static final long serialVersionUID = 40403L;
    }


    /**
     * Video listing as JSON on server main GET
     *
     * @return ArrayList of videos as response body
     */
    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Video> getVideoList() {
        return Lists.newArrayList(videos.findAll());
    }


    /**
     * @param v Video object POST-ed as request body
     * @return same Video object with serverside added attributes for id and url
     */
    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    Video addVideo(@RequestBody Video v, Principal principal) {
        v.setOwner(principal.getName());
        Object stored = videos.save(v);
        if (stored != null)
            return (Video) stored;
        else
            return null;
    }


    /**
     * @param id        Video identifier
     * @param videoData binary mpg container
     * @param response  200 for success (Spring builtin) and custom exception declared in VideoSvc class
     * @return status of Video object
     * @throws IOException
     */
    @RequestMapping(value = VideoSvcApi.VIDEO_DATA_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    VideoStatus setVideoData(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
                             @RequestPart(VideoSvcApi.DATA_PARAMETER) MultipartFile videoData,
                             HttpServletResponse response, Principal principal)
            throws IOException {

        VideoStatus status = null;
        Video vid = videos.findOne(id);

        if (null != vid) {
            if (principal.getName().equals(vid.getOwner())) {
                //  if its owners video
                videoDataMgr.saveVideoData(vid, videoData.getInputStream());
                status = new VideoStatus(VideoStatus.VideoState.READY);

            } else
                //  its someone elses video
                throw new VideoForbiddenException();

        } else
            //  No such video
            throw new VideoNotFoundException();

        return status;

    }


    /**
     * @param id       Video id
     * @param response http response, 404 resolved in method
     * @throws IOException
     */
    @RequestMapping(value = VideoSvcApi.VIDEO_DATA_PATH, method = RequestMethod.GET)
    public void getData(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
                        HttpServletResponse response)
            throws IOException {


        if (null != videos.findOne(id) && videoDataMgr.hasVideoData((Video) videos.findOne(id)))
            videoDataMgr.copyVideoData((Video) videos.findOne(id), response.getOutputStream());
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

    }

    /**
     * @param id     Video id
     * @param rating user rating to be stored
     * @return average rating after user rating insert
     */
    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating/{rating}", method = RequestMethod.POST)
    public
    @ResponseBody
    AverageVideoRating rateVideo(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
                                 @PathVariable("rating") double rating,
                                 Principal principal) {

        Video vid = videos.findOne(id);
        if (null != vid) {

            UserVideoRating existingRating = ratings.findByVideoIdAndUser(id, principal.getName());
            if (null == existingRating) {
                //  no rating stored so far
                Object insertedRating = ratings.save(new UserVideoRating(id, rating, principal.getName()));
            } else {
                //  existing rating overwrite
                existingRating.setRating(rating);
                Object updatedRating = ratings.save(existingRating);
            }

            //  return object of type AverageVideoRating

            return new AverageVideoRating(ratings.findAvgRatingByVideoId(id), id, ratings.findCountRatingByVideoId(id));
        }
        //  No such video
        throw new VideoNotFoundException();

    }

    /**
     * @param id video id
     * @return average rating
     */
    @RequestMapping(value = VideoSvcApi.VIDEO_SVC_PATH + "/{id}/rating", method = RequestMethod.GET)
    public
    @ResponseBody
    AverageVideoRating getVideoRating(@PathVariable(VideoSvcApi.ID_PARAMETER) long id) {
        return new AverageVideoRating(ratings.findAvgRatingByVideoId(id), id, ratings.findCountRatingByVideoId(id));
    }

    private String getUrlBaseForLocalServer() {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return "http://" + request.getServerName()
                + ((request.getServerPort() != 80) ? ":" + request.getServerPort() : "");
    }

    private String getDataUrl(long videoId) {
        return getUrlBaseForLocalServer() + "/video/" + videoId + "/data";
    }

}
