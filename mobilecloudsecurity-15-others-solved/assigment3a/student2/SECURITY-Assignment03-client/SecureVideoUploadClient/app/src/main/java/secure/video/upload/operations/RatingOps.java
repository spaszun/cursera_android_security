package secure.video.upload.operations;

import android.app.Activity;
import android.content.ContentValues;
import android.util.Log;


import java.lang.ref.WeakReference;

import secure.video.upload.R;
import secure.video.upload.activities.RatingVideoActivity;
import secure.video.upload.model.mediator.VideoMediator;
import secure.video.upload.model.mediator.webdata.AverageVideoRating;
import secure.video.upload.model.mediator.webdata.VideoSvcApi;
import secure.video.upload.provider.VideoContract;
import secure.video.upload.utils.ConfigurableOps;
import secure.video.upload.utils.GenericAsyncTask;
import secure.video.upload.utils.GenericAsyncTaskOps;
import secure.video.upload.utils.PersProgressDialog;

public class RatingOps implements ConfigurableOps, GenericAsyncTaskOps<Void, Void, AverageVideoRating>
{
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * Retrofit api
     */
    private VideoSvcApi mVideoSvcApi;

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<RatingVideoActivity> mActivity;

    /**
     * Result to display
     */
    private AverageVideoRating mResult;

    /**
     * Used to get the average rating for a video
     */
    private GenericAsyncTask<Void, Void, AverageVideoRating, RatingOps> mAsyncTask;

    /**
     * Default constructor that's needed by the GenericActivity framework.
     */
    public RatingOps()
    {
    }


    @Override
    public void onConfiguration (Activity activity, boolean firstTimeIn)
    {
        final String time = firstTimeIn ? "first time" : "second+ time";
        Log.d (TAG, "onConfiguration() called the " + time + " with activity = " + activity);

        mActivity = new WeakReference<>((RatingVideoActivity) activity);

        if (firstTimeIn) {
            try {
                mVideoSvcApi = VideoMediator.get();
            }
            catch (SecurityException e) {
                mActivity.get().notifyMediatorNotConnected();
                return;
            }

            if (mAsyncTask != null) {
                mAsyncTask.cancel (true);
            }

            mAsyncTask = new GenericAsyncTask<>(this);
            mAsyncTask.execute();
        }
        else {
            if (mResult != null) {
                mActivity.get().receivedAvgRating (mResult, null);
            }
        }
    }

    @Override
    public void publishProgress (int progress)
    {
        mActivity.get().notifyProgressUpdate (progress, R.string.ratingDialogTitle,
                R.string.ratingDialogExpl);
    }

    @Override
    public AverageVideoRating doInBackground (Void param)
    {
        long videoId = mActivity.get().getVideoId();
        int personalRating = mActivity.get().getPersonalRating();

        // Rate the video and get the avg rating back
        AverageVideoRating avgRating = mVideoSvcApi.rateVideo (videoId, personalRating);

        // Update db
        String SELECTION_ID = VideoContract.VideoEntry.COLUMN_ID + " = " + videoId;
        ContentValues cv = new ContentValues();
        cv.put (VideoContract.VideoEntry.COLUMN_STAR_RATING, avgRating.getRating());
        cv.put (VideoContract.VideoEntry.COLUMN_TOTAL_RATINGS, avgRating.getTotalRatings());
        int rows = mActivity.get().getContentResolver().update (VideoContract.VideoEntry.CONTENT_URI,
                cv, SELECTION_ID, null);
        Log.d (TAG, "Updated " + rows + " rows in the db");

        return avgRating;
    }

    @Override
    public void onPostExecute (AverageVideoRating avgRating, Void param)
    {
        mResult = avgRating;
        String errorMessage = null;
        if (avgRating.getRating() == -1) {
            errorMessage = "Impossible to retrieve the avg rating for the video";
        }
        publishProgress (PersProgressDialog.OPERATION_COMPLETED);
        mActivity.get().receivedAvgRating (avgRating, errorMessage);
    }
}
