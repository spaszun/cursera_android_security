package vandy.mooc.model.services;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;


public class UploadVideoService extends VideoServiceCommon {

    public UploadVideoService() {
        super("UploadVideoService");
    }

    public static Intent makeIntent(Context context, Uri videoUri) {
        return new Intent(context,
                UploadVideoService.class)
                .setData(videoUri);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        startNotification();
        finishNotification(mVideoMediator.uploadVideo
                (intent.getData()));
    }

    private void startNotification() {
        mNotifyManager.notify
                (NOTIFICATION_ID,
                        makeNotification(
                                android.R.drawable.stat_sys_upload,
                                "Upload,",
                                "",
                                null));
    }

    private void finishNotification(String status) {
        mNotifyManager.notify
                (NOTIFICATION_ID,
                        makeNotification(
                                android.R.drawable.stat_sys_upload_done,
                                status));
    }
}
