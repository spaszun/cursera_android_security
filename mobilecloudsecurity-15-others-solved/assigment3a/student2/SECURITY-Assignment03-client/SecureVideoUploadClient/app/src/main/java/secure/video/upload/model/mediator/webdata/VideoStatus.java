package secure.video.upload.model.mediator.webdata;

public class VideoStatus
{
    public enum VideoState {
        READY, PROCESSING, FAILED
    }

    private VideoState state;

    public VideoStatus(VideoState state) {
        super();
        this.state = state;
    }

    public VideoState getState() {
        return state;
    }

    public void setState(VideoState state) {
        this.state = state;
    }
}
