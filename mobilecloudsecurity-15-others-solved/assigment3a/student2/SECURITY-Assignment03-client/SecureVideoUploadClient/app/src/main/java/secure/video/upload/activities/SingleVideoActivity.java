package secure.video.upload.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import java.util.concurrent.TimeUnit;

import secure.video.upload.R;
import secure.video.upload.operations.DownloadVideoOps;
import secure.video.upload.utils.GenericActivity;
import secure.video.upload.utils.PersProgressDialog;

public class SingleVideoActivity extends GenericActivity<DownloadVideoOps>
{
    private Button mPlayVideoBtn;
    private String mVideoData;
    private long mVideoId;
    private RatingBar mPersonalRatingBar;
    private RatingBar mAverageRatingBar;
    private TextView mTotalRatings;

    public static final int RATE_VIDEO_REQUEST = 1;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        setContentView (R.layout.activity_downloaded_video);

        mOpProgressDialog = new PersProgressDialog (SingleVideoActivity.this);

        TextView id = (TextView) findViewById (R.id.videoInfoID);
        TextView title  = (TextView) findViewById (R.id.videoInfoTitle);
        TextView duration = (TextView) findViewById (R.id.videoInfoDuration);
        TextView contentType = (TextView) findViewById (R.id.videoInfoContentType);
        TextView dataUrl = (TextView) findViewById (R.id.videoInfoDataUrl);
        mAverageRatingBar = (RatingBar) findViewById (R.id.videoInfoRatingBar);
        mTotalRatings = (TextView) findViewById (R.id.videoTotalRatings);

        Intent intent = getIntent();
        id.setText ("ID: " + String.valueOf (intent.getLongExtra (VideosInfoActivity.TAG_VIDEO_ID, -1)));
        title.setText ("Title: " + intent.getStringExtra (VideosInfoActivity.TAG_VIDEO_TITLE));

        long durationValue = intent.getLongExtra (VideosInfoActivity.TAG_VIDEO_DURATION, 0);
        long min = TimeUnit.MILLISECONDS.toMinutes (durationValue);
        long sec = TimeUnit.MILLISECONDS.toSeconds (durationValue) -
                TimeUnit.MINUTES.toSeconds (TimeUnit.MILLISECONDS.toMinutes (durationValue));
        duration.setText (String.format("Duration: %d:%d", min, sec));
        contentType.setText ("Content type: " + intent.getStringExtra (VideosInfoActivity.TAG_VIDEO_CONTENT_TYPE));
        dataUrl.setText("Url: " + intent.getStringExtra(VideosInfoActivity.TAG_VIDEO_DATA_URL));
        mAverageRatingBar.setNumStars(5);
        mAverageRatingBar.setRating((float) intent.getDoubleExtra(VideosInfoActivity.TAG_VIDEO_STARS, 0));
        mAverageRatingBar.setOnTouchListener (new View.OnTouchListener() {
            @Override
            public boolean onTouch (View v, MotionEvent event)
            {
                return true;
            }
        });
        mTotalRatings.setText("Number of voters: " + String.valueOf(intent.getIntExtra(VideosInfoActivity.TAG_TOTAL_VIDEO_RATINGS, 0)));

        mPlayVideoBtn = (Button) findViewById (R.id.playVideoBtn);
        mPlayVideoBtn.setEnabled (false);
        mVideoId = intent.getLongExtra(VideosInfoActivity.TAG_VIDEO_ID, 0);
        mPersonalRatingBar = (RatingBar) findViewById (R.id.personalRatingBar);
        mPersonalRatingBar.setOnRatingBarChangeListener (new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged (RatingBar ratingBar, float rating, boolean fromUser)
            {
                int iRating = (int) rating;
                Intent intent = new Intent (getApplicationContext(), RatingVideoActivity.class);
                intent.putExtra (VideosInfoActivity.TAG_VIDEO_ID, mVideoId);
                intent.putExtra (VideosInfoActivity.TAG_VIDEO_STARS, iRating);
                startActivityForResult (intent, RATE_VIDEO_REQUEST);
            }
        });

        super.onCreate (savedInstanceState, DownloadVideoOps.class);
    }

    public void downloadVideoData (View view)
    {
        getOps().downloadVideoData (mVideoId);
    }

    public void notifyMediatorNotConnected()
    {
        Toast.makeText (this, "Impossible to download the video", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void videoDataArrived (String videoData, String errorMessage)
    {
        if (videoData == null) {
            Toast.makeText (this, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }

        mPlayVideoBtn.setEnabled (true);
        mVideoData = videoData;
    }

    public void playVideo (View view)
    {
        if (mVideoData == null) {
            return;
        }

        Intent intent = new Intent (this, VideoPlayerActivity.class);
        intent.putExtra (VideoPlayerActivity.VIDEO_FILE, mVideoData);
        startActivity (intent);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        if (requestCode == RATE_VIDEO_REQUEST) {
            if (resultCode == RESULT_OK) {
                double newRating = data.getDoubleExtra (VideosInfoActivity.TAG_VIDEO_STARS, -1);
                int newTotalRatings = data.getIntExtra (VideosInfoActivity.TAG_TOTAL_VIDEO_RATINGS, -1);
                if ((newRating == -1) || (newTotalRatings == -1)) {
                    Toast.makeText (getApplicationContext(), "Impossible to get the average rating",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                mAverageRatingBar.setRating ((float) newRating);
                mTotalRatings.setText (String.valueOf (newTotalRatings));
            }
        }
    }
}
